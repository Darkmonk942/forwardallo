package com.virtualeducation.forwardpremium.eventbus;

import com.virtualeducation.forwardpremium.objects.SmsObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 31.07.2017.
 */

public class SmsListEvent {
    public final ArrayList<SmsObject> smsList;
    public boolean isBasicContacts;

    public SmsListEvent(ArrayList<SmsObject> smsList,boolean isBasicContacts) {
        this.smsList = smsList;
        this.isBasicContacts=isBasicContacts;
    }
}
