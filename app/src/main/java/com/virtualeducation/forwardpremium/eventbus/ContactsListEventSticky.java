package com.virtualeducation.forwardpremium.eventbus;

import com.virtualeducation.forwardpremium.objects.ContactObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 13.07.2017.
 */

public class ContactsListEventSticky {
    public final ArrayList<ContactObject> contactsList;

    public ContactsListEventSticky(ArrayList<ContactObject> contactsList) {
        this.contactsList = contactsList;
    }
}
