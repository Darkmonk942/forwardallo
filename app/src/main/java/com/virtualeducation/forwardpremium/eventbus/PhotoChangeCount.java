package com.virtualeducation.forwardpremium.eventbus;

/**
 * Created by DarKMonK on 30.07.2017.
 */

public class PhotoChangeCount {
    public final int count;

    public PhotoChangeCount(int count){
        this.count=count;
    }
}
