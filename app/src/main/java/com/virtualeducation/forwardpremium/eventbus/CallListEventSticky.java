package com.virtualeducation.forwardpremium.eventbus;

import com.virtualeducation.forwardpremium.objects.CallObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 11.07.2017.
 */

public class CallListEventSticky {
    public final ArrayList<CallObject> callsList;

    public CallListEventSticky(ArrayList<CallObject> callsList) {
        this.callsList = callsList;
    }
}
