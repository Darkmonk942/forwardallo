package com.virtualeducation.forwardpremium.eventbus;

import android.util.Log;

import com.virtualeducation.forwardpremium.objects.ContactObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 31.07.2017.
 */

public class ContactsListEvent {
    public final ArrayList<ContactObject> contactsList;
    public boolean isBasicContacts=true;

    public ContactsListEvent(ArrayList<ContactObject> contactsList,boolean isBasicContacts) {
        Log.d("ЭвентКонтакт", "Эвент");
        this.contactsList = contactsList;
        this.isBasicContacts=isBasicContacts;
    }
}
