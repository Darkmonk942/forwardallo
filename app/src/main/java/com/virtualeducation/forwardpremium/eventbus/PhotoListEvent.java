package com.virtualeducation.forwardpremium.eventbus;

import com.virtualeducation.forwardpremium.objects.PhotoObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 31.07.2017.
 */

public class PhotoListEvent {

    public final ArrayList<PhotoObject> photoList;
    public boolean isBasicContacts=true;

    public PhotoListEvent(ArrayList<PhotoObject> photoList,boolean isBasicContacts) {
        this.photoList = photoList;
        this.isBasicContacts=isBasicContacts;
    }
}
