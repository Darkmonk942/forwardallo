package com.virtualeducation.forwardpremium.eventbus;

/**
 * Created by DarKMonK on 30.07.2017.
 */

public class PhotoListCheckEvent {
    public final boolean isChecked;

    public PhotoListCheckEvent(boolean isChecked) {
        this.isChecked = isChecked;
    }
}
