package com.virtualeducation.forwardpremium.eventbus;

import com.virtualeducation.forwardpremium.objects.CallObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 11.07.2017.
 */

public class CallsListEvent {
    public final ArrayList<CallObject> callsList;
    public boolean isBasicContacts=true;

    public CallsListEvent(ArrayList<CallObject> callsList,boolean isBasicContacts) {
        this.callsList = callsList;
        this.isBasicContacts=isBasicContacts;
    }
}
