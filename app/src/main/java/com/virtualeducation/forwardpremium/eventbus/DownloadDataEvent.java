package com.virtualeducation.forwardpremium.eventbus;

public class DownloadDataEvent {
    public static final String DOWNLOAD_START="start";
    public static final String DOWNLOAD_FAILED="failed";
    public static final String DOWNLOAD_FINISH="finish";
    public static final String DOWNLOAD_STOP="stop";

    public final String type;
    public final int progress;
    public float progressFloat;

    public DownloadDataEvent(String type,int progress, float progressFloat){
        this.type=type;
        this.progress=progress;
        this.progressFloat = progressFloat;
    }

}
