package com.virtualeducation.forwardpremium.eventbus;

import com.virtualeducation.forwardpremium.objects.PhotoObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 27.07.2017.
 */

public class PhotoListEventSticky {
    public final ArrayList<PhotoObject> photoList;

    public PhotoListEventSticky(ArrayList<PhotoObject> photoList) {
        this.photoList = photoList;
    }
}
