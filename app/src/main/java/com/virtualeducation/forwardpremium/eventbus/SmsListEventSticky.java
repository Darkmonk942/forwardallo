package com.virtualeducation.forwardpremium.eventbus;

import com.virtualeducation.forwardpremium.objects.SmsObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 15.07.2017.
 */

public class SmsListEventSticky {

    public final ArrayList<SmsObject> smsList;

    public SmsListEventSticky(ArrayList<SmsObject> smsList) {
        this.smsList = smsList;
    }
}
