package com.virtualeducation.forwardpremium.eventbus;

import com.virtualeducation.forwardpremium.objects.CallObject;

import java.util.ArrayList;

public class UploadDataEvent {
    public final boolean isFailed;
    public final boolean isFinished;
    public final int progress;

    public UploadDataEvent(boolean isFailed,boolean isFinished,int progress) {
        this.isFailed = isFailed;
        this.isFinished=isFinished;
        this.progress=progress;
    }
}
