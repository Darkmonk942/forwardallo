package com.virtualeducation.forwardpremium.eventbus;

/**
 * Created by DarKMonK on 03.09.2017.
 */

public class PhotoDownloadEvent {
    public final int counter;
    public final boolean isLast;

    public PhotoDownloadEvent(int counter,boolean isLast) {
        this.counter = counter;
        this.isLast = isLast;
    }
}
