package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.virtualeducation.forwardpremium.adapters.SmsListAdapter;
import com.virtualeducation.forwardpremium.databinding.ActivityUserSmsListBinding;
import com.virtualeducation.forwardpremium.eventbus.SmsListEvent;
import com.virtualeducation.forwardpremium.objects.SmsObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 15.07.2017.
 */

public class UserSmsListActivity extends AppCompatActivity {
    ActivityUserSmsListBinding smsListBinding;
    Optimization optimization;
    SmsListAdapter smsListAdapter;
    ArrayList<SmsObject> allSmsList=new ArrayList();
    ArrayList<SmsObject> allSmsListBackup=new ArrayList();
    int smsCount=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        smsListBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_sms_list);

        // Оптимизация интерфейса
        optimization = new Optimization();
        optimization.Optimization(smsListBinding.frameMain);

        getAllSms();
    }

    // Инициализация RecyclerView
    private void recyclerViewInit(){
        smsListBinding.recyclerSms.setHasFixedSize(true);
        smsListBinding.recyclerSms.setLayoutManager(new LinearLayoutManager(this));
        smsListAdapter=new SmsListAdapter(allSmsList,smsListBinding,optimization);
        smsListAdapter.setHasStableIds(true);
        smsListBinding.recyclerSms.setAdapter(smsListAdapter);
    }

    // Переход на главный экран
    public void backClick(View v) {
        EventBus.getDefault().post(new SmsListEvent(allSmsListBackup,true));
        finish();
    }

    // Подтверждаем выборанные звонки
    public void btnConfirmClick(View v){
        EventBus.getDefault().post(new SmsListEvent(allSmsList,true));
        finish();
    }

    // Выбираем/Отключаем все смс
    public void chooseAllSmsClick(View v){
        if(smsListBinding.icSmsCheck.getVisibility()==View.VISIBLE){
            smsListAdapter.currentSmsCount=0;
            smsListBinding.textSmsCount.setText("0/"+String.valueOf(smsCount));
            smsListBinding.icSmsCheck.setVisibility(View.GONE);
            setAllCallsCheck(false);
        }else{
            smsListAdapter.currentSmsCount=smsCount;
            smsListBinding.textSmsCount.setText(String.valueOf(smsCount)+"/"+String.valueOf(smsCount));
            smsListBinding.icSmsCheck.setVisibility(View.VISIBLE);
            setAllCallsCheck(true);
        }
    }

    // Изменяем чекер на всех звонках
    private void setAllCallsCheck(boolean isCheck){
        for (int i=0;i<allSmsList.size();i++){
            allSmsList.get(i).isChecked=isCheck;
        }
        smsListAdapter.notifyDataSetChanged();
    }

    // Получаем список смсок
    private void getAllSms(){
        allSmsList.addAll(MainScreenActivity.allSmsList);
        smsCount=allSmsList.size();

        // Сохраняем весь список смс для восстановления после изменений
        cloneSmsList();

        // Инициализация RecyclerView
        recyclerViewInit();

        // Получаем все выбранные смс
        for (int i=0;i<allSmsList.size();i++) {
            if (allSmsList.get(i).isChecked) {
                smsListAdapter.currentSmsCount = smsListAdapter.currentSmsCount + 1;
            }
        }

        smsListBinding.textSmsCount.setText(String.valueOf(smsListAdapter.currentSmsCount)+"/"+String.valueOf(smsCount));
    }

    // Сохраняем весь список смс для восстановления после изменений
    private void cloneSmsList(){
        Thread smsThread = new Thread(
                new Runnable() {
                    public void run() {
                        for (SmsObject smsObject:allSmsList){
                            allSmsListBackup.add(smsObject.clone());
                        }
                    }
                }
        );
        smsThread.start();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    // Передаём event на главную страницу в виде листа выбранных звонков
    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new SmsListEvent(allSmsListBackup,true));
        finish();
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
