package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.virtualeducation.forwardpremium.eventbus.ContactsListEvent;
import com.virtualeducation.forwardpremium.objects.ContactObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 13.07.2017.
 */

public class UserContactsGetAll {

    public  void gelAllContacts(Context context){
        final Cursor cursorContacts =  context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME +" ASC");

        // Получаем нужные колонки для поиска инфы по таблице контактов
        final int columnIndexId=cursorContacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID);
        final int columnIndexName=cursorContacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        final int columnIndexPhoneNumber=cursorContacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        final int columnIndexContactPhoto=cursorContacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);

        // Создаём новый поток для получения списка контактов
        Thread contactsThread = new Thread(
                new Runnable() {
                    public void run() {
                        final ArrayList<ContactObject> allContactsList=new ArrayList();
                        // Получаем данные по контактам
                        while (cursorContacts.moveToNext()){
                            try {
                                ContactObject contactObject = new ContactObject();
                                contactObject.id = cursorContacts.getString(columnIndexId);
                                if(cursorContacts.getString(columnIndexName)!=null){
                                    contactObject.contactName = cursorContacts.getString(columnIndexName);
                                }
                                if(cursorContacts.getString(columnIndexPhoneNumber)!=null){
                                    contactObject.contactNumber = cursorContacts.getString(columnIndexPhoneNumber);
                                    contactObject.contactNumberTrim=contactObject.contactNumber.replaceAll(" ","");
                                }
                                if(cursorContacts.getString(columnIndexContactPhoto)!=null){
                                    contactObject.contactPhotoUri = cursorContacts.getString(columnIndexContactPhoto);
                                }


                                if(allContactsList.size()== 0){
                                    allContactsList.add(contactObject);
                                }else{
                                    if(!allContactsList.get(allContactsList.size()-1).contactName.equals(contactObject.contactName)){
                                        allContactsList.add(contactObject);
                                    }
                                }

                            }catch (Exception e){

                            }
                        }
                        EventBus.getDefault().post(new ContactsListEvent(allContactsList,false));
                        cursorContacts.close();
                    }
                });
        contactsThread.start();

    }
}
