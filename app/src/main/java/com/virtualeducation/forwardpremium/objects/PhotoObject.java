package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 25.07.2017.
 */

public class PhotoObject implements Cloneable {

    @SerializedName("uid")
    @Expose
    public String id;

    @SerializedName("photo_uri")
    @Expose
    public String photoUri;

    @SerializedName("photo_thumbnail")
    @Expose
    public String photoThumbnail;

    @SerializedName("photo_name")
    @Expose
    public String photoName;

    @SerializedName("photo_category")
    @Expose
    public String photoCategory="";

    public String photoTitleCategory;

    public boolean isChecked=true;

    @Override
    public PhotoObject clone(){
        PhotoObject photoObject=new PhotoObject();
        photoObject.id=this.id;
        photoObject.photoUri=this.photoUri;
        photoObject.photoName=this.photoName;
        photoObject.photoCategory=this.photoCategory;
        photoObject.isChecked=this.isChecked;
        photoObject.photoThumbnail=this.photoThumbnail;

        return photoObject;
    }
}
