package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 04.07.2017.
 */

public class User {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("email")
    @Expose
    public String userEmail;

    @SerializedName("access_token")
    @Expose
    public String accessToken;

    @SerializedName("used")
    @Expose
    public String usedStorage;

    @SerializedName("premium")
    @Expose
    public boolean isPremium;

    @SerializedName("contacts")
    @Expose
    public ArrayList<ContactBackupObject> contactsBackup=new ArrayList();

    @SerializedName("rings")
    @Expose
    public ArrayList<CallBackupObject> callsBackup=new ArrayList();

    @SerializedName("sms")
    @Expose
    public ArrayList<SmsBackupObject> smsBackup=new ArrayList();

    @SerializedName("photos")
    @Expose
    public ArrayList<PhotoBackupObject> photoBackup=new ArrayList();

    @SerializedName("documents")
    @Expose
    public ArrayList<DocsBackupObject> docsBackup=new ArrayList();

    @SerializedName("settings")
    @Expose
    public ArrayList<SettingsBackupObject> settingsBackup=new ArrayList();

    @SerializedName("calendars")
    @Expose
    public ArrayList<CalendarBackupObject> calendarBackup=new ArrayList();

    @SerializedName("applications")
    @Expose
    public ArrayList<AppsBackObject> appsBackup=new ArrayList();
}
