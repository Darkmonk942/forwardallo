package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocDescrObject {

    @SerializedName("size")
    @Expose
    public long size;

    public DocDescrObject(long size){
        this.size=size;
    }
}
