package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 27.08.2017.
 */

public class CloudBackupPhoto {

    @SerializedName("file")
    public PhotoObject photoObject;

    public class PhotoObject {
        @SerializedName("download_url")
        public String url;

        @SerializedName("name")
        public String name;
    }

}
