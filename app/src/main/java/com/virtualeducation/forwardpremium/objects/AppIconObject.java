package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppIconObject {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("image")
    @Expose
    public String icon;
}
