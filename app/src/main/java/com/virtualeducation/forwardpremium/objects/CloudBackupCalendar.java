package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CloudBackupCalendar {
    @SerializedName("data")
    @Expose
    public List<CalendarObject> calendarList;

    @SerializedName("used")
    @Expose
    public String usedStorage;
}
