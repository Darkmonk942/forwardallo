package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 13.07.2017.
 */

public class SmsObject implements Cloneable {

    @SerializedName("uid")
    @Expose
    public String id;

    @SerializedName("text")
    @Expose
    public String smsBody;

    @SerializedName("name")
    @Expose
    public String smsName;

    @SerializedName("phone")
    @Expose
    public String smsNumber;

    @SerializedName("time")
    @Expose
    public String smsDate;

    public transient boolean isChecked=true;

    @SerializedName("type")
    @Expose
    public int smsType = 0;

    public transient String smsUserName="";

    @Override
    public SmsObject clone(){
        SmsObject smsObject=new SmsObject();
        smsObject.id=this.id;
        smsObject.smsBody=this.smsBody;
        smsObject.smsName=this.smsName;
        smsObject.smsNumber=this.smsNumber;
        smsObject.smsDate=this.smsDate;
        smsObject.isChecked=this.isChecked;
        smsObject.smsType=this.smsType;
        smsObject.smsUserName=this.smsUserName;

        return smsObject;
    }

}
