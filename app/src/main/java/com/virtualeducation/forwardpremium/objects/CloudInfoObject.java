package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 21.07.2017.
 */

public class CloudInfoObject {

    @SerializedName("used")
    @Expose
    public String usedStorage;

    @SerializedName("contacts")
    @Expose
    public int contactsCount;

    @SerializedName("rings")
    @Expose
    public int callsCount;

    @SerializedName("sms")
    @Expose
    public int smsCount;

    @SerializedName("photos")
    @Expose
    public int photoCount;

    @SerializedName("documents")
    @Expose
    public int docsCount;

    @SerializedName("applications")
    @Expose
    public int appsCount;

    @SerializedName("settings")
    @Expose
    public int settingsCount;

    @SerializedName("calendars")
    @Expose
    public int calendarCount;
}
