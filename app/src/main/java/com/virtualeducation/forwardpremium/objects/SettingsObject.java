package com.virtualeducation.forwardpremium.objects;

import android.provider.Settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsObject {

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("value")
    @Expose
    public String value;

    public SettingsObject(String name,String value){
        this.name=name;
        this.value=value;
    }
}
