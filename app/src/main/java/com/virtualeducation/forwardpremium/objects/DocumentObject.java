package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentObject {

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("doc_uri")
    @Expose
    public String docUri;

    public DocumentObject(String title,String docUri){
        this.title=title;
        this.docUri=docUri;
    }
}
