package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalendarObject {

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("description")
    @Expose
    public String description="";

    @SerializedName("start_date")
    @Expose
    public long startDate;

    @SerializedName("end_date")
    @Expose
    public long endDate;

    public transient int calendarId;

    @SerializedName("timezone")
    @Expose
    public String timezone;

    @SerializedName("ac_type")
    @Expose
    public String accountType;

    @SerializedName("ac_name")
    @Expose
    public String accountName;

    public CalendarObject(String title,String description,long startDate,
                          long endDate, int calendarId,String timezone,
                          String accountName,String accountType){
        this.title=title;
        this.description=description;
        this.startDate=startDate;
        this.endDate=endDate;
        this.calendarId=calendarId;
        this.timezone=timezone;
        if(timezone.isEmpty()){
            this.timezone="";
        }

        this.accountName=accountName;
        this.accountType=accountType;

    }
}
