package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CloudBackupApps {
    @SerializedName("application_info")
    @Expose
    public AppsObject appsList;

    @SerializedName("app_name")
    @Expose
    public String appName;

    @SerializedName("used")
    @Expose
    public String usedStorage;
}
