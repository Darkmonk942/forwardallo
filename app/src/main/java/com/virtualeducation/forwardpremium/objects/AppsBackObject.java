package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppsBackObject {

    @SerializedName("app_package")
    @Expose
    public String appPackage;

    @SerializedName("id")
    @Expose
    public int id;
}
