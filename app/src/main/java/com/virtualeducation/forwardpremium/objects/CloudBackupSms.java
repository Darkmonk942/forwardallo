package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DarKMonK on 25.08.2017.
 */

public class CloudBackupSms {

    @SerializedName("data")
    @Expose
    public List<SmsObject> smsList;

    @SerializedName("used")
    @Expose
    public String usedStorage;
}
