package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocsBackupObject {

//    @SerializedName("uid")
//    @Expose
//    public int id;

    @SerializedName("name")
    @Expose
    public String name;
}
