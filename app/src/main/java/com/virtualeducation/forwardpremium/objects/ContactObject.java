package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 13.07.2017.
 */

public class ContactObject implements Cloneable {

    @SerializedName("uid")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String contactName;

    @SerializedName("phone")
    @Expose
    public String contactNumber;

    @Expose()
    public transient String contactNumberTrim;

    @SerializedName("photo_uri")
    @Expose
    public transient String contactPhotoUri;

    @SerializedName("is_checked")
    @Expose
    public  transient boolean isChecked=true;

    @Override
    public ContactObject clone(){
        ContactObject contactObject=new ContactObject();
        contactObject.id=this.id;
        contactObject.contactName=this.contactName;
        contactObject.contactNumber=this.contactNumber;
        contactObject.contactPhotoUri=this.contactPhotoUri;
        contactObject.isChecked=this.isChecked;

        return contactObject;
    }

}
