package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalendarBackupObject {

    @SerializedName("calendar_title")
    @Expose
    public String title;

    @SerializedName("calendar_start_date")
    @Expose
    public long startDate;
}
