package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 25.07.2017.
 */

public class SmsBackupObject {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("uid")
    @Expose
    public String uid;

    @SerializedName("time")
    @Expose
    public String smsDate;
}


