package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 25.07.2017.
 */

public class BackupInfoObject {
    @SerializedName("used")
    @Expose
    public String storageUsed;

    @SerializedName("contacts")
    @Expose
    public ArrayList<ContactBackupObject> contactsBackup;

    @SerializedName("callLog")
    @Expose
    public ArrayList<CallBackupObject> callsBackup;

    @SerializedName("sms")
    @Expose
    public ArrayList<SmsBackupObject> smsBackup;

    @SerializedName("files")
    @Expose
    public ArrayList<PhotoBackupObject> photoBackup;
}
