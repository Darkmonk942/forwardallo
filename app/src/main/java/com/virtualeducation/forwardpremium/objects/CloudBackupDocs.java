package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.SerializedName;

public class CloudBackupDocs {

    @SerializedName("file")
    public DocObject docObject;

    public class DocObject {

        @SerializedName("download_url")
        public String url;
    }
}
