package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 10.08.2017.
 */

public class PhotoDescrObject {

    @SerializedName("uid")
    @Expose
    public String uid;

    @SerializedName("size")
    @Expose
    public long size;
}
