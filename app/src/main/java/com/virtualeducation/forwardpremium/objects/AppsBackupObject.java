package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppsBackupObject {

    @SerializedName("app_name")
    @Expose
    public String appName;

    @SerializedName("app_package")
    @Expose
    public String appPackage;

    public AppsBackupObject(String appName, String appPackage){
        this.appName=appName;
        this.appPackage=appPackage;
    }
}
