package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsBackupObject {

    @SerializedName("name")
    @Expose
    public String name;
}
