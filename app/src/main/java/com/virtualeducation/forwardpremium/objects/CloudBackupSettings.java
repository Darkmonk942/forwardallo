package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CloudBackupSettings {
    @SerializedName("data")
    @Expose
    public List<SettingsObject> settingsList;

    @SerializedName("used")
    @Expose
    public String usedStorage;
}
