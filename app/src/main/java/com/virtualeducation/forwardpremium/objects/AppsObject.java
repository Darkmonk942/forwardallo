package com.virtualeducation.forwardpremium.objects;

import android.graphics.drawable.Drawable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppsObject {

    @SerializedName("app_name")
    @Expose
    public String appName;

    @SerializedName("app_vendor")
    @Expose
    public String appVendor;

    @SerializedName("app_icon")
    @Expose
    public String appIcon;

    @SerializedName("app_package")
    @Expose
    public String appPackage;

    @SerializedName("is_install")
    @Expose
    public boolean isInstall;

    @SerializedName("icon_file")
    @Expose
    public AppIconModel appIconModel;

    public class AppIconModel {

        @SerializedName("download_url")
        @Expose
        public String icUrl;
    }
}
