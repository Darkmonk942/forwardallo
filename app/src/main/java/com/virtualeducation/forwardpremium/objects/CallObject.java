package com.virtualeducation.forwardpremium.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Darkmonk on 09.07.2017.
 */

public class CallObject implements Cloneable{

    @SerializedName("uid")
    @Expose
    public String id;

    @SerializedName("phone")
    @Expose
    public String callNumber;

    @SerializedName("type")
    @Expose
    public String callType;

    @SerializedName("time")
    @Expose
    public String callDate;

    public transient String callDateDayTime;

    @SerializedName("duration")
    @Expose
    public String callDuration;

    @SerializedName("name")
    @Expose
    public String callName="";

    public String callPhotoUri;
    public transient boolean isChecked=true;

    @Override
    public CallObject clone(){
        CallObject callObject=new CallObject();
        callObject.id=this.id;
        callObject.callNumber=this.callNumber;
        callObject.callType=this.callType;
        callObject.callDate=this.callDate;
        callObject.callDateDayTime=this.callDateDayTime;
        callObject.callDuration=this.callDuration;
        callObject.callName=this.callName;
        callObject.callPhotoUri=this.callPhotoUri;
        callObject.isChecked=this.isChecked;

        return callObject;
    }

}
