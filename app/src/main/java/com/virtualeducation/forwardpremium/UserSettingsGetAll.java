package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.virtualeducation.forwardpremium.objects.SettingsObject;

import java.util.ArrayList;
import java.util.Date;

public class UserSettingsGetAll {
    String mas[]={"volume_music_earpiece","volume_music_earpiece","volume_alarm","volume_music",
    "volume_voice","microphone_mute","volume_ring_headphone","volume_ring","alarm_alert_set",
    "volume_voice_headphone","user_rotation","volume_ring_speaker","volume_music_headset",
    "volume_system","screen_brightness","volume_voice_speaker"};

    private Context context;
    ArrayList<SettingsObject> allSettings=new ArrayList<>();
    /*
    Маркеры настроек, которые используются для бэкапа
    volume_ring_earpiece
    volume_music_earpiece
    volume_alarm
    volume_music
    volume_voice
    microphone_mute
    volume_ring_headphone
    volume_ring
    alarm_alert_set
    volume_voice_headphone
    user_rotation
    volume_ring_speaker
    volume_music_headset
    volume_system
    screen_brightness
    volume_voice_speaker
     */

    public ArrayList<SettingsObject> getUserSettings(Context context){
        this.context=context;
        allSettings.clear();
                Cursor cursor = context.getContentResolver().query(Settings.System.CONTENT_URI,null,null,null,null);

                if (cursor.moveToFirst()) {
                    do {
                        addSettingToList(cursor.getString(1),cursor.getString(2));
                    } while ( cursor.moveToNext());
                }

                return allSettings;
    }

    // Save settings to list
    private void addSettingToList(String name, String value){
        for (String nameType:mas){
            if(name.equals(nameType)){
                allSettings.add(new SettingsObject(name,value));
                break;
            }
        }
    }


    private void openAndroidPermissionsMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + "com.virtualeducation.forwardpremium"));
        context.startActivity(intent);
    }
}
