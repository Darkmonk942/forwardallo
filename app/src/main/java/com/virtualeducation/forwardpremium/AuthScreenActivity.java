package com.virtualeducation.forwardpremium;

import android.animation.ArgbEvaluator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.adapters.AuthAdapter;
import com.virtualeducation.forwardpremium.databinding.ActivityAuthScreenBinding;
import com.virtualeducation.forwardpremium.objects.User;
import com.virtualeducation.forwardpremium.retrofit.PostUserAuthRequest;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 01.07.2017.
 */

public class AuthScreenActivity extends AppCompatActivity {
    ActivityAuthScreenBinding authBinding;
    PostUserAuthRequest authRequest;
    ProgressDialog alertDialog;
    AuthAdapter authAdapter;
    ArgbEvaluator argbEvaluator;
    Optimization optimization;
    // Colors
    @BindColor(R.color.colorBlue) int colorBlue;
    @BindColor(R.color.colorAccent) int colorLightRed;
    @BindColor(R.color.colorWhite) int colorWhite;
    @BindColor(R.color.colorWhite90) int colorWhite90;
    // Strings
    @BindString(R.string.alert_start_screen_auth_error)
    String alertAuthError;
    @BindString(R.string.alert_only_allo)
    String alertOnlyAllo;
    @BindString(R.string.version_type)
    String versionType;
    // Параметры экрана
    int tenPxWidth;
    int tenPxHeight;
    int pagerTriangleLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        authBinding = DataBindingUtil.setContentView(this, R.layout.activity_auth_screen);

        // Оптимизация интерфейса
        optimization = new Optimization();
        optimization.Optimization(authBinding.frameInner);

        // Подставляем информацию о партнёре
        setPartnerInfo();

        // VideoView для отображения анимированного лого
        authBinding.videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video_auth_screen));
        authBinding.videoView.start();

        // Получаем параметры экрана
        int screenWidth = Hawk.get("width", 0);
        int screenHeight = Hawk.get("height", 0);
        tenPxWidth = screenWidth / 108;
        tenPxHeight = screenHeight / 192;
        pagerTriangleLimit = screenWidth - 28 * tenPxWidth;

        // Инициализация ViewPager-а
        initViewPager();

        // Формируем запросы для регистрации/авторизации/восстановления пароля
        authRequest = RetrofitApi.getInstance().retrofit.create(PostUserAuthRequest.class);

        // Добавляем пустую зону в Скрол при открытой клавиатуре
        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if (isOpen) {
                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                            params.setMargins(0, -1 * tenPxHeight * 70, 0, 0);
                            authBinding.frameInner.setLayoutParams(params);
                        } else {
                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                            params.setMargins(0, 0, 0, 0);
                            authBinding.frameInner.setLayoutParams(params);
                        }
                    }
                });
    }

    // Инициализация ViewPager-а
    private void initViewPager() {
        authAdapter = new AuthAdapter(this, optimization);
        authBinding.viewPager.setAdapter(authAdapter);
        authBinding.viewPager.setOffscreenPageLimit(2);

        final Integer colors[] = {colorBlue, colorLightRed};
        argbEvaluator = new ArgbEvaluator();

        // Слушаем изменение в Pager-e
        authBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                authBinding.imageTriangle.setX(25 * tenPxWidth + positionOffset * (pagerTriangleLimit - 25 * tenPxWidth));
                if (position < (authAdapter.getCount() - 1) && position < (colors.length - 1)) {
                    authBinding.viewPager.setBackgroundColor((Integer) argbEvaluator.evaluate(positionOffset, colors[position], colors[position + 1]));
                    authBinding.imageTriangle.setBackgroundColor((Integer) argbEvaluator.evaluate(positionOffset, colors[position], colors[position + 1]));
                } else {
                    authBinding.viewPager.setBackgroundColor(colors[colors.length - 1]);
                    authBinding.imageTriangle.setBackgroundColor(colors[colors.length - 1]);
                    authBinding.imageTriangle.setX(pagerTriangleLimit);
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    authBinding.textNewUser.setTextColor(colorWhite);
                    authBinding.textAlreadyUsed.setTextColor(colorWhite90);
                    authBinding.frameMain.setBackgroundColor(colorBlue);
                } else {
                    authBinding.textNewUser.setTextColor(colorWhite90);
                    authBinding.textAlreadyUsed.setTextColor(colorWhite);
                    authBinding.frameMain.setBackgroundColor(colorLightRed);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    // Переходим на экран активации промокода
    public void btnAddPromoClick(View v) {
        startActivity(new Intent(this, PromocodeActivity.class));
        finish();
    }

    // Отправляем запрос на регистрацию нового пользователя
    public void btnRegistrationClick(View v) {

        // Проверка валидности email
        if (!authAdapter.editRegEmail.getText().toString().contains("@") &&
                !authAdapter.editRegEmail.getText().toString().contains(".") &&
                authAdapter.editRegEmail.getText().toString().length() < 5) {
            ToastManager.createToast(this, alertAuthError);
            return;
        }

        // Проверка валидности пароля
        if (authAdapter.editRegPass.getText().toString().length() < 5) {
            ToastManager.createToast(this, alertAuthError);
            return;
        }

        // Отправляем запрос на регистрацию
        createProgressAlert(true);


        authRequest.userRegistration(authAdapter.editRegEmail.getText().toString(),
                        authAdapter.editRegPass.getText().toString(),
                        (String)Hawk.get("promocode")).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                createProgressAlert(false);

                if (response == null || response.code() != 200) {
                    ToastManager.createToast(getApplicationContext(), alertAuthError);
                    return;
                }

                // Сохраняем все данные пользователя
                Hawk.put("userData", response.body());
                Hawk.put("userAuthStage", "inUse");
                Hawk.put("accessToken", "Bearer " + response.body().accessToken);
//                Hawk.put("premium", response.body().isPremium);
//                Hawk.put("userAuthData",
//                        new Pair<String, String>
//                                (authAdapter.editRegEmail.getText().toString(),
//                                        authAdapter.editRegPass.getText().toString()));
                Hawk.put("promocode", "");

                // Переходим на главный экран
                startActivity(new Intent(AuthScreenActivity.this, MainScreenActivity.class));
                finish();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("Ответ1", t.toString());
                createProgressAlert(false);
                ToastManager.createToast(getApplicationContext(), alertAuthError);
            }
        });
    }

    // Обработка входа в приложение через email+пароль
    public void btnLoginClick(View v) {
        // Проверка валидности email
        if (!authAdapter.editAuthEmail.getText().toString().contains("@") &&
                !authAdapter.editAuthEmail.getText().toString().contains(".") &&
                authAdapter.editAuthEmail.getText().toString().length() < 5) {
            ToastManager.createToast(this, alertAuthError);
            return;
        }

        // Проверка валидности пароля
        if (authAdapter.editAuthPass.getText().toString().length() < 5) {
            ToastManager.createToast(this, alertAuthError);
            return;
        }

        // Отправляем запрос на логин пользователя
        createProgressAlert(true);

        authRequest.userLogin(authAdapter.editAuthEmail.getText().toString(),
                        authAdapter.editAuthPass.getText().toString()).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        createProgressAlert(false);

                        if (response.body() == null || response.code() != 200) {
                            ToastManager.createToast(getApplicationContext(), alertAuthError);
                            return;
                        }

                        // Обработка ответа
                Hawk.put("userData", response.body());
                        Hawk.put("userAuthStage", "inUse");
                        Hawk.put("accessToken", "Bearer " + response.body().accessToken);
                //Hawk.put("premium", response.body().isPremium);
//                Hawk.put("userAuthData",
//                        new Pair<String, String>
//                                (authAdapter.editAuthEmail.getText().toString(),
//                                        authAdapter.editAuthPass.getText().toString()));

                        // Переходим на экран восстановления бэкапа
                        startActivity(new Intent(AuthScreenActivity.this, RecoverDataActivity.class));
                        finish();
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Log.d("ответ2", t.toString());
                        createProgressAlert(false);
                        ToastManager.createToast(getApplicationContext(), alertAuthError);
                    }
                });
    }

    // Переходы между регистрация/авторизация фреймами
    public void authTypeSetClick(View v) {
        if (v.getId() == R.id.text_new_user) {
            authBinding.viewPager.setCurrentItem(0);
        } else {
            authBinding.viewPager.setCurrentItem(1);
        }
    }

    // Отображение фрейма восстановление пароля
    public void btnRecoverPassClick(View v) {
        if (authAdapter.frameAuth.getVisibility() == View.VISIBLE) {
            authAdapter.frameAuth.setVisibility(View.GONE);
            authAdapter.frameRecoverPass.setVisibility(View.VISIBLE);
        } else {
            authAdapter.frameAuth.setVisibility(View.VISIBLE);
            authAdapter.frameRecoverPass.setVisibility(View.GONE);
        }
    }

    // Отправка эмейла на сервер для восстановления пароля
    public void btnRecoverPassDoneClick(View v) {
//        String recoverEmail = authAdapter.editRestoreEmail.getText().toString();
//        if (!recoverEmail.contains("@") && !recoverEmail.contains(".")) {
//            ToastManager.createToast(this, "Произошла ошибка");
//            return;
//        }
//
//        createProgressAlert(true);
//        Call<Void> recoverPassCall = authRequest.userRecoverPass(recoverEmail);
//        RetrofitApi.getInstance().createRequest(recoverPassCall);
//        RetrofitApi.getInstance().setOnRequestListener(new RetrofitRequestCallback() {
//            @Override
//            public void onEventSuccess(Object responseObject) {
//                Response<Void> response = (Response<Void>) responseObject;
//                createProgressAlert(false);
//                if (response.code() == 200) {
//                    authAdapter.editRestoreEmail.setText("");
//                    ToastManager.createToast(AuthScreenActivity.this, "Проверьте Ваш почтовый ящик");
//                } else {
//                    ToastManager.createToast(AuthScreenActivity.this, "Произошла ошибка");
//                }
//
//            }
//
//            @Override
//            public void onEventFailure(Throwable error) {
//                ToastManager.createToast(AuthScreenActivity.this, "Произошла ошибка");
//                createProgressAlert(false);
//            }
//        });
    }

    // Менеджер АлертДиалога
    public void createProgressAlert(boolean setVisibility) {

        if (setVisibility) {
            alertDialog = ProgressDialog.show(this, "",
                    "Проверка...", true);
        } else {
            if (alertDialog != null) {
                alertDialog.dismiss();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnerInfo(){
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                authBinding.icLogo.setImageResource(R.drawable.ic_logo_sulpak);
                authBinding.imageTriangle.setBackgroundResource(R.color.colorSulpak);
                authBinding.frameMain.setBackgroundColor(getResources().getColor(R.color.colorSulpak));
                //currentColor = R.color.colorSulpak;
                break;
        }
    }
}
