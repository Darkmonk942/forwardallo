package com.virtualeducation.forwardpremium.alarms;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by DarKMonK on 19.08.2017.
 */

public class AlarmJobCreator implements JobCreator {

    @Override
    public Job create(String tag) {
        switch (tag) {
            case AlarmSyncJob.TAG:
                return new AlarmSyncJob();
            default:
                return null;
        }
    }
}