package com.virtualeducation.forwardpremium.alarms;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.media.RingtoneManager;
import com.orhanobut.hawk.Hawk;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.SplashScreen;

/**
 * Created by DarKMonK on 28.09.2017.
 */

public class JobSchedulerService extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {

        PendingIntent splashScreenIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SplashScreen.class).putExtra("isPush",true), 0);

        String text="";
        if(Hawk.get("language","ua").equals("ru")){
            text="Пришло время сохранить данные в облаке!";
        }else{
            text="Прийшов час зберегти данi у хмарi!";
        }

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Forward Backup")
                .setContentText(text)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(splashScreenIntent)
                .setSmallIcon(R.drawable.ic_splash_logo)
                .setShowWhen(true)
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat.from(this)
                .notify(1, notification);
        return true;
    }
    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}