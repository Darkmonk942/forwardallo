package com.virtualeducation.forwardpremium.alarms;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.RingtoneManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.SplashScreen;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by DarKMonK on 19.08.2017.
 */

public class AlarmSyncJob extends Job {

    public static final String TAG = "job_demo_tag";

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        PendingIntent splashScreenIntent = PendingIntent.getActivity(getContext(), 0,
                new Intent(getContext(), SplashScreen.class).putExtra("isPush",true), 0);
        String text="";
        if(Hawk.get("language","ua").equals("ru")){
           text="Пришло время сохранить данные в облаке!";
        }else{
           text="Прийшов час зберегти данi у хмарi!";
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), "2")
                .setSmallIcon(R.drawable.ic_splash_logo)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentTitle("Forward Backup")
                .setContentText(text)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(splashScreenIntent)
                .setShowWhen(true);

        /*Notification notification = new NotificationCompat.Builder(getContext())
                .setContentTitle("Forward Backup")
                .setContentText(text)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(splashScreenIntent)
                .setSmallIcon(R.drawable.ic_splash_logo)
                .setShowWhen(true)
                .build();*/

        NotificationManagerCompat.from(getContext())
                .notify(1, mBuilder.build());

        // run your job here
        return Result.SUCCESS;
    }

    public static void scheduleJob() {
        Long currentPeriodic=TimeUnit.DAYS.toMillis(1);
        switch (Hawk.get("backupCalendarType",1)){
            case 1:
                currentPeriodic=TimeUnit.DAYS.toMillis(1);
                break;
            case 2:
                currentPeriodic=TimeUnit.DAYS.toMillis(7);
                break;
            case 3:
                currentPeriodic=TimeUnit.DAYS.toMillis(30);
                break;
        }

        new JobRequest.Builder(AlarmSyncJob.TAG)
                .setPeriodic(currentPeriodic,TimeUnit.MINUTES.toMillis(20))
                .setUpdateCurrent(true)
                .setPersisted(true)
                .build()
                .schedule();
    }
}