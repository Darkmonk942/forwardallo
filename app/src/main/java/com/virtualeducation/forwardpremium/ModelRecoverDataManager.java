package com.virtualeducation.forwardpremium;

import android.Manifest;
import android.app.DownloadManager;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.listener.MultiFileDownloadListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;
import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.eventbus.DownloadDataEvent;
import com.virtualeducation.forwardpremium.eventbus.PhotoDownloadEvent;
import com.virtualeducation.forwardpremium.objects.CalendarObject;
import com.virtualeducation.forwardpremium.objects.CallObject;
import com.virtualeducation.forwardpremium.objects.CloudBackupDocs;
import com.virtualeducation.forwardpremium.objects.CloudBackupPhoto;
import com.virtualeducation.forwardpremium.objects.ContactObject;
import com.virtualeducation.forwardpremium.objects.SettingsObject;
import com.virtualeducation.forwardpremium.objects.SmsObject;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by DarKMonK on 07.08.2017.
 */

public class ModelRecoverDataManager {
    private Context context;

    public ModelRecoverDataManager(Context context) {
        this.context = context;
    }

    // Восстановление Контактов
    public void recoverContacts(List<ContactObject> backupContacts) {

        ArrayList<ContactObject> currentContacts= MainScreenActivity.allContactsList;

        /*
        if(currentContacts.size()!=0){
            // Проверяем контакт на наличие его в телефоне
            for (int i=0;i<backupContacts.size();i++){
                for (int j=0;j<currentContacts.size();j++){
                    if(backupContacts.get(i).contactName.equals(currentContacts.get(j).contactName)&&
                            backupContacts.get(i).contactNumber.equals(currentContacts.get(j).contactNumber)){
                        backupContacts.remove(backupContacts.get(i));
                    }
                }
            }
        }*/

        // Создаём нужные контакты
        for (ContactObject contactObject:backupContacts){
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
            Log.d("Смсочка0",contactObject.contactNumber);
            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                    .build());
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,contactObject.contactName)
                    .build());
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,contactObject.contactNumber)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
            try {
                context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

                // Добавляем созданный контакт в общий лист контактов
                MainScreenActivity.allContactsList.add(contactObject);
            } catch (Exception e) {

            }
        }
    }

    // Восстановление СМС
    public void recoverSms(List<SmsObject> backupSms) {

        ArrayList<SmsObject> currentSms=MainScreenActivity.allSmsList;

        // Проверяем смс на наличие его в телефоне
        /*if(currentSms.size()!=0){
            for (int i=0;i<backupSms.size();i++){
                for (int j=0;j<currentSms.size();j++){
                    if(backupSms.get(i).id.equals(currentSms.get(j).id)&&
                            backupSms.get(i).smsDate.equals(currentSms.get(j).smsDate)){
                        backupSms.remove(backupSms.get(i));
                    }
                }
            }
        }*/

        // Создаём список смсок
        for (SmsObject smsObject:backupSms){

            ContentValues smsValue = new ContentValues();
            if(smsObject.smsBody!=null){smsValue.put("body", smsObject.smsBody);}
            if(smsObject.smsNumber!=null){smsValue.put("address", smsObject.smsNumber);}
            if(smsObject.smsDate!=null){smsValue.put("date", smsObject.smsDate);}
            if(smsObject.smsType!=0){smsValue.put("type", String.valueOf(smsObject.smsType));}

            Uri uri=context.getContentResolver().insert(Uri.parse("content://sms"),smsValue);
            Log.d("Смсочка0",uri.toString());
            // Добавляем созданный смс в общий лист смс
            MainScreenActivity.allSmsList.add(smsObject);
        }

    }

    // Восстановление Звонков
    public void recoverCalls(List<CallObject> backupCalls) {

        ArrayList<CallObject> currentCalls=MainScreenActivity.allCallsList;

        // Проверяем звонок на наличие его в телефоне
        /*if(currentCalls.size()!=0){
            for (int i=0;i<backupCalls.size();i++){
                for (int j=0;j<currentCalls.size();j++){
                    if(backupCalls.get(i).id.equals(currentCalls.get(j).id)&&
                            backupCalls.get(i).callNumber.equals(currentCalls.get(j).callNumber)){
                        backupCalls.remove(backupCalls.get(i));
                    }
                }
            }
        }*/

        // Создаём список звонков
        for (CallObject callObject:backupCalls){
            ContentValues values = new ContentValues();
            values.put(CallLog.Calls.NUMBER, callObject.callNumber);
            values.put(CallLog.Calls.DATE, callObject.callDate+"000");
            values.put(CallLog.Calls.DURATION, callObject.callDuration);
            values.put(CallLog.Calls.TYPE, CallLog.Calls.OUTGOING_TYPE);
            values.put(CallLog.Calls.NEW, 1);
            values.put(CallLog.Calls.CACHED_NAME, callObject.callName);
            values.put(CallLog.Calls.CACHED_NUMBER_TYPE, 0);
            values.put(CallLog.Calls.CACHED_NUMBER_LABEL, callObject.callName);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                Log.d("uri","пермс");
                return;
            }
            Uri uri=context.getContentResolver().insert(CallLog.Calls.CONTENT_URI, values);
            Log.d("uri",uri.toString());

            // Добавляем созданный звонок в общий лист звонков
            MainScreenActivity.allCallsList.add(callObject);

        }
    }

    // Восстанавливаем фотки
    public void recoverPhoto(final List<CloudBackupPhoto> cloudBackupPhotos){
        final float progressPartPerc = 99f/ cloudBackupPhotos.size();
        Log.e("progressPartPerc", String.valueOf(progressPartPerc));
        for (int i=0;i<cloudBackupPhotos.size();i++){
            final int counter=i;

            Glide.with(context).load(cloudBackupPhotos.get(i).photoObject.url).asBitmap().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(final Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    String folderPath = Environment.getExternalStorageDirectory() + "/ForwardBackup";
                    File folder = new File(folderPath);
                    if (!folder.exists()) {
                        folder.mkdirs();
                    }

                    final File image = new File(folder.getAbsolutePath(), cloudBackupPhotos.get(counter).photoObject.name + ".jpeg");

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Log.e("ФотоYes", "ok");
                                FileOutputStream out = new FileOutputStream(image);
                                resource.compress(Bitmap.CompressFormat.JPEG, 100, out);
                                out.flush();
                                out.close();

                                if(counter == cloudBackupPhotos.size()-1) {
                                    EventBus.getDefault().post(new DownloadDataEvent(DownloadDataEvent.DOWNLOAD_FINISH,0, progressPartPerc));
                                }else{
                                    EventBus.getDefault().post(new DownloadDataEvent(DownloadDataEvent.DOWNLOAD_START,0, progressPartPerc*counter));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("ФотоNo", e.getMessage());
                            }
                        }
                    }).start();


                    //MediaStore.Images.Media.insertImage(context.getContentResolver(), resource, "forward","");
                    //EventBus.getDefault().post(new PhotoDownloadEvent(counter,counter==cloudBackupPhotos.size()-1));
                }
            });
        }
    }

    // Восстанавливаем календарь
    public void recoverCalendar(final List<CalendarObject> calendarObjects){

        ContentResolver cr = context.getContentResolver();

        for (int i=0;i<calendarObjects.size();i++){
            try {
                ContentValues values = new ContentValues();
                values.put(CalendarContract.Events.DTSTART, calendarObjects.get(i).startDate);
                values.put(CalendarContract.Events.DTEND, calendarObjects.get(i).endDate);
                values.put(CalendarContract.Events.TITLE, calendarObjects.get(i).title);
                if(calendarObjects.get(i).description!=null){
                    values.put(CalendarContract.Events.DESCRIPTION, calendarObjects.get(i).description);
                }else{
                    values.put(CalendarContract.Events.DESCRIPTION, " ");
                }

                values.put(CalendarContract.Events.CALENDAR_ID, calendarObjects.get(i).calendarId);
                values.put(CalendarContract.Events.EVENT_TIMEZONE, calendarObjects.get(i).timezone);
                Uri uri = CalendarContract.Events.CONTENT_URI;
                uri = uri.buildUpon().appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER,"true")
                        .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, calendarObjects.get(i).accountName)
                        .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, calendarObjects.get(i).accountType).build();
                cr.insert(uri, values);
            }catch (Exception e){}
        }
    }

    // Восстанавливаем настройки
    public void recoverSettings(final List<SettingsObject> settingsObjects){
        ContentResolver cr = context.getContentResolver();
        for (int i=0;i<settingsObjects.size();i++){
            try{
                Settings.System.putInt(cr,settingsObjects.get(i).name, Integer.valueOf(settingsObjects.get(i).value));
            }catch (Exception e){

            }
        }
    }

    // Восстанавливаем документы
    public void recoverDocuments(final List<CloudBackupDocs> docs){
        String docsLink[]=new String[docs.size()];
        String folderPath = Environment.getExternalStorageDirectory() + "/ForwardBackup";
        File folder = new File(folderPath);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        for (int i=0;i<docs.size();i++){
            docsLink[i]=docs.get(i).docObject.url;
        }

        FileLoader.multiFileDownload(context)
                .fromDirectory("ForwardBackup", FileLoader.DIR_EXTERNAL_PUBLIC)
                .progressListener(new MultiFileDownloadListener() {
                    @Override
                    public void onProgress(File downloadedFile, int progress, int totalFiles) {

                    }

                    @Override
                    public void onError(Exception e, int progress) {
                        super.onError(e, progress);
                    }
                }).loadMultiple(docsLink);
    }

}
