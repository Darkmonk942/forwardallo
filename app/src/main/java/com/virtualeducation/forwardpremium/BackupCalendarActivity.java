package com.virtualeducation.forwardpremium;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.alarms.JobSchedulerService;
import com.virtualeducation.forwardpremium.databinding.ActivityBackupCalendarBinding;

import java.util.concurrent.TimeUnit;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 24.07.2017.
 */

public class BackupCalendarActivity extends AppCompatActivity {
    ActivityBackupCalendarBinding backupCalendarBinding;

    int copyCalendarType;
    int cloudStorage=20;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        backupCalendarBinding= DataBindingUtil.setContentView(this,R.layout.activity_backup_calendar);

        setPartnersInfo();

        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(backupCalendarBinding.frameMain);

        // Получаем текущий график копирования
        copyCalendarType=Hawk.get("backupCalendarType",1);
        animateSetCalendarType(1,copyCalendarType);

        // Получаем вкл/выкл напоминание копирование
        boolean isNotifyEnable= Hawk.get("backupNotify",true);
        backupCalendarBinding.switchNotification.setChecked(isNotifyEnable);

        // Проверяем тип пользователя
        if(Hawk.get("premium",false)){
            cloudStorage=50;
            backupCalendarBinding.icCloudBlue.setImageResource(R.drawable.ic_main_logo_blue);
            backupCalendarBinding.icCloudWhite.setImageResource(R.drawable.ic_logo_50gb);
        }

        // Получаем кол-во занятого места на сервере
        float usedStorage=getIntent().getFloatExtra("usedStorage",10f);
        int usedStoragePercent=Math.round((usedStorage/cloudStorage)*100);
        backupCalendarBinding.progressDeviceStorage.setProgress(usedStoragePercent);
        backupCalendarBinding.textStorage.
                setText(getResources().getString(R.string.backup_cal_storage)+" "+String.valueOf(usedStorage)+" Gb");

        // Слушатель на вкл/выкл локальных нотификаций копирования
        backupCalendarBinding.switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                notificationManager(isChecked);
            }
        });
    }

    // Переход на главный экран
    public void backClick(View v){
        finish();
    }

    // Обработка клика для изменения графика копирования
    public void setCalendarType(View v){
        animateSetCalendarType(copyCalendarType,Integer.valueOf(v.getTag().toString()));
    }

    private void animateSetCalendarType(int start,int end){
        if(start==1&&end==1){
            backupCalendarBinding.lottieCalendar.setAnimation("lottie_slider_1_1.json");
            backupCalendarBinding.lottieCalendar.playAnimation();
        }
        if(start==end){
            return;
        }
        if(start==1&&end==2){
            backupCalendarBinding.lottieCalendar.setAnimation("lottie_slider_1_2.json");
            copyCalendarType=2;
        }
        if(start==1&&end==3){
            backupCalendarBinding.lottieCalendar.setAnimation("lottie_slider_1_3.json");
            copyCalendarType=3;
        }
        if(start==2&&end==3){
            backupCalendarBinding.lottieCalendar.setAnimation("lottie_slider_2_3.json");
            copyCalendarType=3;
        }
        if(start==2&&end==1){
            backupCalendarBinding.lottieCalendar.setAnimation("lottie_slider_2_1.json");
            copyCalendarType=1;
        }
        if(start==3&&end==1){
            backupCalendarBinding.lottieCalendar.setAnimation("lottie_slider_3_1.json");
            copyCalendarType=1;
        }
        if(start==3&&end==2){
            backupCalendarBinding.lottieCalendar.setAnimation("lottie_slider_3_2.json");
            copyCalendarType=2;
        }
        backupCalendarBinding.lottieCalendar.playAnimation();
        Hawk.put("backupCalendarType",copyCalendarType);
    }

    // Вкл/выкл нотификаций копирования
    private void notificationManager(boolean isEnable){
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancelAll();
        Long currentPeriodic=TimeUnit.DAYS.toMillis(1);
        switch (copyCalendarType){
            case 1:
                currentPeriodic=TimeUnit.DAYS.toMillis(1);
                break;
            case 2:
                currentPeriodic=TimeUnit.DAYS.toMillis(7);
                break;
            case 3:
                currentPeriodic=TimeUnit.DAYS.toMillis(30);
                break;
        }

        if(isEnable){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                jobScheduler.schedule(new JobInfo.Builder(1,
                        new ComponentName(this, JobSchedulerService.class))
                        .setPersisted(true)
                        .setPeriodic(currentPeriodic,JobInfo.getMinFlexMillis())
                        .build());
            }else{
                jobScheduler.schedule(new JobInfo.Builder(1,
                        new ComponentName(this, JobSchedulerService.class))
                        .setPersisted(true)
                        .setPeriodic(TimeUnit.MINUTES.toMillis(15))
                        .build());
            }
        }
        Hawk.put("backupNotify",isEnable);
    }

    // Сохраняем изменение в графике
    @Override
    public void onPause() {
        super.onPause();
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnersInfo() {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                backupCalendarBinding.frameHeader.setBackgroundColor(getResources().getColor(R.color.colorSulpak));
                backupCalendarBinding.textData.setTextColor(getResources().getColor(R.color.colorSulpak));
                backupCalendarBinding.frameHeader.setBackgroundColor(getResources().getColor(R.color.colorSulpak));
                backupCalendarBinding.icNotify.setImageResource(R.drawable.ic_notify_sulpak);
                backupCalendarBinding.icCloudBlue.setImageResource(R.drawable.ic_sulpak_red);
                break;
        }
    }
}
