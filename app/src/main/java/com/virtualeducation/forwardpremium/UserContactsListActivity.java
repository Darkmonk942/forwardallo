package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.virtualeducation.forwardpremium.adapters.ContactListAdapter;
import com.virtualeducation.forwardpremium.databinding.ActivityUserContactsListBinding;
import com.virtualeducation.forwardpremium.eventbus.ContactsListEvent;
import com.virtualeducation.forwardpremium.objects.ContactObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 13.07.2017.
 */

public class UserContactsListActivity extends AppCompatActivity {
    ActivityUserContactsListBinding userContactsListBinding;
    ContactListAdapter contactListAdapter;
    ArrayList<ContactObject> allContactsList=new ArrayList();
    ArrayList<ContactObject> allContactsListBackup=new ArrayList();
    Optimization optimization;
    int contactsCount=0;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        userContactsListBinding= DataBindingUtil.setContentView(this,R.layout.activity_user_contacts_list);

        // Оптимизация интерфейса
        optimization=new Optimization();
        optimization.Optimization(userContactsListBinding.frameMain);

        getAllContacts();
    }

    // Инициализация RecyclerView
    private void recyclerViewInit(){
        userContactsListBinding.recyclerContacts.setHasFixedSize(true);
        userContactsListBinding.recyclerContacts.setLayoutManager(new LinearLayoutManager(this));
        contactListAdapter=new ContactListAdapter(allContactsList,userContactsListBinding,this,optimization);
        contactListAdapter.setHasStableIds(true);
        userContactsListBinding.recyclerContacts.setAdapter(contactListAdapter);
    }

    // Переход на главный экран
    public void backClick(View v) {
        EventBus.getDefault().post(new ContactsListEvent(allContactsListBackup,true));
        finish();
    }

    // Подтверждаем выборанные звонки
    public void btnConfirmClick(View v){
        EventBus.getDefault().post(new ContactsListEvent(allContactsList,true));
        finish();
    }

    // Выбираем/Отключаем все звонки
    public void chooseAllCallsClick(View v){
        if(userContactsListBinding.icContactsCheck.getVisibility()==View.VISIBLE){
            contactListAdapter.currentContactsCount=0;
            userContactsListBinding.textContactsCount.setText("0/"+String.valueOf(contactsCount));
            userContactsListBinding.icContactsCheck.setVisibility(View.GONE);
            setAllCallsCheck(false);
        }else{
            contactListAdapter.currentContactsCount=contactsCount;
            userContactsListBinding.textContactsCount.setText(String.valueOf(contactsCount)+"/"+String.valueOf(contactsCount));
            userContactsListBinding.icContactsCheck.setVisibility(View.VISIBLE);
            setAllCallsCheck(true);
        }
    }

    // Изменяем чекер на всех звонках
    private void setAllCallsCheck(boolean isCheck){
        for (int i=0;i<allContactsList.size();i++){
            allContactsList.get(i).isChecked=isCheck;
        }
        contactListAdapter.notifyDataSetChanged();
    }

    // Получаем список контактов
    private void getAllContacts(){
        allContactsList.clear();
        allContactsList.addAll(MainScreenActivity.allContactsList);
        contactsCount=allContactsList.size();

        // Сохраняем весь список контактов для восстановления после изменений
        cloneContactsList();

        // Инициализация RecyclerView
        recyclerViewInit();

        // Получаем все выбранные вызовы
        for (int i=0;i<allContactsList.size();i++) {
            if (allContactsList.get(i).isChecked) {
                contactListAdapter.currentContactsCount = contactListAdapter.currentContactsCount + 1;
            }
        }

        userContactsListBinding.textContactsCount.setText(String.valueOf(contactListAdapter.currentContactsCount)+"/"+String.valueOf(contactsCount));
    }

    // Сохраняем весь список контактов для восстановления после изменений
    private void cloneContactsList(){
        Thread contactsThread = new Thread(
                new Runnable() {
                    public void run() {
                        for (ContactObject contactObject:allContactsList){
                            allContactsListBackup.add(contactObject.clone());
                        }
                    }
                }
        );
        contactsThread.start();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    // Передаём event на главную страницу в виде листа выбранных звонков
    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new ContactsListEvent(allContactsListBackup,true));
        finish();
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
