package com.virtualeducation.forwardpremium.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.virtualeducation.forwardpremium.Optimization;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.databinding.ActivityUserAppsListBinding;
import com.virtualeducation.forwardpremium.databinding.ActivityUserCallsListBinding;
import com.virtualeducation.forwardpremium.objects.AppsObject;
import com.virtualeducation.forwardpremium.objects.CallObject;
import com.virtualeducation.forwardpremium.objects.CloudBackupApps;

import java.util.ArrayList;

public class AppsListAdapter extends RecyclerView.Adapter<AppsListAdapter.ViewHolder> {
    private ArrayList<CloudBackupApps> allAppsList;
    private ActivityUserCallsListBinding userCallsListBinding;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView appName;
        TextView vendorName;
        ImageView icApp;
        Button btnInstall;
        Button btnInstallOff;

        public ViewHolder(FrameLayout appsFrame) {
            super(appsFrame);
            appName=(TextView)appsFrame.findViewById(R.id.text_app_name);
            vendorName=(TextView)appsFrame.findViewById(R.id.text_vendor_name);
            icApp=(ImageView) appsFrame.findViewById(R.id.ic_app);
            btnInstall=(Button)appsFrame.findViewById(R.id.btn_install);
            btnInstallOff=(Button)appsFrame.findViewById(R.id.btn_install_off);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AppsListAdapter(ArrayList<CloudBackupApps> allAppsList, ActivityUserAppsListBinding activityBinding,
                           Context context) {
        this.allAppsList=allAppsList;
        this.userCallsListBinding=userCallsListBinding;
        this.context=context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AppsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {

        FrameLayout callsFrame = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_apps, parent, false);

        AppsListAdapter.ViewHolder vh = new AppsListAdapter.ViewHolder(callsFrame);
        return vh;
    }

    @Override
    public void onBindViewHolder(AppsListAdapter.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        final AppsListAdapter.ViewHolder currentHolder=holder;
        final int currentPosition=position;

        final CloudBackupApps cloudBackupApps=allAppsList.get(position);
        if(cloudBackupApps.appName!=null){
            holder.appName.setText(cloudBackupApps.appName);
        }
        if(cloudBackupApps.appsList.appPackage!=null){
            holder.vendorName.setText(cloudBackupApps.appsList.appPackage);
        }
        if(cloudBackupApps.appsList.appIconModel!=null){
            Glide.with(context)
                    .load(cloudBackupApps.appsList.appIconModel.icUrl)
                    .into(holder.icApp);
        }

        if(!cloudBackupApps.appsList.isInstall){
            holder.btnInstall.setVisibility(View.VISIBLE);
            holder.btnInstallOff.setVisibility(View.GONE);
            holder.btnInstall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id="+cloudBackupApps.appsList.appPackage));
                    context.startActivity(intent);
                }
            });
        }else{
            holder.btnInstall.setVisibility(View.GONE);
            holder.btnInstallOff.setVisibility(View.VISIBLE);
        }

    }

    // Кол-во элементов листа
    @Override
    public int getItemCount() {
        return allAppsList.size();
    }
}