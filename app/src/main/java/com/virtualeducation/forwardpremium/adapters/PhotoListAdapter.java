package com.virtualeducation.forwardpremium.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.virtualeducation.forwardpremium.Optimization;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.eventbus.PhotoChangeCount;
import com.virtualeducation.forwardpremium.objects.PhotoObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 27.07.2017.
 */

public class PhotoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public ArrayList<PhotoObject> allPhotoList;
    private final RequestManager glide;
    private Context context;
    Optimization optimization;


    // Provide a suitable constructor (depends on the kind of dataset)
    public PhotoListAdapter(ArrayList<PhotoObject> allPhotoList,Context context,
                            Optimization optimization,
                            RequestManager glide) {
        this.allPhotoList=allPhotoList;
        this.context=context;
        this.optimization=optimization;
        this.glide=glide;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        FrameLayout photoFrame;

        if(viewType==0){
            photoFrame = (FrameLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_category_photo, parent, false);

            optimization.Optimization(photoFrame);
            return new TitleViewHolder(photoFrame);
        }else{
            photoFrame = (FrameLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_photo, parent, false);

            optimization.Optimization(photoFrame);
            return new PhotoViewHolder(photoFrame);
        }
    }


    public static class PhotoViewHolder extends RecyclerView.ViewHolder {
        FrameLayout photoFrame;
        ImageView photoImage;
        final ImageView checkImage;
        public PhotoViewHolder(FrameLayout photoFrame) {
            super(photoFrame);
            this.photoFrame=photoFrame;
            photoImage=(ImageView) photoFrame.findViewById(R.id.img_photo);
            checkImage=(ImageView) photoFrame.findViewById(R.id.ic_check);
        }
    }

    public static class TitleViewHolder extends RecyclerView.ViewHolder {
        FrameLayout photoFrame;
        ImageView categoryCheck;
        ImageView categoryCheckField;
        TextView titleText;
        public TitleViewHolder(FrameLayout photoFrame) {
            super(photoFrame);
            this.photoFrame=photoFrame;
            titleText=(TextView)photoFrame.findViewById(R.id.text_category);
            categoryCheck=(ImageView)photoFrame.findViewById(R.id.ic_check_all);
            categoryCheckField=(ImageView)photoFrame.findViewById(R.id.ic_check_all_field);

        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        holder.setIsRecyclable(false);

        if(holder.getItemViewType()==0){
            final TitleViewHolder photoHolder=(TitleViewHolder)holder;

            if(allPhotoList.get(position).isChecked){
                photoHolder.categoryCheck.setVisibility(View.VISIBLE);
            }else{
                photoHolder.categoryCheck.setVisibility(View.GONE);
            }


            photoHolder.titleText.setText(allPhotoList.get(position).photoTitleCategory);
            photoHolder.categoryCheckField.setTag(allPhotoList.get(position).photoTitleCategory);
            photoHolder.categoryCheckField.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(photoHolder.categoryCheck.getVisibility()==View.VISIBLE){
                        photoHolder.categoryCheck.setVisibility(View.GONE);
                        allPhotoList.get(position).isChecked=false;
                    }else{
                        photoHolder.categoryCheck.setVisibility(View.VISIBLE);
                        allPhotoList.get(position).isChecked=true;
                    }

                    setCategoryCheck(photoHolder.categoryCheck.getVisibility()==View.VISIBLE,
                            photoHolder.categoryCheckField.getTag().toString());
                }
            });

        }else{
            final PhotoViewHolder photoHolder=(PhotoViewHolder)holder;

            // Проверка на вкл/выкл фотки для копирования
            if(allPhotoList.get(position).isChecked){
                photoHolder.checkImage.setVisibility(View.VISIBLE);
            }else{
                photoHolder.checkImage.setVisibility(View.GONE);
            }

            /*Picasso.with(context)
                .load("file://"+allPhotoList.get(position).photoThumbnail)
                .config(Bitmap.Config.RGB_565)
                .resize(200,200)
                .centerCrop()
                .into(photoHolder.photoImage);*/

            glide.load("file://"+allPhotoList.get(position).photoThumbnail)
                    .dontAnimate()
                    //.thumbnail( 0.3f )
                    .sizeMultiplier(0.7f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(photoHolder.photoImage);



            // Клик на добавление/удаление фотки для копирования
            photoHolder.photoImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(photoHolder.checkImage.getVisibility()==View.VISIBLE){
                        photoHolder.checkImage.setVisibility(View.GONE);
                        allPhotoList.get(position).isChecked=false;
                        EventBus.getDefault().post(new PhotoChangeCount(-1));
                    }else{
                        photoHolder.checkImage.setVisibility(View.VISIBLE);
                        allPhotoList.get(position).isChecked=true;
                        EventBus.getDefault().post(new PhotoChangeCount(1));
                    }
                }
            });
        }

    }


    // Кол-во элементов листа
    @Override
    public int getItemCount() {
        return allPhotoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(allPhotoList.get(position).photoTitleCategory!=null){
            return 0;
        }else{
            return 1;
        }
    }

    // Снимаем/Добавляем чек на все фотки выбранной категории
    private void setCategoryCheck(boolean isCheck,String title){
        int counter=0;
        for (int i=0;i<allPhotoList.size();i++){
            if(allPhotoList.get(i).photoCategory.equals(title)){
                if(allPhotoList.get(i).isChecked!=isCheck){
                    allPhotoList.get(i).isChecked=isCheck;
                    counter++;
                }
            }
        }

        if(isCheck){
            EventBus.getDefault().post(new PhotoChangeCount(counter));
        }else{
            EventBus.getDefault().post(new PhotoChangeCount(-counter));
        }
        notifyDataSetChanged();
    }

    // Снимаем/добавляем Все чекеры во всех категориях
    public void setAllCheckFromAllCategory(boolean isCheck){

        for (int i=0;i<allPhotoList.size();i++){
            allPhotoList.get(i).isChecked=isCheck;
        }
        notifyDataSetChanged();
    }

    // Снимаем/добавляем все чекеры с фоток текущей категории
    /*private void setAllCheck(){
        boolean isCheck=true;
        int checkedSetCount=0;
        if (icCheck.getVisibility()==View.VISIBLE){
            icCheck.setVisibility(View.GONE);
            isCheck=false;
        }else{
            //icCheck.setVisibility(View.VISIBLE);
        }

        for (int i=0;i<allPhotoList.size();i++){
            if(allPhotoList.get(i).isChecked!=isCheck){
                allPhotoList.get(i).isChecked=isCheck;
                checkedSetCount=checkedSetCount+1;
            }
        }
        notifyDataSetChanged();

        // Отправляем эвент с кол-вом изменений выбранных фото
        if(!isCheck){
            checkedSetCount=-checkedSetCount;
        }
        EventBus.getDefault().post(new PhotoChangeCount(checkedSetCount));
    }*/

}
