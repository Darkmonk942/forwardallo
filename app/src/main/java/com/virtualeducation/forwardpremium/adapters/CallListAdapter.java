package com.virtualeducation.forwardpremium.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.virtualeducation.forwardpremium.Optimization;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.databinding.ActivityUserCallsListBinding;
import com.virtualeducation.forwardpremium.objects.CallObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 10.07.2017.
 */

    public class CallListAdapter extends RecyclerView.Adapter<CallListAdapter.ViewHolder> {
    private ArrayList<CallObject> allCallsList;
    private ActivityUserCallsListBinding userCallsListBinding;
    Optimization optimization;
    private int callsCount;
    public int currentCallsCount;
    public static class ViewHolder extends RecyclerView.ViewHolder {

        FrameLayout callsFrame;
        TextView callName;
        TextView callDate;
        ImageView checkImage;
        ImageView bgCalls;
        public ViewHolder(FrameLayout callsFrame) {
            super(callsFrame);
            this.callsFrame=callsFrame;
            callName=(TextView)callsFrame.findViewById(R.id.text_call_name);
            callDate=(TextView)callsFrame.findViewById(R.id.text_call_date);
            checkImage=(ImageView) callsFrame.findViewById(R.id.ic_check);
            bgCalls=(ImageView) callsFrame.findViewById(R.id.bg_calls);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CallListAdapter(ArrayList<CallObject> allCallsList, ActivityUserCallsListBinding userCallsListBinding,
                           Optimization optimization) {
        this.allCallsList=allCallsList;
        this.userCallsListBinding=userCallsListBinding;
        this.optimization=optimization;
        callsCount=allCallsList.size();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CallListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        FrameLayout callsFrame = (FrameLayout)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_calls, parent, false);

        optimization.Optimization(callsFrame);
        ViewHolder vh = new ViewHolder(callsFrame);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,int position) {
        holder.setIsRecyclable(false);
        final ViewHolder currentHolder=holder;
        final int currentPosition=position;

        CallObject callObject=allCallsList.get(position);
        if(callObject.callName!=null){
            holder.callName.setText(callObject.callName);
        }else{
            holder.callName.setText(callObject.callNumber);
        }
        holder.callDate.setText(callObject.callDateDayTime);
        holder.checkImage.setVisibility(View.VISIBLE);

        if(callObject.isChecked){
            holder.checkImage.setVisibility(View.VISIBLE);
        }else{
            holder.checkImage.setVisibility(View.GONE);
        }

        // Снимаем/добавляем чек на текущий звонок
        holder.bgCalls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(allCallsList.get(currentPosition).isChecked){
                    currentCallsCount=currentCallsCount-1;
                    userCallsListBinding.textCallsCount.setText(String.valueOf(currentCallsCount+"/"+String.valueOf(callsCount)));
                    currentHolder.checkImage.setVisibility(View.GONE);
                    allCallsList.get(currentPosition).isChecked=false;

                    if(userCallsListBinding.icCallsCheck.getVisibility()==View.VISIBLE){
                        userCallsListBinding.icCallsCheck.setVisibility(View.GONE);
                    }
                }else{
                    currentCallsCount=currentCallsCount+1;
                    userCallsListBinding.textCallsCount.setText(String.valueOf(currentCallsCount+"/"+String.valueOf(callsCount)));
                    currentHolder.checkImage.setVisibility(View.VISIBLE);
                    allCallsList.get(currentPosition).isChecked=true;

                    if(currentCallsCount==callsCount){
                        userCallsListBinding.icCallsCheck.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    // Кол-во элементов листа
    @Override
    public int getItemCount() {
        return allCallsList.size();
    }
}
