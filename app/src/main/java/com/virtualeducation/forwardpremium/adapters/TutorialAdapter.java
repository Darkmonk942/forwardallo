package com.virtualeducation.forwardpremium.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.virtualeducation.forwardpremium.BuildConfig;
import com.virtualeducation.forwardpremium.Optimization;
import com.virtualeducation.forwardpremium.Partners;
import com.virtualeducation.forwardpremium.R;

/**
 * Created by DarKMonK on 02.08.2017.
 */

public class TutorialAdapter extends PagerAdapter {

    Context context;
    Optimization optimization;
    boolean isPremium;
    public TutorialAdapter(Context context,boolean isPremium) {
        optimization=new Optimization();
        this.context = context;
        this.isPremium=isPremium;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        View layout=null;
        switch (position){
            case 0:
                layout = View.inflate(context, R.layout.pager_frame_tutorial1, null);
                TextView textAboutCloud=(TextView)layout.findViewById(R.id.text_about_cloud);
                ImageView icCloud1=(ImageView)layout.findViewById(R.id.ic_cloud);
                setPartnerIcon(icCloud1);
                break;
            case 1:
                layout = View.inflate(context, R.layout.pager_frame_tutorial2, null);
                ImageView icCloud2=(ImageView)layout.findViewById(R.id.ic_cloud);
                setPartnerIcon(icCloud2);
                break;
            case 2:
                layout = View.inflate(context, R.layout.pager_frame_tutorial3, null);
                ImageView icCloud3=(ImageView)layout.findViewById(R.id.ic_cloud);
                setPartnerIcon(icCloud3);
                break;
        }

        optimization.Optimization((FrameLayout)layout.getRootView());
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private void setPartnerIcon(ImageView imageView){
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                imageView.setImageResource(R.drawable.ic_tutor_sulpak);
                break;
        }
    }

}
