package com.virtualeducation.forwardpremium.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.AuthScreenActivity;
import com.virtualeducation.forwardpremium.BuildConfig;
import com.virtualeducation.forwardpremium.MainScreenActivity;
import com.virtualeducation.forwardpremium.Optimization;
import com.virtualeducation.forwardpremium.Partners;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.RecoverDataActivity;
import com.virtualeducation.forwardpremium.ToastManager;
import com.virtualeducation.forwardpremium.objects.User;
import com.virtualeducation.forwardpremium.retrofit.PostUserAuthRequest;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DarKMonK on 03.07.2017.
 */

public class AuthAdapter extends PagerAdapter {
    Context context;
    public TextInputEditText editRegEmail;
    public TextInputEditText editRegPass;
    public TextInputEditText editAuthEmail;
    public TextInputEditText editAuthPass;
    public TextInputEditText editPhoneCode;
    public TextInputEditText editPhone;
    public TextInputEditText editSms1;
    public TextInputEditText editSms2;
    public TextInputEditText editSms3;
    public TextInputEditText editSms4;
    public TextInputEditText editSms5;
    public TextInputEditText editSms6;
    public TextInputEditText editSms7;
    public TextInputEditText editSms8;
    public TextInputEditText editEnterPass;
    public TextInputEditText editAuthPhoneCode;
    public TextInputEditText editAuthPhone;
    public TextInputEditText editAuthPassword;
    public TextInputEditText editAuthUserPassword;
    public TextInputEditText editRecoverCountryCode;
    public TextInputEditText editRecoverPhone;
    public TextInputLayout layoutPassNew;
    public TextInputLayout layoutPassNewRepeat;


    public FrameLayout frameAuth;
    public FrameLayout frameRecoverPass;

    public FrameLayout framePhone;
    public FrameLayout frameSms;
    public FrameLayout frameEmail;
    public FrameLayout framePass;
    public FrameLayout frameEnterPass;
    public FrameLayout frameNewPass;
    public TextInputEditText editNewPass;

    public TextView textPromo;
    public TextView textCloud;
    public TextView textAbout;
    public TextView textAboutNewPass;
    public TextView textAboutNewPhone;
    public TextView textCodeSms;

    public Button btnOk;
    public Button btnNewPass;
    public Button btnActivate;
    public Button btnPassRecover;
    public Button btnSetPassOk;
    public Button btnsetPassBack;


    int phoneLength =0;

    Optimization optimization;

    public AuthAdapter(Context context,Optimization optimization) {
        this.optimization=optimization;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        View layout=null;
        if(position==0) { // Фрейм регистрации
            layout = View.inflate(context, R.layout.pager_frame_registration, null);
            editRegEmail=(TextInputEditText)layout.findViewById(R.id.edit_reg_email);
            editRegPass=(TextInputEditText)layout.findViewById(R.id.edit_reg_pass);
            editPhoneCode=(TextInputEditText)layout.findViewById(R.id.edit_country_code);
            editPhone=(TextInputEditText)layout.findViewById(R.id.edit_phone);
            editAuthPassword=(TextInputEditText)layout.findViewById(R.id.edit_pass);
            framePhone=(FrameLayout)layout.findViewById(R.id.frame_phone);
            frameEmail=(FrameLayout)layout.findViewById(R.id.frame_email);
            framePass=(FrameLayout)layout.findViewById(R.id.frame_pass);
            frameEnterPass=(FrameLayout)layout.findViewById(R.id.frame_enter_pass);
            textPromo=(TextView)layout.findViewById(R.id.text_promo);
            btnActivate=(Button)layout.findViewById(R.id.btn_activate);
            btnNewPass=(Button) layout.findViewById(R.id.btn_reg_ok);
            textAbout=(TextView) layout.findViewById(R.id.text_about);
            textAboutNewPass=(TextView)layout.findViewById(R.id.text_about_new_pass);
            textAboutNewPhone=(TextView)layout.findViewById(R.id.text_about_phone);

            btnNewPass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   userRegistration();
                }
            });

            optimization.Optimization((FrameLayout)layout);

            //Hawk.put("promocode", "232");
            if(!Hawk.get("promocode","").equals("")){
                textPromo.setVisibility(View.GONE);
                btnActivate.setVisibility(View.GONE);
                showSmsFrame();
            }
        }else{ // Фрейм авторизации
            layout = View.inflate(context, R.layout.pager_frame_auth, null);
            editAuthEmail=(TextInputEditText)layout.findViewById(R.id.edit_auth_email);
            editAuthPass=(TextInputEditText)layout.findViewById(R.id.edit_auth_pass);
            frameAuth=(FrameLayout)layout.findViewById(R.id.frame_auth_inner);
            frameRecoverPass=(FrameLayout)layout.findViewById(R.id.frame_pass_recover);
            editAuthPhoneCode=(TextInputEditText)layout.findViewById(R.id.edit_country_code);
            editAuthPhone=(TextInputEditText)layout.findViewById(R.id.edit_phone);
            editRecoverCountryCode=(TextInputEditText)layout.findViewById(R.id.edit_recover_country_code);
            editRecoverPhone=(TextInputEditText)layout.findViewById(R.id.edit_recover_phone);
            frameSms=(FrameLayout)layout.findViewById(R.id.frame_sms);
            textCloud = (TextView)layout.findViewById(R.id.text_cloud);
            btnOk=(Button)layout.findViewById(R.id.btn_ok);
            btnPassRecover=(Button)layout.findViewById(R.id.btn_pass_recover);
            Button btnRecoverBack = (Button)layout.findViewById(R.id.btn_recover_back);
            Button btnRecoverOk = (Button)layout.findViewById(R.id.btn_recover_ok);
            btnSetPassOk=(Button)layout.findViewById(R.id.btn_set_pass_ok);
            btnsetPassBack=(Button)layout.findViewById(R.id.btn_set_pass_back);
            textCodeSms=(TextView)layout.findViewById(R.id.text_code_sms);

            editSms1 = (TextInputEditText)layout.findViewById(R.id.edit_sms_1);
            editSms2 = (TextInputEditText)layout.findViewById(R.id.edit_sms_2);
            editSms3 = (TextInputEditText)layout.findViewById(R.id.edit_sms_3);
            editSms4 = (TextInputEditText)layout.findViewById(R.id.edit_sms_4);
            editSms5 = (TextInputEditText)layout.findViewById(R.id.edit_sms_5);
            editSms6 = (TextInputEditText)layout.findViewById(R.id.edit_sms_6);
            editSms7 = (TextInputEditText)layout.findViewById(R.id.edit_sms_7);
            editSms8 = (TextInputEditText)layout.findViewById(R.id.edit_sms_8);
            editEnterPass = (TextInputEditText)layout.findViewById(R.id.edit_pass);
            layoutPassNew = (TextInputLayout)layout.findViewById(R.id.layout_edit_reg_pass);
            layoutPassNewRepeat = (TextInputLayout)layout.findViewById(R.id.layout_edit_repeat_pass);
            frameNewPass=(FrameLayout)layout.findViewById(R.id.frame_enter_new_pass);
            editNewPass = (TextInputEditText)layout.findViewById(R.id.edit_new_pass);

            setCodeTextWatcher(editRecoverCountryCode);
            setCodeTextWatcher(editAuthPhoneCode);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userAuth();
                }
            });
            btnPassRecover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFrameRecoverPass();
                }
            });
            btnRecoverBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFrameAuth();
                }
            });
            btnRecoverOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userRequestSms();
                }
            });
            btnSetPassOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userSetPassword();
                }
            });
            btnsetPassBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFrameRecoverPass();
                }
            });

            setSmsTextWatcher(editSms1);
            setSmsTextWatcher(editSms2);
            setSmsTextWatcher(editSms3);
            setSmsTextWatcher(editSms4);
            setSmsTextWatcher(editSms5);
            setSmsTextWatcher(editSms6);
            setSmsTextWatcher(editSms7);
            setSmsTextWatcher(editSms8);

            optimization.Optimization(frameAuth);
            optimization.Optimization(frameRecoverPass);
        }

        setPartnerInfo();

        collection.addView(layout);
        return layout;
    }

    private void showSmsFrame() {
        framePhone.setVisibility(View.VISIBLE);
        textAboutNewPhone.setVisibility(View.VISIBLE);
        btnNewPass.setVisibility(View.VISIBLE);
        frameEnterPass.setVisibility(View.VISIBLE);

        setCodeTextWatcher(editPhoneCode);
        setPhoneTextWatcher(editPhone);
    }

    private void showFrameRecoverPass() {
        frameAuth.setVisibility(View.GONE);
        btnOk.setVisibility(View.GONE);
        btnPassRecover.setVisibility(View.GONE);
        frameRecoverPass.setVisibility(View.VISIBLE);
        btnSetPassOk.setVisibility(View.GONE);
        btnsetPassBack.setVisibility(View.GONE);
        frameSms.setVisibility(View.GONE);
        textCodeSms.setVisibility(View.GONE);
        frameNewPass.setVisibility(View.GONE);
    }

    private void showFrameAuth() {
        frameAuth.setVisibility(View.VISIBLE);
        btnOk.setVisibility(View.VISIBLE);
        btnPassRecover.setVisibility(View.VISIBLE);
        frameRecoverPass.setVisibility(View.GONE);
        frameNewPass.setVisibility(View.GONE);
        frameSms.setVisibility(View.GONE);
        textCodeSms.setVisibility(View.GONE);
        btnSetPassOk.setVisibility(View.GONE);
        btnsetPassBack.setVisibility(View.GONE);
    }

    private void showFrameSetPassword() {
        frameSms.setVisibility(View.VISIBLE);
        textCodeSms.setVisibility(View.VISIBLE);
        frameRecoverPass.setVisibility(View.GONE);
        btnSetPassOk.setVisibility(View.VISIBLE);
        btnsetPassBack.setVisibility(View.VISIBLE);
        frameNewPass.setVisibility(View.VISIBLE);
    }

    private void showEnterCodeFrame() {
        framePhone.setVisibility(View.GONE);
        btnActivate.setVisibility(View.GONE);
        frameSms.setVisibility(View.VISIBLE);
        textAbout.setVisibility(View.GONE);
        textAboutNewPass.setVisibility(View.VISIBLE);
        btnOk.setVisibility(View.GONE);
        textAboutNewPhone.setVisibility(View.GONE);
        layoutPassNew.setVisibility(View.VISIBLE);
        layoutPassNewRepeat.setVisibility(View.VISIBLE);
        btnNewPass.setVisibility(View.VISIBLE);
    }



    // Регистрация нового пользователя
    private void userRegistration() {
        PostUserAuthRequest regRequest = RetrofitApi.getInstance().retrofit.create(PostUserAuthRequest.class);

        final String code = Hawk.get("promocode", "");
        final String phone = editPhoneCode.getText().toString() + editPhone.getText().toString().replaceAll(" ","");
        final String password = editAuthPassword.getText().toString().trim();

        if(phone.length() < 10){
            ToastManager.createToast(context, "Введите номер телефона");
            return;
        }
        if(password.length() < 8){
            ToastManager.createToast(context, "Пароль не может быть меньше восьми символов");
            Log.e("вава","dsfdf");
            return;
        }

        regRequest.userPhoneRegistration(code, phone, password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.code() == 200 && response.body()!=null){
                    // Сохраняем все данные пользователя
                    Hawk.put("userData", response.body());
                    Hawk.put("userAuthStage", "inUse");
                    Hawk.put("accessToken", "Bearer " + response.body().accessToken);
                    Hawk.put("promocode", "");

                    // Переходим на главный экран
                    context.startActivity(new Intent(context, MainScreenActivity.class));
                    ((Activity)context).finish();
                }else{
                    ToastManager.createToast(context, "Произошла ошибка");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                ToastManager.createToast(context, "Отсутствует интернет соединение или доступ к серверу");
            }
        });
    }

    private void userAuth() {
        PostUserAuthRequest authRequest = RetrofitApi.getInstance().retrofit.create(PostUserAuthRequest.class);

        final String phone = editAuthPhoneCode.getText().toString() + editAuthPhone.getText().toString().replaceAll(" ","");
        final String password = editEnterPass.getText().toString().trim();

        if(phone.length() < 10){
            ToastManager.createToast(context, "Введите номер телефона");
            Log.e("Лог","phone");
            return;
        }
        if(TextUtils.isEmpty(password)){
            ToastManager.createToast(context, "Введите пароль");
            Log.e("Лог","password");
            return;
        }

        authRequest.userAuthByPhone(phone, password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful() || response.body() != null){
                    Hawk.put("userData", response.body());
                    Hawk.put("userAuthStage", "inUse");
                    Hawk.put("accessToken", "Bearer " + response.body().accessToken);

                    // Переходим на экран восстановления даных
                    context.startActivity(new Intent(context, RecoverDataActivity.class));
                    ((Activity)context).finish();
                }else {
                    ToastManager.createToast(context, "Данные для входа введены неверно");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                ToastManager.createToast(context, "Отсутствует интернет соединение или доступ к серверу");
            }
        });
    }

    private void userRequestSms() {
        PostUserAuthRequest authRequest = RetrofitApi.getInstance().retrofit.create(PostUserAuthRequest.class);

        final String phone = editRecoverCountryCode.getText().toString() + editRecoverPhone.getText().toString().replaceAll(" ","");

        if(phone.length() < 10){
            ToastManager.createToast(context, "Введите номер телефона");
            Log.e("Лог","phone");
            return;
        }

        authRequest.userRequestSms(phone).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    ToastManager.createToast(context, "На ваш телефон отправлена смс для изменения пароля");
                    showFrameSetPassword();
                }else{
                    ToastManager.createToast(context, "Данный телефон отсутствует в базе");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                ToastManager.createToast(context, "Отсутствует интернет соединение или доступ к серверу");
            }
        });
    }

    private void userSetPassword() {
        PostUserAuthRequest authRequest = RetrofitApi.getInstance().retrofit.create(PostUserAuthRequest.class);

        final String phone = editRecoverCountryCode.getText().toString() + editRecoverPhone.getText().toString().replaceAll(" ","");
        final String code = editSms1.getText().toString() + editSms2.getText().toString() + editSms3.getText().toString() + editSms4.getText().toString()
                + editSms5.getText().toString() + editSms6.getText().toString() + editSms7.getText().toString() + editSms8.getText().toString();
        final String password = editNewPass.getText().toString();

        if(code.length() < 8){
            ToastManager.createToast(context, "Введите код из смс");
            return;
        }
        if(password.length() < 8){
            ToastManager.createToast(context, "Пароль не может быть меньше восьми символов");
            return;
        }

        authRequest.userSetPassword(phone,password, code).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()) {
                    ToastManager.createToast(context, "Войдите с новым паролем");
                    showFrameAuth();
                }else{
                    ToastManager.createToast(context, "Поля заполнены неверно");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                ToastManager.createToast(context, "Отсутствует интернет соединение или доступ к серверу");
            }
        });
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private void setCodeTextWatcher(final TextInputEditText editCodeInner) {
        editCodeInner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable text) {
                if(text.length() == 0){
                    editCodeInner.setText("+");
                    editCodeInner.setSelection(1);
                }
                if(text.length() == 4){
                    View next = editCodeInner.focusSearch(View.FOCUS_RIGHT);
                    next.requestFocus();
                }
            }
        });
    }

    private void setPhoneTextWatcher(final TextInputEditText editPhoneText) {
        editPhoneText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence text, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable text) {
                if (phoneLength <= editPhoneText.getText().toString().length()
                        &&(editPhoneText.getText().toString().length()==3
                        ||editPhoneText.getText().toString().length()==7
                        ||editPhoneText.getText().toString().length()==11)){
                    editPhoneText.setText(editPhoneText.getText().toString()+" ");
                    int pos = editPhoneText.getText().length();
                    editPhoneText.setSelection(pos);
                }else if (phoneLength >= editPhoneText.getText().toString().length()
                        &&(editPhoneText.getText().toString().length()==3
                        ||editPhoneText.getText().toString().length()==7
                        ||editPhoneText.getText().toString().length()==11)){
                    editPhoneText.setText(editPhoneText.getText().toString().substring(0,editPhoneText.getText().toString().length()-1));
                    int pos = editPhoneText.getText().length();
                    editPhoneText.setSelection(pos);
                }
                phoneLength = editPhoneText.getText().toString().length();
            }
        });
    }



    private void setSmsTextWatcher(final TextInputEditText editText) {
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL){
                        switch (editText.getId()){
                            case R.id.edit_sms_1:
                                break;
                            case R.id.edit_sms_2:
                                if(editText.getText().length() == 0){
                                    //editSms1.setText("");
                                    editSms1.requestFocus();
                                }
                                break;
                            case R.id.edit_sms_3:
                                if(editText.getText().length() == 0){
                                   // editSms2.setText("");
                                    editSms2.requestFocus();
                                }
                                break;
                            case R.id.edit_sms_4:
                                if(editText.getText().length() == 0){
                                    //editSms3.setText("");
                                    editSms3.requestFocus();
                                }
                                break;
                            case R.id.edit_sms_5:
                                if(editText.getText().length() == 0){
                                   // editSms4.setText("");
                                    editSms4.requestFocus();
                                }
                                break;
                            case R.id.edit_sms_6:
                                if(editText.getText().length() == 0){
                                    //editSms5.setText("");
                                    editSms5.requestFocus();
                                }
                                    break;
                            case R.id.edit_sms_7:
                                if(editText.getText().length() == 0){
                                   // editSms6.setText("");
                                    editSms6.requestFocus();
                                }
                                break;
                            case R.id.edit_sms_8:
                                if(editText.getText().length() == 0){
                                   // editSms7.setText("");
                                    editSms7.requestFocus();
                                }
                                break;
                        }
                }

                return false;
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable text) {
                if (text.length() == 1) {
                    switch (editText.getId()){
                        case R.id.edit_sms_1:
                            editSms2.requestFocus();
                            break;
                        case R.id.edit_sms_2:
                            editSms3.requestFocus();
                            break;
                        case R.id.edit_sms_3:
                            editSms4.requestFocus();
                            break;
                        case R.id.edit_sms_4:
                            editSms5.requestFocus();
                            break;
                        case R.id.edit_sms_5:
                            editSms6.requestFocus();
                            break;
                        case R.id.edit_sms_6:
                            editSms7.requestFocus();
                            break;
                        case R.id.edit_sms_7:
                            editSms8.requestFocus();
                            break;
                    }
                }


            }
        });
    }

    private void setPartnerInfo(){
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                try{
                    textAbout.setText(context.getString(R.string.start_screen_about_app_sulpak));
                    textCloud.setText(context.getString(R.string.recover_screen_recover_about_sulpak));
                }catch (Exception e){}
                break;
        }
    }

}
