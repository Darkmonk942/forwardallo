package com.virtualeducation.forwardpremium.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.virtualeducation.forwardpremium.Optimization;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.databinding.ActivityUserContactsListBinding;
import com.virtualeducation.forwardpremium.objects.ContactObject;
import com.virtualeducation.forwardpremium.transform.RoundImageTransform;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 13.07.2017.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {
    private ArrayList<ContactObject> allContactsList;
    private ActivityUserContactsListBinding userContactsListBinding;
    private Context context;
    private int contactsCount;
    public int currentContactsCount;

    private Optimization optimization;
    public static class ViewHolder extends RecyclerView.ViewHolder {

        FrameLayout contactsFrame;
        TextView contactName;
        TextView contactNumber;
        ImageView contactPhoto;
        ImageView checkImage;
        ImageView contactBg;
        public ViewHolder(FrameLayout contactsFrame) {
            super(contactsFrame);
            this.contactsFrame=contactsFrame;
            contactName=(TextView)contactsFrame.findViewById(R.id.text_contact_name);
            contactNumber=(TextView)contactsFrame.findViewById(R.id.text_contact_number);
            contactBg=(ImageView) contactsFrame.findViewById(R.id.bg_contacts);
            contactPhoto=(ImageView) contactsFrame.findViewById(R.id.ic_contact_photo);
            checkImage=(ImageView) contactsFrame.findViewById(R.id.ic_check);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ContactListAdapter(ArrayList<ContactObject> allContactsList,
                              ActivityUserContactsListBinding userContactsListBinding,
                              Context context, Optimization optimization) {
        this.allContactsList=allContactsList;
        this.userContactsListBinding=userContactsListBinding;
        this.context=context;
        this.optimization=optimization;
        contactsCount=allContactsList.size();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {

        FrameLayout contactsFrame = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_contacts, parent, false);

        optimization.Optimization(contactsFrame);
        ViewHolder vh = new ViewHolder(contactsFrame);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,int position) {
        holder.setIsRecyclable(false);
        final ViewHolder currentHolder=holder;
        final int currentPosition=position;

        final ContactObject contactObject=allContactsList.get(position);
        holder.contactName.setText(contactObject.contactName);
        holder.contactNumber.setText(contactObject.contactNumber);
        holder.checkImage.setVisibility(View.VISIBLE);

        Glide.clear(holder.checkImage);

        // Иконка контакта
        Glide.with(context)
                .load(contactObject.contactPhotoUri)
                .transform(new RoundImageTransform(context))
                .placeholder(ContextCompat.getDrawable(context,R.drawable.ic_list_contacts))
                .into(holder.contactPhoto);

        if(contactObject.isChecked){
            holder.checkImage.setVisibility(View.VISIBLE);
        }else{
            holder.checkImage.setVisibility(View.GONE);
        }

        // Снимаем/добавляем чек на текущий звонок
        holder.contactBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(allContactsList.get(currentPosition).isChecked){
                    currentContactsCount=currentContactsCount-1;
                    userContactsListBinding.textContactsCount.setText(String.valueOf(currentContactsCount+"/"+String.valueOf(contactsCount)));
                    currentHolder.checkImage.setVisibility(View.GONE);
                    allContactsList.get(currentPosition).isChecked=false;

                    if(userContactsListBinding.icContactsCheck.getVisibility()==View.VISIBLE){
                        userContactsListBinding.icContactsCheck.setVisibility(View.GONE);
                    }
                }else{
                    currentContactsCount=currentContactsCount+1;
                    userContactsListBinding.textContactsCount.setText(String.valueOf(currentContactsCount+"/"+String.valueOf(contactsCount)));
                    currentHolder.checkImage.setVisibility(View.VISIBLE);
                    allContactsList.get(currentPosition).isChecked=true;

                    if(currentContactsCount==contactsCount){
                        userContactsListBinding.icContactsCheck.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    // Кол-во элементов листа
    @Override
    public int getItemCount() {
        return allContactsList.size();
    }
}
