package com.virtualeducation.forwardpremium.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.virtualeducation.forwardpremium.Optimization;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.databinding.ActivityUserSmsListBinding;
import com.virtualeducation.forwardpremium.objects.SmsObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 15.07.2017.
 */

public class SmsListAdapter extends RecyclerView.Adapter<SmsListAdapter.ViewHolder> {
    private ArrayList<SmsObject> allSmsList;
    private ActivityUserSmsListBinding smsListBinding;
    Optimization optimization;
    private int smsCount;
    public int currentSmsCount;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        FrameLayout smsFrame;
        TextView smsName;
        TextView smsBody;
        ImageView checkImage;
        ImageView bgSms;
        public ViewHolder(FrameLayout smsFrame) {
            super(smsFrame);
            this.smsFrame=smsFrame;
            smsName=(TextView)smsFrame.findViewById(R.id.text_sms_name);
            smsBody=(TextView)smsFrame.findViewById(R.id.text_sms_body);
            checkImage=(ImageView) smsFrame.findViewById(R.id.ic_check);
            bgSms=(ImageView) smsFrame.findViewById(R.id.bg_sms);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SmsListAdapter(ArrayList<SmsObject> allSmsList, ActivityUserSmsListBinding smsListBinding,
                          Optimization optimization) {
        this.allSmsList=allSmsList;
        this.smsListBinding=smsListBinding;
        this.optimization=optimization;
        smsCount=allSmsList.size();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SmsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {

        FrameLayout smsFrame = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_sms, parent, false);

        optimization.Optimization(smsFrame);
        SmsListAdapter.ViewHolder vh = new SmsListAdapter.ViewHolder(smsFrame);
        return vh;
    }

    @Override
    public void onBindViewHolder(SmsListAdapter.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        final ViewHolder currentHolder=holder;
        final int currentPosition=position;

        final SmsObject smsObject=allSmsList.get(position);
        if(!smsObject.smsUserName.equals("")){
            holder.smsName.setText(smsObject.smsUserName+"("+smsObject.smsNumber+")");
        }else{
            holder.smsName.setText(smsObject.smsNumber);
        }

        if(smsObject.smsBody.length()>21){
            holder.smsBody.setText(smsObject.smsBody.substring(0,20)+"...");
        }else{
            holder.smsBody.setText(smsObject.smsBody);
        }

        holder.checkImage.setVisibility(View.VISIBLE);

        if(smsObject.isChecked){
            holder.checkImage.setVisibility(View.VISIBLE);
        }else{
            holder.checkImage.setVisibility(View.GONE);
        }

        // Снимаем/добавляем чек на текущий звонок
        holder.bgSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(allSmsList.get(currentPosition).isChecked){
                    currentSmsCount=currentSmsCount-1;
                    smsListBinding.textSmsCount.setText(String.valueOf(currentSmsCount+"/"+String.valueOf(smsCount)));
                    currentHolder.checkImage.setVisibility(View.GONE);
                    allSmsList.get(currentPosition).isChecked=false;

                    if(smsListBinding.icSmsCheck.getVisibility()==View.VISIBLE){
                        smsListBinding.icSmsCheck.setVisibility(View.GONE);
                    }
                }else{
                    currentSmsCount=currentSmsCount+1;
                    smsListBinding.textSmsCount.setText(String.valueOf(currentSmsCount+"/"+String.valueOf(smsCount)));
                    currentHolder.checkImage.setVisibility(View.VISIBLE);
                    allSmsList.get(currentPosition).isChecked=true;

                    if(currentSmsCount==smsCount){
                        smsListBinding.icSmsCheck.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    // Кол-во элементов листа
    @Override
    public int getItemCount() {
        return allSmsList.size();
    }
}
