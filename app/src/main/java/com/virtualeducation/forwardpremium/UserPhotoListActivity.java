package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Pair;
import android.view.View;

import com.bumptech.glide.Glide;
import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.adapters.PhotoListAdapter;
import com.virtualeducation.forwardpremium.databinding.ActivityUserPhotoListBinding;
import com.virtualeducation.forwardpremium.eventbus.PhotoChangeCount;
import com.virtualeducation.forwardpremium.eventbus.PhotoListEvent;
import com.virtualeducation.forwardpremium.objects.PhotoObject;
import com.virtualeducation.forwardpremium.transform.GridSpacingItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 27.07.2017.
 */

public class UserPhotoListActivity extends AppCompatActivity {
    ActivityUserPhotoListBinding userPhotoListBinding;
    Optimization optimization;
    private int photoCount=0;
    private int currentPhotoCount=0;
    private int cloudStorage=20;

    List<PhotoListAdapter> allPhotoAdapters=new ArrayList();
    ArrayList<PhotoObject> allPhotoList=new ArrayList();
    ArrayList<PhotoObject> allPhotoListBackup=new ArrayList();
    ArrayList<PhotoObject> allSortPhoto=new ArrayList();
    List<Pair<String,ArrayList<PhotoObject>>> allPhotoByCategory=new ArrayList();
    PhotoListAdapter photoListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userPhotoListBinding= DataBindingUtil.setContentView(this,R.layout.activity_user_photo_list);

        // Оптимизация интерфейса
        optimization=new Optimization();
        optimization.Optimization(userPhotoListBinding.frameMain);

        // Определяем тип пользователя
        if(Hawk.get("premium",false)){
            cloudStorage=50;
            userPhotoListBinding.icCloud.setImageResource(R.drawable.ic_logo_50gb);
        }

        // Определяем занятое место и отображаем в прогрессе
        setUsedStorageInfo();

        // Получаем все фотки юзера
        getAllPhoto();
    }

    // Переход на предыдущий главный экран
    public void backClick(View v){
        EventBus.getDefault().post(new PhotoListEvent(allPhotoListBackup,true));
        finish();
    }

    // Определяем занятное место в приложении
    private void setUsedStorageInfo(){
        float usedStorage=getIntent().getFloatExtra("usedStorage",10f);
        int usedStoragePercent=Math.round((usedStorage/cloudStorage)*100);
        userPhotoListBinding.progressDeviceStorage.setProgress(usedStoragePercent);
        userPhotoListBinding.textStorage.
                setText(getResources().getString(R.string.backup_cal_storage)+" "+String.valueOf(usedStorage)+" Gb");
    }

    // Снимаем выделение/выделяем все фотки
    public void chooseAllPhotoClick(View v){
        if(userPhotoListBinding.icPhotoCheck.getVisibility()==View.VISIBLE){
            userPhotoListBinding.icPhotoCheck.setVisibility(View.GONE);
            currentPhotoCount=0;
            photoListAdapter.setAllCheckFromAllCategory(false);

        }else{
            userPhotoListBinding.icPhotoCheck.setVisibility(View.VISIBLE);
            currentPhotoCount=photoCount;
            photoListAdapter.setAllCheckFromAllCategory(true);
        }
        userPhotoListBinding.textPhotoCount.setText(String.valueOf(currentPhotoCount)+"/"+String.valueOf(photoCount));
    }

    // Получаем список фоток
    private void getAllPhoto(){
        allPhotoList.addAll(MainScreenActivity.allPhotoList);
        photoCount=allPhotoList.size();

        for (PhotoObject photoObject:allPhotoList){
            allPhotoListBackup.add(photoObject.clone());
        }

        // Определяем кол-во выбранных фото
        for (int i=0;i<allPhotoList.size();i++){
            if (allPhotoList.get(i).isChecked){
                currentPhotoCount=currentPhotoCount+1;
            }
        }
        userPhotoListBinding.textPhotoCount.setText(String.valueOf(currentPhotoCount)+"/"+String.valueOf(photoCount));

        // Отображаем все фотографии, которые есть в телефоне
        photoDecomposeByCategory();
    }

    // Распределяем фотографии по категориям
    private void photoDecomposeByCategory(){

        for (int i=0;i<allPhotoList.size();i++){
            if(i==0){
                Pair<String,ArrayList<PhotoObject>> dataPair=new Pair(allPhotoList.get(i).photoCategory,new ArrayList<String>());
                dataPair.second.add(allPhotoList.get(i));
                allPhotoByCategory.add(dataPair);
            }else{
                if(allPhotoList.get(i).photoCategory.equals(allPhotoList.get(i-1).photoCategory)){
                    allPhotoByCategory.get(allPhotoByCategory.size()-1).second.add(allPhotoList.get(i));
                }else{
                    Pair<String,ArrayList<PhotoObject>> dataPair=new Pair(allPhotoList.get(i).photoCategory,new ArrayList<PhotoObject>());
                    dataPair.second.add(allPhotoList.get(i));
                    allPhotoByCategory.add(dataPair);
                }
            }
        }

        // Отображаем все фотографии, по категориям
        createPhotoList();
    }

    // Отображаем все фотки с телефона
    private void createPhotoList(){

        for (Pair<String,ArrayList<PhotoObject>> dataPair :allPhotoByCategory){
            PhotoObject photoObject=new PhotoObject();
            photoObject.photoTitleCategory=dataPair.first;
            allSortPhoto.add(photoObject);
            allSortPhoto.addAll(dataPair.second);
        }

            photoListAdapter=
                new PhotoListAdapter(allSortPhoto,this,optimization, Glide.with(this));
        photoListAdapter.setHasStableIds(true);

        // Создаём и подключаем RecyclerView со списком фотографий нужной категории
        userPhotoListBinding.recyclerPhoto.setHasFixedSize(true);
        userPhotoListBinding.recyclerPhoto.setItemViewCacheSize(20);
        userPhotoListBinding.recyclerPhoto.setDrawingCacheEnabled(true);
        userPhotoListBinding.recyclerPhoto.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,4,GridLayoutManager.VERTICAL,false);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (photoListAdapter.getItemViewType(position)) {
                    case 0:
                        return 4;
                    case 1:
                        return 1;
                    default:
                        return 0;
                }
            }
        });
        userPhotoListBinding.recyclerPhoto.setLayoutManager(gridLayoutManager);
        userPhotoListBinding.recyclerPhoto.addItemDecoration(new GridSpacingItemDecoration(4,20,true));

        userPhotoListBinding.recyclerPhoto.setAdapter(photoListAdapter);

    }

    // Подтверждаем выбранные фотки и переходим на главный экран
    public void btnConfirmClick(View v){
        for (int i=0;i<allSortPhoto.size();i++){
            if(allSortPhoto.get(i).photoTitleCategory!=null){
                allSortPhoto.remove(allSortPhoto.get(i));
            }
        }

        // Отправляем ивент на главный экран с изменённым листом
        EventBus.getDefault().post(new PhotoListEvent(allSortPhoto,true));
        finish();
    }

    // Получаем изменения кол-ва выбранных фоток
    @Subscribe
    public void onEventCheckCount(PhotoChangeCount photoChangeCount){
        int count=photoChangeCount.count;
        currentPhotoCount=currentPhotoCount+count;
        userPhotoListBinding.textPhotoCount.setText(String.valueOf(currentPhotoCount)+"/"+String.valueOf(photoCount));
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    // Передаём event на главную страницу в виде листа выбранных звонков
    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new PhotoListEvent(allPhotoListBackup,true));
        finish();
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
