package com.virtualeducation.forwardpremium;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.alarms.JobSchedulerService;
import com.virtualeducation.forwardpremium.databinding.ActivityMainScreenBinding;
import com.virtualeducation.forwardpremium.eventbus.CallsListEvent;
import com.virtualeducation.forwardpremium.eventbus.ContactsListEvent;
import com.virtualeducation.forwardpremium.eventbus.PhotoListEvent;
import com.virtualeducation.forwardpremium.eventbus.SmsListEvent;
import com.virtualeducation.forwardpremium.eventbus.UploadDataEvent;
import com.virtualeducation.forwardpremium.objects.AppsBackObject;
import com.virtualeducation.forwardpremium.objects.AppsBackupObject;
import com.virtualeducation.forwardpremium.objects.AppsObject;
import com.virtualeducation.forwardpremium.objects.CalendarBackupObject;
import com.virtualeducation.forwardpremium.objects.CalendarObject;
import com.virtualeducation.forwardpremium.objects.CallBackupObject;
import com.virtualeducation.forwardpremium.objects.CallObject;
import com.virtualeducation.forwardpremium.objects.ContactBackupObject;
import com.virtualeducation.forwardpremium.objects.ContactObject;
import com.virtualeducation.forwardpremium.objects.DocDescrObject;
import com.virtualeducation.forwardpremium.objects.DocsBackupObject;
import com.virtualeducation.forwardpremium.objects.DocumentObject;
import com.virtualeducation.forwardpremium.objects.PhotoBackupObject;
import com.virtualeducation.forwardpremium.objects.PhotoDescrObject;
import com.virtualeducation.forwardpremium.objects.PhotoObject;
import com.virtualeducation.forwardpremium.objects.SettingsBackupObject;
import com.virtualeducation.forwardpremium.objects.SettingsObject;
import com.virtualeducation.forwardpremium.objects.SmsBackupObject;
import com.virtualeducation.forwardpremium.objects.SmsObject;
import com.virtualeducation.forwardpremium.objects.User;
import com.virtualeducation.forwardpremium.retrofit.DeleteAllUserData;
import com.virtualeducation.forwardpremium.retrofit.GetBackupInfo;
import com.virtualeducation.forwardpremium.retrofit.PostNewSmsList;
import com.virtualeducation.forwardpremium.retrofit.PostReserveEmail;
import com.virtualeducation.forwardpremium.retrofit.PostUserAppsList;
import com.virtualeducation.forwardpremium.retrofit.PostUserCalendarList;
import com.virtualeducation.forwardpremium.retrofit.PostUserCallsList;
import com.virtualeducation.forwardpremium.retrofit.PostUserContactsList;
import com.virtualeducation.forwardpremium.retrofit.PostUserDocumentsList;
import com.virtualeducation.forwardpremium.retrofit.PostUserPhotoList;
import com.virtualeducation.forwardpremium.retrofit.PostUserSettingsList;
import com.virtualeducation.forwardpremium.retrofit.PostUserSmsList;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;
import com.virtualeducation.forwardpremium.services.UploadDataService;
import com.wunderlist.slidinglayer.SlidingLayer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.os.Environment.getExternalStorageDirectory;

/**
 * Created by DarKMonK on 04.07.2017.
 */

public class MainScreenActivity extends AppCompatActivity {
    ActivityMainScreenBinding mainScreenBinding;
    AlertDialog alertDialogNotify;
    ModelMainScreenDataChecker dataChecker;
    ProgressDialog alertDialog;

    UserContactsGetAll userContactsGetAll;
    UserCallsGetAll userCallsGetAll;
    UserPhotoGetAll userPhotoGetAll;
    UserAppsAllGet userAppsAllGet;
    UserCalendarEventGetAll userEventsGetAll;
    UserSettingsGetAll userSettingsGetAll;
    UserDocumentsGetAll userDocsGetAll;
    PostReserveEmail postReserveEmail;

    RetrofitApi retrofitApi;
    GetBackupInfo getBackupInfo;
    DeleteAllUserData deleteAllUserData;
    PostUserCallsList postUserCallsList;
    PostUserContactsList postUserContactsList;
    PostUserSmsList postUserSmsList;
    PostUserPhotoList postUserPhotoList;
    PostNewSmsList postNewSmsList;
    PostUserCalendarList postUserCalendarList;
    PostUserSettingsList postUserSettingsList;
    PostUserAppsList postUserAppsList;
    PostUserDocumentsList postUserDocumentsList;

    User userObject;
    Gson gson=new Gson();
    // Списки объектов для бэкапа
    public static ArrayList<Call<Void>> allPostCalls=new ArrayList();
    ArrayList<PhotoObject> allBackupPhotoList=new ArrayList();
    ArrayList<SmsObject> allBackupSmsList=new ArrayList();
    ArrayList<ContactObject> allBackupContactsList=new ArrayList();
    ArrayList<CallObject> allBackupCallsList=new ArrayList();
    public static ArrayList<CallObject> allCallsList=new ArrayList();
    public static ArrayList<ContactObject> allContactsList=new ArrayList();
    public static ArrayList<SmsObject> allSmsList=new ArrayList();
    public static ArrayList<PhotoObject> allPhotoList=new ArrayList();
    public static ArrayList<AppsObject> allAppsList=new ArrayList();
    public static ArrayList<SettingsObject> allSettingsList=new ArrayList();
    public static ArrayList<CalendarObject> allCalendarList=new ArrayList();
    public static ArrayList<DocumentObject> allDocsList=new ArrayList();
    ArrayList<Pair<String,Object>> copyDataPair=new ArrayList();

    ObservableInt contactsObs=new ObservableInt(0);
    ObservableInt callsObs=new ObservableInt(0);
    ObservableInt photoObs=new ObservableInt(0);
    ObservableInt smsObs=new ObservableInt(0);

    ObjectAnimator copyAnimation;
    Optimization optimization;
    final Handler handler = new Handler();
    char[] accessToken;
    AtomicInteger currentCall=new AtomicInteger(0);
    public final ObservableBoolean isCopyObs=new ObservableBoolean();
    int backupDeleteCheck=0;
    Intent serviceIntent;
    // Strings
    @BindString(R.string.main_screen_device) String deviceString;
    @BindString(R.string.alert_wi_fi) String wifiAlertString;
    @BindString(R.string.drawer_text_my_cloud) String text50Gb;
    // Colors
    @BindColor(R.color.colorMainContacts) int colorContacts;
    @BindColor(R.color.colorMainCalls) int colorCalls;
    @BindColor(R.color.colorMainPhoto) int colorPhoto;
    @BindColor(R.color.colorMainSms) int colorSms;
    @BindColor(R.color.colorGray) int colorGray;
    @BindColor(R.color.colorMainCalendar) int colorCalendar;
    @BindColor(R.color.colorMainNotes) int colorNotes;
    @BindColor(R.color.colorMainApps) int colorApps;
    @BindColor(R.color.colorMainSettings) int colorSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mainScreenBinding= DataBindingUtil.setContentView(this,R.layout.activity_main_screen);
        isCopyObs.set(true);
        mainScreenBinding.setIsCopy(isCopyObs);

        setPartnerInfo();

        serviceIntent = new Intent(this, UploadDataService.class);

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);

        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }

        if(Hawk.get("backupNotify",true)){
            Long currentPeriodic=TimeUnit.DAYS.toMillis(1);
            switch (Hawk.get("backupCalendarType",1)){
                case 1:
                    currentPeriodic=TimeUnit.DAYS.toMillis(1);
                    break;
                case 2:
                    currentPeriodic=TimeUnit.DAYS.toMillis(7);
                    break;
                case 3:
                    currentPeriodic=TimeUnit.DAYS.toMillis(30);
                    break;
            }
            if(jobScheduler.getAllPendingJobs().size()==0){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    jobScheduler.schedule(new JobInfo.Builder(1,
                            new ComponentName(this, JobSchedulerService.class))
                            .setPersisted(true)
                            .setPeriodic(currentPeriodic,TimeUnit.MINUTES.toMillis(15))
                            .build());
                }else{
                    jobScheduler.schedule(new JobInfo.Builder(1,
                            new ComponentName(this, JobSchedulerService.class))
                            .setPersisted(true)
                            .setPeriodic(currentPeriodic)
                            .build());
                }
            }

            //AlarmSyncJob.scheduleJob();
        }

        // Слушаем скрол, для видимости кнопки копирования
        mainScreenBinding.scrollTile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_UP:
                        if(mainScreenBinding.btnCopy.getVisibility()==View.GONE){
                            mainScreenBinding.btnCopy.setVisibility(View.VISIBLE);
                            mainScreenBinding.imgMask.setVisibility(View.VISIBLE);
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(mainScreenBinding.btnCopy.getVisibility()==View.VISIBLE){
                            mainScreenBinding.btnCopy.setVisibility(View.GONE);
                            mainScreenBinding.imgMask.setVisibility(View.GONE);
                        }
                        break;
                }
                return false;

            }
        });

        // Оптимизация интерфейса
        optimization=new Optimization();
        optimization.Optimization(mainScreenBinding.frameMain);
        optimization.Optimization(mainScreenBinding.frameHeaderCopy);
        optimization.Optimization(mainScreenBinding.frameHeaderIdle);
        optimization.OptimizationLinear(mainScreenBinding.linearProgress);
        optimization.Optimization(mainScreenBinding.frameTiles);


        // Получаем токен текущего пользователя
        accessToken= Hawk.get("accessToken","").toCharArray();

        // NetworkManager
        retrofitApi=RetrofitApi.getInstance();
        postReserveEmail=retrofitApi.retrofit.create(PostReserveEmail.class);

        // Проверяем наличие промокода и отправлем на сервер
        connectPromocodeEmail();

        // Компануем запрос на получение всех айдишников бэкапа и занятого места
        getBackupInfo=retrofitApi.retrofit.create(GetBackupInfo.class);
        deleteAllUserData= retrofitApi.retrofit.create(DeleteAllUserData.class);
        postUserSmsList=retrofitApi.retrofit.create(PostUserSmsList.class);
        postUserCallsList=retrofitApi.retrofit.create(PostUserCallsList.class);
        postUserContactsList=retrofitApi.retrofit.create(PostUserContactsList.class);
        postUserPhotoList=retrofitApi.retrofit.create(PostUserPhotoList.class);
        postNewSmsList=retrofitApi.retrofit.create(PostNewSmsList.class);
        postUserCalendarList=retrofitApi.retrofit.create(PostUserCalendarList.class);
        postUserSettingsList=retrofitApi.retrofit.create(PostUserSettingsList.class);
        postUserAppsList=retrofitApi.retrofit.create(PostUserAppsList.class);
        postUserDocumentsList=retrofitApi.retrofit.create(PostUserDocumentsList.class);


        // Получаем информацию о девайсе пользователя (место на диске, модель)
        getDeviceInfo();

        // Класс для обработки вкл/выкл типов данных для бэкапа
        dataChecker=new ModelMainScreenDataChecker(mainScreenBinding
                ,colorContacts,colorCalls,colorSms,colorPhoto,colorGray,
                colorNotes,colorCalendar,colorApps,colorSettings);

        // Получаем контакты устройства
        userContactsGetAll=new UserContactsGetAll();
        userContactsGetAll.gelAllContacts(this);
        // Кол-во звонков
        userCallsGetAll=new UserCallsGetAll();
        userCallsGetAll.getAllCalls(this);
        // Получаем Фото устройства
        userPhotoGetAll=new UserPhotoGetAll();
        userPhotoGetAll.getAllPhoto(this);
        // Получаем установленные приложения
        userAppsAllGet=new UserAppsAllGet();
        allAppsList=userAppsAllGet.getAllApps(this);
        // Получаем все эвенты с календаря
        userEventsGetAll=new UserCalendarEventGetAll();
        allCalendarList=userEventsGetAll.getCalendarEvents(this);
        // Получаем все настройки
        userSettingsGetAll=new UserSettingsGetAll();
        allSettingsList=userSettingsGetAll.getUserSettings(this);
        // Получаем все документы
        userDocsGetAll=new UserDocumentsGetAll();
        allDocsList=userDocsGetAll.getAllDocuments(this);


        // Отправляем запрос на получение информации о бэкапе
        userObject=Hawk.get("userData",new User());

        mainScreenBinding.slidingMenu.setOnInteractListener(new SlidingLayer.OnInteractListener() {
            @Override
            public void onOpen() {
                mainScreenBinding.bgShadow.setVisibility(View.VISIBLE);
            }

            @Override
            public void onShowPreview() {
                mainScreenBinding.bgShadow.setVisibility(View.GONE);
            }

            @Override
            public void onClose() {

            }

            @Override
            public void onOpened() {

            }

            @Override
            public void onPreviewShowed() {

            }

            @Override
            public void onClosed() {

            }
        });

        // Проверка на премиум
        if(Hawk.get("premium",false)){
            TextView textDrawerCloud=(TextView)findViewById(R.id.text_drawer_cloud);
            textDrawerCloud.setText(text50Gb);
        }

        getCalendarCount(allCalendarList);
        getAppsCount(allAppsList);
        getSettingsCount(allSettingsList);
        getDocsCount(allDocsList);

    }

    // Определяем какие данные уже на сервере. Они не будут отображаться
    private void deleteAlreadyBackupData(String type){

        switch (type){
            case "contacts":
                // Удаляем уже забэкапленные данные
                // Контакты
                ArrayList<ContactBackupObject> contactsList=userObject.contactsBackup;
                for (int i=0;i<contactsList.size();i++){
                    for (int j=0;j<allContactsList.size();j++){
                        if(contactsList.get(i).uid.equals(allContactsList.get(j).id)&&
                                contactsList.get(i).phoneNumber.equals(allContactsList.get(j).contactNumber)){
                            // Удаляем контакт, который уже в бэкапе
                            allContactsList.remove(allContactsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textContactsCount.setText(String.valueOf(allContactsList.size()));
                contactsObs.set(allContactsList.size());
                contactsObs.notifyChange();
                if(allContactsList.size()>0){
                        dataChecker.setDataCheck("contacts",true);
                }
                break;
            case "calls":
                // Звонки
                ArrayList<CallBackupObject> callsList=userObject.callsBackup;
                for (int i=0;i<callsList.size();i++){
                    for (int j=0;j<allCallsList.size();j++){
                        if(callsList.get(i).uid.equals(allCallsList.get(j).id)&&
                                callsList.get(i).phoneNumber.equals(allCallsList.get(j).callNumber)){
                            // Удаляем контакт, который уже в бэкапе
                            allCallsList.remove(allCallsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textCallsCount.setText(String.valueOf(allCallsList.size()));
                callsObs.set(allCallsList.size());
                callsObs.notifyChange();
                if(allCallsList.size()>0){
                    dataChecker.setDataCheck("calls",true);
                }
                break;
            case "sms":
                // СМС
                ArrayList<SmsBackupObject> smsList=userObject.smsBackup;
                for (int i=0;i<smsList.size();i++){
                    for (int j=0;j<allSmsList.size();j++){
                        if(smsList.get(i).uid.equals(allSmsList.get(j).id)&&
                                smsList.get(i).smsDate.equals(allSmsList.get(j).smsDate)){
                            // Удаляем контакт, который уже в бэкапе
                            allSmsList.remove(allSmsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textSmsCount.setText(String.valueOf(allSmsList.size()));
                smsObs.set(allSmsList.size());
                smsObs.notifyChange();
                if(allSmsList.size()>0){
                    dataChecker.setDataCheck("sms",true);
                }
                break;
            case "photo":
                // ФОТО
                ArrayList<PhotoBackupObject> photoList=userObject.photoBackup;

                for (int i=0;i<photoList.size();i++){
                    for (int j=0;j<allPhotoList.size();j++){
                        if(photoList.get(i).photoName.equals(allPhotoList.get(j).photoName)){
                            // Удаляем контакт, который уже в бэкапе
                            allPhotoList.remove(allPhotoList.get(j));
                        }
                    }
                }
                mainScreenBinding.textPhotoCount.setText(String.valueOf(allPhotoList.size()));
                photoObs.set(allPhotoList.size());
                photoObs.notifyChange();
                if(allPhotoList.size()>0){
                    dataChecker.setDataCheck("photo",true);
                }
                break;
            case "apps":
                ArrayList<AppsBackObject> appList=userObject.appsBackup;
                for (int i=0;i<appList.size();i++){
                    for (int j=0;j<allAppsList.size();j++){
                        Log.e("Прилоги", appList.get(i).appPackage+ " "+ allAppsList.get(j).appIcon);
                        if(appList.get(i).appPackage.equals(allAppsList.get(j).appPackage)){
                            // Удаляем контакт, который уже в бэкапе
                            allAppsList.remove(allAppsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textAppsCount.setText(String.valueOf(allAppsList.size()));
                if(allAppsList.size()>0){
                    dataChecker.setDataCheck("apps",true);
                }
                break;
            case "notes":
                ArrayList<DocsBackupObject> docsList=userObject.docsBackup;
                for (int i=0;i<docsList.size();i++){
                    for (int j=0;j<allDocsList.size();j++){
                        if(docsList.get(i).name.contains(allDocsList.get(j).title)){
                            // Удаляем контакт, который уже в бэкапе
                            allDocsList.remove(allDocsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textNotesCount.setText(String.valueOf(allDocsList.size()));
                if(allDocsList.size()>0){
                    dataChecker.setDataCheck("notes",true);
                }
                break;
            case "settings":
                ArrayList<SettingsBackupObject> settingsList=userObject.settingsBackup;
                for (int i=0;i<settingsList.size();i++){
                    for (int j=0;j<allSettingsList.size();j++){
                        if(settingsList.get(i).name==null|| allSettingsList.get(j).name==null){
                            continue;
                        }
                        if(settingsList.get(i).name.equals(allSettingsList.get(j).name)){
                            // Удаляем настройку, которая уже в бэкапе
                            allSettingsList.remove(allSettingsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textSettingsCount.setText(String.valueOf(allSettingsList.size()));
                if(allSettingsList.size()>0){
                    dataChecker.setDataCheck("settings",true);
                }
                break;
            case "calendar":
                ArrayList<CalendarBackupObject> calendarList=userObject.calendarBackup;
                for (int i=0;i<calendarList.size();i++){
                    for (int j=0;j<allCalendarList.size();j++){
                        if(calendarList.get(i).title==null|| allCalendarList.get(j).title==null){
                            continue;
                        }
                        if(calendarList.get(i).title.equals(allCalendarList.get(j).title)){
                            // Удаляем настройку, которая уже в бэкапе
                            allCalendarList.remove(allCalendarList.get(j));
                        }
                    }
                }
                mainScreenBinding.textCalendarCount.setText(String.valueOf(allCalendarList.size()));
                if(allCalendarList.size()>0){
                    dataChecker.setDataCheck("calendar",true);
                }
                break;
        }
        backupDeleteCheck=backupDeleteCheck+1;
        if(backupDeleteCheck==4){
            if(getIntent().getBooleanExtra("isPush",false)){
                createTermsDialog();
            }
        }

    }

    // Получаем текущее название девайса пользователя и место на диске
    private void getDeviceInfo(){
        String deviceCompany = Build.MANUFACTURER;
        String deviceModel = Build.MODEL;

        long freeSpace= getExternalStorageDirectory().getFreeSpace();
        long totalSpace=Environment.getExternalStorageDirectory().getTotalSpace();

        float usableSpaceDecimal=((float)(totalSpace-freeSpace) / (1024 * 1024 * 1024));
        float totalSpaceDecimal=((float)(totalSpace) / (1024 * 1024 * 1024));

        // Форматируем полученные результаты в 2 знака после запятой
        String totalSpaceFormat=String.format("%.2f",totalSpaceDecimal);
        String usableSpaceFormat=String.format("%.2f",usableSpaceDecimal);

        mainScreenBinding.textPhoneName.setText(deviceCompany+"\n"+deviceModel);
        mainScreenBinding.textPhoneMemoryUsed.setText(usableSpaceFormat+" GB");
        mainScreenBinding.textPhoneMemoryTotal.setText(totalSpaceFormat+" GB");

        // Отображаем на ProgressBar загруженное место на диске
        ObjectAnimator storageAnimation = ObjectAnimator.ofInt(mainScreenBinding.progressDeviceStorage, "progress", 100-Math.round((totalSpaceDecimal-usableSpaceDecimal)/totalSpaceDecimal*100));
        storageAnimation.setDuration(1000);
        storageAnimation.setInterpolator(new DecelerateInterpolator());
        storageAnimation.start();
    }

    // Начинаем копирование после клика на кнопку Копирования
    public void copyBtnClick(View v){

        // Проверка на отправку только по Wi-Fi
        if(Hawk.get("settingsWifi",true)){
            if(!CheckInternetConnection.isConnectedWifi(this)){
                ToastManager.createToast(this,wifiAlertString);
                return;
            }
        }

        // Создаём колы с выбранными контактами
        ArrayList<CallObject> copyCallList=new ArrayList();
        ArrayList<SmsObject> copyNewSmsList=new ArrayList();
        ArrayList<ContactObject> copyContactList=new ArrayList();
        ArrayList<PhotoObject> copyPhotoList=new ArrayList();

        Log.d("Проверка","0");
        // Звонки
        for (CallObject callObject:allCallsList){
            if (callObject.isChecked){
                if (callObject.callName==null){
                    callObject.callName="";
                }
                if(callObject.callDate==null){
                    callObject.callDate="";
                }
                if(callObject.callType==null){
                    callObject.callType="";
                }
                if(callObject.callNumber==null){
                    callObject.callNumber="";
                }
                if(callObject.callDuration==null){
                    callObject.callDuration="";
                }
                copyCallList.add(callObject);
            }
        }
        Log.d("Проверка","1");
        // Контакты
        for (ContactObject contactObject:allContactsList){
            if (contactObject.isChecked){
                if(contactObject.contactName==null){
                    contactObject.contactName="";
                }
                if(contactObject.contactNumber==null){
                    contactObject.contactNumber="";
                }
                copyContactList.add(contactObject);
            }
        }
        Log.d("Проверка","2");
        // СМС
        for (SmsObject smsObject:allSmsList){
            if (smsObject.isChecked){
                copyNewSmsList.add(smsObject);
            }
            if(smsObject.smsName==null){
                smsObject.smsName="";
            }
        }

        // Приложения
        ArrayList<AppsBackupObject> appsBackupObjects=new ArrayList<>();
        for (AppsObject appsObject:allAppsList){
            appsBackupObjects.add(new AppsBackupObject(appsObject.appName,appsObject.appPackage));
        }

        Log.d("Проверка","3");
        // Фото
        for (PhotoObject photoObject:allPhotoList){
            if (photoObject.isChecked){
                copyPhotoList.add(photoObject);
            }
            if(photoObject.photoName==null){
                photoObject.photoName="";
            }
            if(photoObject.photoCategory==null){
                photoObject.photoCategory="";
            }
        }
        Log.d("Проверка","4");
        for (CallObject callObject:copyCallList){
            try{
                callObject.callDate=callObject.callDate.substring(0,callObject.callDate.length()-3);
            }catch (Exception e){

            }
        }

        Call<Void> postCalendarCall=postUserCalendarList.sendCalendarList(String.valueOf(accessToken),allCalendarList);
        Call<Void> postSettingsCall=postUserSettingsList.sendSettingsList(String.valueOf(accessToken),allSettingsList);
        Call<Void> postAppsCall=postUserAppsList.sendAppList(String.valueOf(accessToken),appsBackupObjects);

        Call<Void> postCallsCall=postUserCallsList.sendCallsList(String.valueOf(accessToken),copyCallList);
        Log.d("Проверка","5001");
        Call<Void> postContactsCall=postUserContactsList.sendContactsList(String.valueOf(accessToken),copyContactList);
        Log.d("Проверка","5002");
        Call<Void> postSmsCall=postNewSmsList.sendSms(String.valueOf(accessToken),copyNewSmsList);
        Log.d("Проверка","51");

        if(mainScreenBinding.icCheckCalendar.getVisibility()==View.VISIBLE&&allCalendarList.size()>0){
            allPostCalls.add(postCalendarCall);
            copyDataPair.add(new Pair<String, Object>("calendar",allCalendarList));
        }

        if(mainScreenBinding.icCheckSettings.getVisibility()==View.VISIBLE&&allSettingsList.size()>0){
            allPostCalls.add(postSettingsCall);
            copyDataPair.add(new Pair<String, Object>("settings",allSettingsList));
        }

        if(mainScreenBinding.icCheckApps.getVisibility()==View.VISIBLE&&appsBackupObjects.size()>0){
            allPostCalls.add(postAppsCall);
            copyDataPair.add(new Pair<String, Object>("apps",appsBackupObjects));
        }

        if(mainScreenBinding.icCheckContacts.getVisibility()==View.VISIBLE&&copyContactList.size()>0){
            allPostCalls.add(postContactsCall);
            copyDataPair.add(new Pair<String, Object>("contact",copyContactList));
        }

        if(mainScreenBinding.icCheckCalls.getVisibility()==View.VISIBLE&&copyCallList.size()>0){
            allPostCalls.add(postCallsCall);
            copyDataPair.add(new Pair<String, Object>("call",copyCallList));
        }
        if(mainScreenBinding.icCheckSms.getVisibility()==View.VISIBLE&&copyNewSmsList.size()>0){
            allPostCalls.add(postSmsCall);
            copyDataPair.add(new Pair<String, Object>("sms",copyNewSmsList));
        }
        Log.d("Проверка","6");
        // Фото
//        List<PhotoDescrObject> photoDescrList=new ArrayList();
//        List<MultipartBody.Part> photoImageList=new ArrayList();
//        List<PhotoObject> photoObjectList=new ArrayList();
        if(mainScreenBinding.icCheckPhoto.getVisibility()==View.VISIBLE&&copyPhotoList.size()>0){

            for (PhotoObject photoObject: copyPhotoList) {
                File imageFile = new File(photoObject.photoUri);
                RequestBody requestFile=RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
                MultipartBody.Part bodyPart=MultipartBody.Part.createFormData("file", photoObject.photoName, requestFile);
                Call<Void> photoCall = postUserPhotoList
                        .uploadMultipleFilesDynamic(String.copyValueOf(accessToken), bodyPart, photoObject.id, false);
                allPostCalls.add(photoCall);
                copyDataPair.add(new Pair<String, Object>("photo",copyPhotoList));
            }

//            for (int i=0;i<copyPhotoList.size();i++){
//                File imageFile = new File(copyPhotoList.get(i).photoUri);
//
//                photoObjectList.add(copyPhotoList.get(i));
//                RequestBody requestFile=RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
//                MultipartBody.Part bodyPart=MultipartBody.Part.createFormData("imageFiles[]", imageFile.getName(), requestFile);
//                photoImageList.add(bodyPart);
//                PhotoDescrObject photoDescrObject=new PhotoDescrObject();
//                photoDescrObject.size=imageFile.length();
//                photoDescrObject.uid=copyPhotoList.get(i).id;
//                photoDescrList.add(photoDescrObject);
//
//                if((i+1)%5==0&&i!=0){
//
//                    List<PhotoDescrObject> currentPhotoDescr=new ArrayList();
//                    List<MultipartBody.Part> currentPhotoImage=new ArrayList();
//                    List<PhotoObject> currentPhotoObjects=new ArrayList();
//                    for (int j=i-4;j<=i;j++){
//                        currentPhotoDescr.add(photoDescrList.get(j));
//                        currentPhotoImage.add(photoImageList.get(j));
//                        currentPhotoObjects.add(photoObjectList.get(j));
//                    }
//                    Log.d("Количество",String.valueOf(currentPhotoDescr.size()));
//                    String requestParams= gson.toJson(
//                            currentPhotoDescr,
//                            new TypeToken<ArrayList<PhotoDescrObject>>() {}.getType());
//                    // Создаём колл для текущих 5-ти фоток
//                    Call<Void> photoCall=postUserPhotoList.uploadMultipleFilesDynamic(String.copyValueOf(accessToken),
//                            currentPhotoImage,
//                            requestParams);
//                    allPostCalls.add(photoCall);
//                    copyDataPair.add(new Pair<String, Object>("photo",currentPhotoObjects));
//                }
//                if((i+1)%5!=0&&i==copyPhotoList.size()-1){
//                    List<PhotoDescrObject> currentPhotoDescr=new ArrayList();
//                    List<MultipartBody.Part> currentPhotoImage=new ArrayList();
//                    List<PhotoObject> currentPhotoObjects=new ArrayList();
//                    for (int j=i;(j+1)%5!=0;j--){
//                        currentPhotoDescr.add(photoDescrList.get(j));
//                        currentPhotoImage.add(photoImageList.get(j));
//                        currentPhotoObjects.add(photoObjectList.get(j));
//                    }
//                    Log.d("Количество",String.valueOf(currentPhotoDescr.size()));
//                    String requestParams= gson.toJson(
//                            currentPhotoDescr,
//                            new TypeToken<ArrayList<PhotoDescrObject>>() {}.getType());
//                    // Создаём колл для текущих 5-ти фоток
//                    Call<Void> photoCall=postUserPhotoList.uploadMultipleFilesDynamic(String.copyValueOf(accessToken),
//                            currentPhotoImage,
//                            requestParams);
//                    allPostCalls.add(photoCall);
//                    copyDataPair.add(new Pair<String, Object>("photo",currentPhotoObjects));
//                }
//            }
        }

        // Документы
        if(mainScreenBinding.icCheckNotes.getVisibility()==View.VISIBLE && allDocsList.size()>0) {
            for (DocumentObject documentObject : allDocsList) {
                File docFile = new File(documentObject.docUri);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), docFile);
                MultipartBody.Part bodyPart = MultipartBody.Part.createFormData("file", docFile.getName(), requestFile);
                Call<Void> docCall = postUserDocumentsList.uploadMultipleFilesDynamic(String.valueOf(accessToken), bodyPart, UUID.randomUUID().toString(), false);
                allPostCalls.add(docCall);
                copyDataPair.add(new Pair<String, Object>("notes", allDocsList));
            }
        }
//        List<DocDescrObject> docsDescrList=new ArrayList();
//        List<MultipartBody.Part> docsImageList=new ArrayList();
//        List<DocumentObject> docsObjectList=new ArrayList();
//        if(mainScreenBinding.icCheckNotes.getVisibility()==View.VISIBLE&&allDocsList.size()>0){
//            // Собираем массив файлов документов
//            for (DocumentObject documentObject:allDocsList){
//                File docFile = new File(documentObject.docUri);
//                docsObjectList.add(documentObject);
//
//                RequestBody requestFile=RequestBody.create(MediaType.parse("multipart/form-data"), docFile);
//                MultipartBody.Part bodyPart=MultipartBody.Part.createFormData("docFiles[]", docFile.getName(), requestFile);
//                docsImageList.add(bodyPart);
//
//                docsDescrList.add(new DocDescrObject(docFile.length()));
//            }
//            String requestParams= gson.toJson(
//                    docsDescrList,
//                    new TypeToken<ArrayList<DocDescrObject>>() {}.getType());
//            Call<Void> docsCall=postUserDocumentsList.uploadMultipleFilesDynamic(String.copyValueOf(accessToken),
//                    docsImageList);
//            Log.d("запросик",requestParams);
//            Log.d("запросик", String.valueOf(docsImageList.size()));
//            allPostCalls.add(docsCall);
//            copyDataPair.add(new Pair<String, Object>("notes",docsObjectList));
//
//        }

        Log.d("Проверка","7");
        if(allPostCalls.size()==0){

        }else{
            mainScreenBinding.frameHeaderIdle.setVisibility(View.GONE);
            mainScreenBinding.frameHeaderCopy.setVisibility(View.VISIBLE);
            mainScreenBinding.textProgress.setText("0");
            mainScreenBinding.progressCopy.setProgress(0);

            // Стартуем сервис
            isCopyObs.set(false); // block all clicks
            startService(serviceIntent);
            //postBackup(allPostCalls.get(0));

        }
    }


    // Ловим ивент на загрузку части данных
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void uploadEvent(UploadDataEvent dataEvent){
//        if(dataEvent.progress==200){
//            mainScreenBinding.frameHeaderIdle.setVisibility(View.VISIBLE);
//            mainScreenBinding.frameHeaderCopy.setVisibility(View.GONE);
//            //currentCall.set(0);
//            allPostCalls.clear();
//            copyDataPair.clear();
//            isCopyObs.set(true);
//            return;
//        }
        deleteCopyData(copyDataPair.get(dataEvent.progress).second,copyDataPair.get(dataEvent.progress).first);
        int partProgress=Math.round(100/allPostCalls.size());
        float percent = 100f/allPostCalls.size();
        if(partProgress==0){
            partProgress=1;
        }
        if(dataEvent.progress==0&&dataEvent.isFinished){
            setCopyProgress((dataEvent.progress+1)*partProgress,true,partProgress,(dataEvent.progress+1)*percent );
        }else{
            setCopyProgress((dataEvent.progress+1)*partProgress,dataEvent.progress+1==allPostCalls.size(),partProgress,(dataEvent.progress+1)*percent);
        }

        if(dataEvent.isFailed){
            mainScreenBinding.frameHeaderIdle.setVisibility(View.VISIBLE);
            mainScreenBinding.frameHeaderCopy.setVisibility(View.GONE);
            //currentCall.set(0);
            allPostCalls.clear();
            copyDataPair.clear();
            isCopyObs.set(true);
            ToastManager.createToast(getApplicationContext(),getString(R.string.main_screen_backup_error));
            stopService(serviceIntent);
            return;
        }
        if(dataEvent.isFinished){
            allPostCalls.clear();
            //currentCall.set(0);
            copyDataPair.clear();
            isCopyObs.set(true);
            endOfCopy();
            stopService(serviceIntent);
        }
    }


    private void postBackup(Call<Void> postCall){
        retrofitApi.createRequest(postCall);
        retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {
                Response<Void> response=(Response<Void>)responseObject;
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                } catch (Exception e) {

                }
                if(response.code()==200){
                    deleteCopyData(copyDataPair.get(currentCall.get()).second,copyDataPair.get(currentCall.get()).first);
                    int partProgress=Math.round(100/allPostCalls.size());

                    // НУЖНО
                    //setCopyProgress((currentCall.get()+1)*partProgress,currentCall.get()+1==allPostCalls.size(),partProgress);
                    if(currentCall.addAndGet(1)==allPostCalls.size()){
                        allPostCalls.clear();
                        currentCall.set(0);
                        copyDataPair.clear();
                        isCopyObs.set(true);
                        endOfCopy();
                    }else{
                        // Отправляем следующий запрос
                        postBackup(allPostCalls.get(currentCall.get()));
                    }
                }else{
                    mainScreenBinding.frameHeaderIdle.setVisibility(View.VISIBLE);
                    mainScreenBinding.frameHeaderCopy.setVisibility(View.GONE);
                    currentCall.set(0);
                    allPostCalls.clear();
                    copyDataPair.clear();
                    isCopyObs.set(true);
                    ToastManager.createToast(getApplicationContext(),"Ошибка "+String.valueOf(response.code()));
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                mainScreenBinding.frameHeaderIdle.setVisibility(View.VISIBLE);
                mainScreenBinding.frameHeaderCopy.setVisibility(View.GONE);
                currentCall.set(0);
                allPostCalls.clear();
                copyDataPair.clear();
                isCopyObs.set(true);
                ToastManager.createToast(getApplicationContext(),"Ошибка "+error.toString());
            }
        });
    }

    // Удаление уже скопированых данных
    private void deleteCopyData(Object copyList,String type){
        switch (type){
            case "contact":
                ArrayList<ContactObject>  contactsCopyList=(ArrayList<ContactObject>)copyList;
                for (int i=0;i<contactsCopyList.size();i++){
                    for (int j=0;j<allContactsList.size();j++){
                        if(contactsCopyList.get(i).id.equals(allContactsList.get(j).id)){
                            // Удаляем контакт, который уже в бэкапе
                            allContactsList.remove(allContactsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textContactsCount.setText(String.valueOf(allContactsList.size()));
                break;
            case "call":
                ArrayList<CallObject>  callsCopyList=(ArrayList<CallObject>)copyList;
                for (int i=0;i<callsCopyList.size();i++){
                    for (int j=0;j<allCallsList.size();j++){
                        if(callsCopyList.get(i).id.equals(allCallsList.get(j).id)){
                            // Удаляем контакт, который уже в бэкапе
                            allCallsList.remove(allCallsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textCallsCount.setText(String.valueOf(allCallsList.size()));
                break;
            case "sms":
                ArrayList<SmsObject>  smsCopyList=(ArrayList<SmsObject>)copyList;
                for (int i=0;i<smsCopyList.size();i++){
                    for (int j=0;j<allSmsList.size();j++){
                        if(smsCopyList.get(i).id.equals(allSmsList.get(j).id)){
                            // Удаляем контакт, который уже в бэкапе
                            allSmsList.remove(allSmsList.get(j));
                        }
                    }
                }
                mainScreenBinding.textSmsCount.setText(String.valueOf(allSmsList.size()));
                break;
            case "photo":
                ArrayList<PhotoObject>  photoCopyList=(ArrayList<PhotoObject>)copyList;
                for (int i=0;i<photoCopyList.size();i++){
                    for (int j=0;j<allPhotoList.size();j++){
                        if(photoCopyList.get(i).id.equals(allPhotoList.get(j).id)){
                            // Удаляем контакт, который уже в бэкапе
                            allPhotoList.remove(allPhotoList.get(j));
                        }
                    }
                }
                mainScreenBinding.textPhotoCount.setText(String.valueOf(allPhotoList.size()));
                break;
            case "calendar":
                ArrayList<CalendarObject>  calendarCopyList=(ArrayList<CalendarObject>)copyList;
                for (int i=0;i<calendarCopyList.size();i++){
                    for (int j=0;j<allCalendarList.size();j++){
                        try{
                            if(calendarCopyList.get(i).title.equals(allCalendarList.get(j).title)){
                                // Удаляем календарь, который уже в бэкапе
                                allCalendarList.remove(allCalendarList.get(j));
                            }
                        }catch (Exception e){}
                    }
                }
                allCalendarList.clear();
                mainScreenBinding.textCalendarCount.setText(String.valueOf(allCalendarList.size()));
                break;
            case "notes":
                ArrayList<DocumentObject>  docsCopyList=(ArrayList<DocumentObject>)copyList;
                for (int i=0;i<docsCopyList.size();i++){
                    for (int j=0;j<allDocsList.size();j++){
                        if(docsCopyList.get(i).title.equals(allDocsList.get(j).title)){
                            // Удаляем контакт, который уже в бэкапе
                            allDocsList.remove(allDocsList.get(j));
                        }
                    }
                }
                allDocsList.clear();
                mainScreenBinding.textNotesCount.setText(String.valueOf(allDocsList.size()));
                break;
            case "apps":
                ArrayList<AppsBackupObject>  appsCopyList=(ArrayList<AppsBackupObject>)copyList;
                for (int i=0;i<appsCopyList.size();i++){
                    for (int j=0;j<allAppsList.size();j++){
                        Log.e("Апки",appsCopyList.get(i).appPackage + " " + allAppsList.get(j).appPackage );
                        if(appsCopyList.get(i).appPackage.equals(allAppsList.get(j).appPackage)){
                            // Удаляем контакт, который уже в бэкапе
                            allAppsList.remove(allAppsList.get(j));
                        }
                    }
                }
                allAppsList.clear();
                mainScreenBinding.textAppsCount.setText(String.valueOf(allAppsList.size()));
                break;
            case "settings":
                ArrayList<SettingsObject>  settingsCopyList=(ArrayList<SettingsObject>)copyList;
                for (int i=0;i<settingsCopyList.size();i++){
                    for (int j=0;j<allSettingsList.size();j++){
                        if(settingsCopyList.get(i).name.equals(allSettingsList.get(j).name)){
                            // Удаляем контакт, который уже в бэкапе
                            allSettingsList.remove(allSettingsList.get(j));
                        }
                    }
                }
                allSettingsList.clear();
                mainScreenBinding.textSettingsCount.setText(String.valueOf(allSettingsList.size()));
                break;
        }
    }

    // Отображение прогресса отправки
    private void setCopyProgress(int progress,boolean isLast,int partProgress, float newProgress){

        if(copyAnimation!=null){
            if(copyAnimation.isRunning()){
                copyAnimation.pause();
            }
        }

        if(isLast){
            progress=100;
            newProgress = 100f;
        }

        Log.d("Проггресс",String.valueOf(progress));
        copyAnimation = ObjectAnimator.ofInt(mainScreenBinding.progressCopy, "progress",Math.round(newProgress));
        copyAnimation.setDuration(500);
        copyAnimation.setInterpolator(new DecelerateInterpolator());
        copyAnimation.start();

        mainScreenBinding.textProgress
                .setAnimationDuration(300)
                .countAnimation(Integer.valueOf(mainScreenBinding.textProgress.getText().toString()), Math.round(newProgress));
        final int progressCurrent=progress;
        final float progressPercentCurrent=newProgress;

        if(progress!=100){
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScreenBinding.textProgress.setText(String.valueOf(Math.round(progressPercentCurrent)));
                }
            }, 400);
        }

    }

    // Завершаем всю выкачку на сервер
    private void endOfCopy(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mainScreenBinding.frameHeaderIdle.setVisibility(View.VISIBLE);
                mainScreenBinding.frameHeaderCopy.setVisibility(View.GONE);
                // Заканчиваем выкачку на сервер
                ToastManager.createToast(getApplicationContext(),"Бэкап удачно создан");
            }
        }, 500);
    }

    // Клик на чекер (выделение категорий для копирования)
    public void checkDataClick(View v){
        switch (v.getId()){
            case R.id.ic_circle_contacts:
                dataChecker.setDataCheck("contacts",mainScreenBinding.icCheckContacts.getVisibility()==View.GONE);
                break;
            case R.id.ic_circle_calls:
                dataChecker.setDataCheck("calls",mainScreenBinding.icCheckCalls.getVisibility()==View.GONE);
                break;
            case R.id.ic_circle_sms:
                dataChecker.setDataCheck("sms",mainScreenBinding.icCheckSms.getVisibility()==View.GONE);
                break;
            case R.id.ic_circle_photo:
                dataChecker.setDataCheck("photo",mainScreenBinding.icCheckPhoto.getVisibility()==View.GONE);
                break;
            case R.id.ic_circle_settings:
                dataChecker.setDataCheck("settings",mainScreenBinding.icCheckSettings.getVisibility()==View.GONE);
                break;
            case R.id.ic_circle_apps:
                dataChecker.setDataCheck("apps",mainScreenBinding.icCheckApps.getVisibility()==View.GONE);
                break;
            case R.id.ic_circle_notes:
                dataChecker.setDataCheck("notes",mainScreenBinding.icCheckNotes.getVisibility()==View.GONE);
                break;
            case R.id.ic_circle_calendar:
                dataChecker.setDataCheck("calendar",mainScreenBinding.icCheckCalendar.getVisibility()==View.GONE);
                break;
        }
    }

    // Переход на экраны листов контактов/фото/звонков/смс
    public void userDaraListClick(View v){
        switch (v.getId()){
            case R.id.card_contact:
                startActivity(new Intent(this,UserContactsListActivity.class));
                break;
            case R.id.card_calls:
                startActivity(new Intent(this,UserCallsListActivity.class));
                break;
            case R.id.card_sms:
                startActivity(new Intent(this,UserSmsListActivity.class));
                break;
            case R.id.card_photo:
                float usedStorage=Float.valueOf(userObject.usedStorage);
                startActivity(new Intent(this,UserPhotoListActivity.class).putExtra("usedStorage",usedStorage));
                break;
        }
    }

    // Определяем кол-во контактов в телефоне
    private void getContactsCount(ArrayList<ContactObject> userContacts){
        allContactsList=userContacts;
        allBackupContactsList=userContacts;
        mainScreenBinding.textContactsCount.setText(String.valueOf(allContactsList.size()));

        // Проверка на выделения фрейма как активного для копировани

        deleteAlreadyBackupData("contacts");
    }

    // Определяем кол-во СМС в телефоне
    private void getSMSCount(ArrayList<SmsObject> userSms){
        allSmsList=userSms;
        allBackupSmsList=userSms;
        mainScreenBinding.textSmsCount.setText(String.valueOf(allSmsList.size()));

        // Проверка на выделения фрейма как активного для копировани

        deleteAlreadyBackupData("sms");
    }

    // Определяем кол-во входящих/исходящих звонков
    private void getCallsCount(ArrayList<CallObject> userCalls){

        allCallsList=userCalls;
        allBackupCallsList=userCalls;
        mainScreenBinding.textCallsCount.setText(String.valueOf(allCallsList.size()));

        // Проверка на выделения фрейма как активного для копировани

        deleteAlreadyBackupData("calls");
    }

    // Определяем кол-во всех фоток на телефоне
    private void getPhotoCount(ArrayList<PhotoObject> userPhoto){
        allPhotoList=userPhoto;
        allBackupPhotoList=userPhoto;
        mainScreenBinding.textPhotoCount.setText(String.valueOf(allPhotoList.size()));

        // Проверка на выделения фрейма как активного для копировани

        deleteAlreadyBackupData("photo");
    }

    // Определяем кол-во приложений на телефоне
    private void getAppsCount(ArrayList<AppsObject> userApps){

        deleteAlreadyBackupData("apps");
    }

    // Определяем кол-во эвентов в календаре
    private void getCalendarCount(ArrayList<CalendarObject> userCalendar){

        deleteAlreadyBackupData("calendar");
    }

    // Определяем кол-во настроек
    private void getSettingsCount(ArrayList<SettingsObject> userSettings){

        deleteAlreadyBackupData("settings");
    }

    // Определяем кол-во документов
    private void getDocsCount(ArrayList<DocumentObject> userDocs){

        deleteAlreadyBackupData("notes");
    }

    // Подписываемся на получение изменённых контактов
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleContactsEvent(ContactsListEvent contactsListEvent) {
        if(contactsListEvent.isBasicContacts) {
            allContactsList = contactsListEvent.contactsList;
            int checkedContacts = 0;
            // Получаем все выбранные вызовы
            for (int i = 0; i < allContactsList.size(); i++) {
                if (allContactsList.get(i).isChecked) {
                    checkedContacts = checkedContacts + 1;
                }
            }
            // Отображаем кол-во выбранных звонков для бэкапа
            mainScreenBinding.textContactsCount.setText(String.valueOf(checkedContacts));

        }else{
            // Определяем кол-во контактов в телефоне
            getContactsCount(contactsListEvent.contactsList);

            // Кол-во СМС
            UserSmsGetAll userSmsGetAll=new UserSmsGetAll();
            userSmsGetAll.getAllSms(this);
        }
    }

    // Подписываемся на получение изменённых звонков
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleCallsEvent(CallsListEvent callsListEvent) {
        if(callsListEvent.isBasicContacts){
            allCallsList=callsListEvent.callsList;
            int checkedCalls=0;
            // Получаем все выбранные вызовы
            for (int i=0;i<allCallsList.size();i++){
                if(allCallsList.get(i).isChecked){
                    checkedCalls=checkedCalls+1;
                }
            }
            // Отображаем кол-во выбранных звонков для бэкапа
            mainScreenBinding.textCallsCount.setText(String.valueOf(checkedCalls));

        }else{
            getCallsCount(callsListEvent.callsList);
        }

    }

    // Подписываемся на получение изменённых смс
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleSmsEvent(SmsListEvent smsListEvent) {
        if(smsListEvent.isBasicContacts){
            allSmsList=smsListEvent.smsList;
            int checkedSms=0;
            // Получаем все выбранные вызовы
            for (int i=0;i<allSmsList.size();i++){
                if(allSmsList.get(i).isChecked){
                    checkedSms=checkedSms+1;
                }
            }

            // Отображаем кол-во выбранных звонков для бэкапа
            mainScreenBinding.textSmsCount.setText(String.valueOf(checkedSms));
        }else{
            getSMSCount(smsListEvent.smsList);
        }

    }

    // Подписываемся на получение изменённых фоток
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handlePhotoEvent(PhotoListEvent photoListEvent) {
        if(photoListEvent.isBasicContacts){
            allPhotoList.clear();
            allPhotoList.addAll(photoListEvent.photoList);
            int checkedCalls=0;

            for (int i=0;i<allPhotoList.size();i++){
                if(allPhotoList.get(i).isChecked){
                    checkedCalls=checkedCalls+1;
                }
            }
            // Отображаем кол-во выбранных фото для бэкапа
            if(photoListEvent.photoList.size()==0){
                dataChecker.setDataCheck("photo",false);
            }
            mainScreenBinding.textPhotoCount.setText(String.valueOf(checkedCalls));
        }else{
            getPhotoCount(photoListEvent.photoList);
        }
    }

    // Вызов Бокового меню
    public void drawerShowClick(View v){
        mainScreenBinding.slidingMenu.openLayer(true);
    }

    // Обработка кликов на список в меню
    public void drawerItemClick(View v){
        mainScreenBinding.slidingMenu.closeLayer(true);
        if(mainScreenBinding.bgShadow.getVisibility()==View.VISIBLE){
            mainScreenBinding.bgShadow.setVisibility(View.GONE);
        }
        switch (v.getTag().toString()){
            case "cloud":
                startActivity(new Intent(this,MyCloudScreenActivity.class)
                        .putExtra("usedStorage",Float.valueOf(userObject.usedStorage)));
                break;
            case "calendar":
                float usedStorage=Float.valueOf(userObject.usedStorage);
                startActivity(new Intent(this,BackupCalendarActivity.class).putExtra("usedStorage",usedStorage));
                break;
            case "about":
                startActivity(new Intent(this,AboutAppActivity.class));
                break;
            case "callback":
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, "virtualeducationassistant@gmail.com");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Forward Backup");
                startActivity(intent);
                break;
            case "settings":
                startActivity(new Intent(this,SettingsActivity.class));
                break;
            case "delete":
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Удалить все Ваши данные на сервере?")
                        .setCancelable(true)
                        .setPositiveButton("Да",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                        deleteAllUserData();
                                    }
                                })
                        .setNegativeButton("Нет",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });
                builder.show();
                break;
            case "share":
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Я пользуюсь Forward Backup" +
                        " https://play.google.com/store/apps/details?id=com.virtualeducation.forwardpremium&hl=ru");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case "exit":
                startActivity(new Intent(this,AuthScreenActivity.class));
                Hawk.put("userAuthStage","auth");
                finish();
                break;
        }
    }

    // Удаляем всю информацию с сервера
    private void deleteAllUserData(){
        createProgressAlert(true);
        Call<Void> deleteAllDataCall=deleteAllUserData.deleteAllData(String.copyValueOf(accessToken));
        retrofitApi.createRequest(deleteAllDataCall);
        retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {
                createProgressAlert(false);
                Response<Void> response=(Response<Void>)responseObject;
                if(response.code()!=200 &&response.body()==null){
                    ToastManager.createToast(getApplicationContext(),"Ошибка");
                }
                userObject.callsBackup.clear();
                userObject.contactsBackup.clear();
                userObject.photoBackup.clear();
                userObject.smsBackup.clear();
                userObject.appsBackup.clear();
                userObject.settingsBackup.clear();
                userObject.calendarBackup.clear();
                userObject.docsBackup.clear();
                userObject.usedStorage="0";
                Hawk.put("userData",userObject);
                allCallsList.clear();
                allContactsList.clear();
                allSmsList.clear();
                allPhotoList.clear();
                allAppsList.clear();
                allCalendarList.clear();
                allSettingsList.clear();
                allDocsList.clear();
                ToastManager.createToast(getApplicationContext(),"Ваши данные успешно удалены");

                // Получаем контакты устройства
                userContactsGetAll.gelAllContacts(MainScreenActivity.this);
                // Кол-во звонков
                userCallsGetAll.getAllCalls(MainScreenActivity.this);
                // Получаем Фото устройства
                userPhotoGetAll.getAllPhoto(MainScreenActivity.this);
                allAppsList=userAppsAllGet.getAllApps(MainScreenActivity.this);
                // Получаем все эвенты с календаря
                allCalendarList=userEventsGetAll.getCalendarEvents(MainScreenActivity.this);
                // Получаем все настройки
                allSettingsList=userSettingsGetAll.getUserSettings(MainScreenActivity.this);
                // Получаем все документы
                allDocsList=userDocsGetAll.getAllDocuments(MainScreenActivity.this);
                // Получаем все приложения
                allAppsList=userAppsAllGet.getAllApps(MainScreenActivity.this);
                // Получаем все документы
                allDocsList=userDocsGetAll.getAllDocuments(MainScreenActivity.this);
                // Получаем все настройки
                allSettingsList=userSettingsGetAll.getUserSettings(MainScreenActivity.this);
            }

            @Override
            public void onEventFailure(Throwable error) {
                createProgressAlert(false);
                Log.d("Удаление",error.toString());
            }
        });
    }

    // Менеджер АлертДиалога
    public void createProgressAlert(boolean setVisibility){

        if(setVisibility){
            alertDialog = ProgressDialog.show(this, "",
                    "Подождите...", true);
        }else {
            if(alertDialog!=null) {
                alertDialog.dismiss();
            }
        }
    }

    // Создаём диалог для отображения предложения бэкапа
    private void createTermsDialog(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this,R.style.CustomDialog);
        FrameLayout dialogView = (FrameLayout)getLayoutInflater().inflate(R.layout.alert_backup_data,null);
        optimization.Optimization(dialogView);
        optimization.Optimization((FrameLayout) dialogView.findViewById(R.id.frame_alert_calls));
        optimization.Optimization((FrameLayout) dialogView.findViewById(R.id.frame_alert_contacts));
        optimization.Optimization((FrameLayout) dialogView.findViewById(R.id.frame_alert_sms));
        optimization.Optimization((FrameLayout) dialogView.findViewById(R.id.frame_alert_photo));
        optimization.OptimizationLinear((LinearLayout) dialogView.findViewById(R.id.linear_alert_bottom));
        alert.setView(dialogView);

        ((TextView)dialogView.findViewById(R.id.alert_contacts_count)).setText(String.valueOf(contactsObs.get()));
        ((TextView)dialogView.findViewById(R.id.alert_sms_count)).setText(String.valueOf(smsObs.get()));
        ((TextView)dialogView.findViewById(R.id.alert_calls_count)).setText(String.valueOf(callsObs.get()));
        ((TextView)dialogView.findViewById(R.id.alert_photo_count)).setText(String.valueOf(photoObs.get()));

        dialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogNotify.dismiss();
            }
        });

        dialogView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainScreenBinding.btnCopy.callOnClick();
            }
        });

        alertDialogNotify=alert.create();
        alertDialogNotify.show();
    }

    private void connectPromocodeEmail(){

        if(Hawk.get("usedPromo",false)){
            return;
        }
        if(!Hawk.get("promocode","").equals("")){

            postReserveEmail.sendEmail(String.copyValueOf(accessToken),Hawk.get("promocode","")).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Hawk.put("usedPromo",true);
                    Log.d("код", String.valueOf(response.code()));
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d("код", t.toString());
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Hawk.get("newLanguage",false)){
            recreate();
            Hawk.put("newLanguage",false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        //Intent serviceIntent = new Intent(this, UploadDataService.class);
        stopService(serviceIntent);
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnerInfo() {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                mainScreenBinding.bgHeader.setBackgroundResource(R.drawable.gradient_main_sulpak);
                mainScreenBinding.btnCopy.setBackgroundResource(R.drawable.shape_btn_main_sulpak);
                mainScreenBinding.drawerFrame.frameBottom.setBackgroundColor(getResources().getColor(R.color.colorSulpakBg));
                mainScreenBinding.drawerFrame.bgHeader.setBackgroundColor(getResources().getColor(R.color.colorSulpakBg));
                mainScreenBinding.drawerFrame.icLogo.setImageResource(R.drawable.ic_drawer_logo_sulpak);
                mainScreenBinding.drawerFrame.divider.setBackgroundColor(getResources().getColor(R.color.colorSulpakBg));

                mainScreenBinding.drawerFrame.icPartner.setImageResource(R.drawable.ic_drawer_app_sulpak);
                mainScreenBinding.drawerFrame.icCalendar.setImageResource(R.drawable.ic_drawer_calendar_sulpak);
                mainScreenBinding.drawerFrame.icRefresh.setImageResource(R.drawable.ic_drawer_refresh_sulpak);
                mainScreenBinding.drawerFrame.icSettings.setImageResource(R.drawable.ic_drawer_settings_sulpak);
                mainScreenBinding.drawerFrame.icAbout.setImageResource(R.drawable.ic_drawer_info_sulpak);
                mainScreenBinding.drawerFrame.icLetter.setImageResource(R.drawable.ic_drawer_letter_sulpak);
                mainScreenBinding.drawerFrame.icChat.setImageResource(R.drawable.ic_drawer_chat_sulpak);
                break;
        }
    }
}
