package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.retrofit.PostPromocode;
import com.virtualeducation.forwardpremium.retrofit.PostUserAuthRequest;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PromocodeActivity extends AppCompatActivity {
    private TextInputEditText promocodeEdit;
    private PostPromocode postPromocode;

    private Button btnNext;
    private Button btnAlready;

    private ImageView icLogo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promocode);

        promocodeEdit = (TextInputEditText) findViewById(R.id.edit_promocode);
        icLogo = (ImageView) findViewById(R.id.ic_logo);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnAlready = (Button) findViewById(R.id.btn_already);

        createEditTextListener();
        setPartnersInfo();
        postPromocode = RetrofitApi.getInstance().retrofit.create(PostPromocode.class);
    }

    // Обработка ввода чисел в поля
    private void createEditTextListener() {

        promocodeEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length() % 5 == 0 && editable.length() > 0) {

                    if (' ' == editable.charAt(editable.length() - 1)) {
                        editable.delete(editable.length() - 1, editable.length());
                    }
                }

                if (editable.length() % 5 == 0 && editable.length() > 0) {
                    editable.insert(editable.length() - 1, " ");
                }
            }

        });

    }

    public void checkPromocodeClick(View v) {
        final String promocode = promocodeEdit.getText().toString().trim().replaceAll(" ", "");

        postPromocode.sendPromocode(promocode).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("код", String.valueOf(response.code()));
                switch (response.code()) {
                    case 200:
                        startActivity(new Intent(PromocodeActivity.this, AuthScreenActivity.class));
                        Hawk.put("promocode", promocode);
                        Hawk.put("userAuthStage", "auth");
                        finish();
                        break;
                    case 400:
                        Toast.makeText(PromocodeActivity.this, getResources().getString(R.string.promocode_alert_key_no),
                                Toast.LENGTH_LONG).show();
                        break;
                    case 401:
                        Toast.makeText(PromocodeActivity.this, getResources().getString(R.string.promocode_alert_key_used),
                                Toast.LENGTH_LONG).show();
                        break;
                    default:
                        Toast.makeText(PromocodeActivity.this, getResources().getString(R.string.promocode_alert_key_no),
                                Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    public void withoutPromoClick(View v) {
        startActivity(new Intent(this, AuthScreenActivity.class));
        Hawk.put("userAuthStage", "auth");
        finish();
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnersInfo() {
        switch (BuildConfig.PARTNER) {
            case Partners.SULPAK:
                icLogo.setImageResource(R.drawable.ic_logo_sulpak);
                btnAlready.setBackgroundResource(R.drawable.shape_btn_start_sulpak);
                btnNext.setBackgroundResource(R.drawable.shape_btn_start_sulpak);
                break;
        }
    }
}
