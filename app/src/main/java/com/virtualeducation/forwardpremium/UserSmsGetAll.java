package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.util.Log;

import com.virtualeducation.forwardpremium.eventbus.SmsListEvent;
import com.virtualeducation.forwardpremium.objects.ContactObject;
import com.virtualeducation.forwardpremium.objects.SmsObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 15.07.2017.
 */

public class UserSmsGetAll {

    public void getAllSms(Context context){
        Uri smsContentUri = Uri.parse("content://sms");
        final Cursor cursorSms = context.getContentResolver().query(smsContentUri, null,null, null, null);

        // Получаем номера нужных столбцов из таблицы смс
        final int columnIndexId=cursorSms.getColumnIndex(Telephony.Sms._ID);
        final int columnIndexName=cursorSms.getColumnIndex(Telephony.Sms.ADDRESS);
        final int columnIndexDate=cursorSms.getColumnIndex(Telephony.Sms.DATE);
        final int columnIndexBody=cursorSms.getColumnIndex(Telephony.Sms.BODY);
        final int columnIndexType=cursorSms.getColumnIndex(Telephony.Sms.TYPE);

        // Создаём новый поток для получения списка звонков
        Thread smsThread = new Thread(
                new Runnable() {
                    public void run() {
                        final ArrayList<SmsObject> allSmsList=new ArrayList();
                        final ArrayList<ContactObject> allContactsList=MainScreenActivity.allContactsList;
                        // Получаем все данные по смс
                        while (cursorSms.moveToNext()){

                            try {
                                SmsObject smsObject = new SmsObject();
                                smsObject.id = cursorSms.getString(columnIndexId);
                                if(cursorSms.getString(columnIndexName)!=null){smsObject.smsName = cursorSms.getString(columnIndexName);}
                                if(cursorSms.getString(columnIndexDate)!=null){smsObject.smsDate = cursorSms.getString(columnIndexDate);}
                                if(cursorSms.getString(columnIndexBody)!=null){smsObject.smsBody = cursorSms.getString(columnIndexBody);}
                                if(cursorSms.getString(columnIndexType)!=null){smsObject.smsType = Integer.valueOf(cursorSms.getString(columnIndexType));}

                                if (smsObject.smsName.matches("^[+]?[0-9]{4,20}$")) {
                                    smsObject.smsNumber = smsObject.smsName;
                                }

                                if(smsObject.smsNumber==null){
                                    smsObject.smsNumber=smsObject.smsName;
                                }
                                if(smsObject.smsNumber==null){
                                    smsObject.smsNumber="Unknown";
                                }

                                allSmsList.add(smsObject);


                            }catch (Exception e){
                                Log.d("смски",e.toString());
                            }
                        }

                        // Обработка всех СМС
                        for (SmsObject smsObject:allSmsList){
                            for (ContactObject contactObject:allContactsList){

                                if (contactObject.contactName.equals(smsObject.smsName)) {
                                    smsObject.smsNumber = contactObject.contactNumber;
                                }

                                if (contactObject.contactNumberTrim.equals(smsObject.smsNumber)) {
                                    smsObject.smsUserName = contactObject.contactName;
                                }else{
                                    if(contactObject.contactNumberTrim.length()>=9&&smsObject.smsNumber.contains(contactObject.contactNumberTrim)){
                                        smsObject.smsUserName = contactObject.contactName;
                                    }
                                }
                            }
                        }

                        EventBus.getDefault().post(new SmsListEvent(allSmsList,false));
                        cursorSms.close();
                    }
                }
        );
        smsThread.start();

    }
}
