package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CallObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 01.08.2017.
 */

public interface PostUserSmsList {
    @Headers("Content-Type: application/json")
    @POST("sms")
    Call<Void> sendSmsList(@Header("Authorization") String token, @Body ArrayList<CallObject> smsList);
}
