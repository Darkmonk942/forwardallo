package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.SmsObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 08.10.2017.
 */

public interface PostNewSmsList {
    @Headers("Content-Type: application/json")
    @POST("sms/")
    Call<Void> sendSms(@Header("Authorization") String token, @Body ArrayList<SmsObject> smsList);
}
