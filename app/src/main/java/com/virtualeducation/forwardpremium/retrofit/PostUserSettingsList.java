package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CalendarObject;
import com.virtualeducation.forwardpremium.objects.SettingsObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PostUserSettingsList {
    @Headers("Content-Type: application/json")
    @POST("settings/")
    Call<Void> sendSettingsList(@Header("Authorization") String token, @Body ArrayList<SettingsObject> settingsObjects);
}
