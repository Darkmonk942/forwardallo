package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.AppsBackupObject;
import com.virtualeducation.forwardpremium.objects.AppsObject;
import com.virtualeducation.forwardpremium.objects.CalendarObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PostUserAppsList {
    @Headers("Content-Type: application/json")
    @POST("applications/")
    Call<Void> sendAppList(@Header("Authorization") String token, @Body ArrayList<AppsBackupObject> appsObjects);
}
