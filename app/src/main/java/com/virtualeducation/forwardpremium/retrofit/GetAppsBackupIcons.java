package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.AppIconObject;
import com.virtualeducation.forwardpremium.objects.SmsObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface GetAppsBackupIcons {
    @FormUrlEncoded
    @POST("app/icons")
    Call<List<AppIconObject>> getIcons(@Header("Authorization") String token,
                                       @Field("name") String names);
}
