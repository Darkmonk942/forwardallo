package com.virtualeducation.forwardpremium.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DarKMonK on 04.07.2017.
 */

public class RetrofitApi {

    private static RetrofitApi retrofitApi;
    public Retrofit retrofit;
    public RetrofitRequestCallback retrofitCallback;

    private RetrofitApi() {
        // Логирование для всех запросов к сети
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(15, TimeUnit.MINUTES)
                .connectTimeout(15, TimeUnit.MINUTES)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://forward.dbvirtualeducation.com/v1/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

    public static RetrofitApi getInstance(){
        if (retrofitApi == null){ //if there is no instance available... create new one
            retrofitApi = new RetrofitApi();
        }

        return retrofitApi;
    }

    // Обработка полученного запроса
    public void createRequest(Object request){
        Call<Object> retrofitRequest=(Call<Object>) request;
        retrofitRequest.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                // Callback удачного запроса
                if(retrofitCallback != null)
                {
                    retrofitCallback.onEventSuccess(response);
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable error) {

                // Callback ошибки
                if(retrofitCallback != null) {
                    retrofitCallback.onEventFailure(error);
                }
            }
        });
    }

    // Callback запроса
    public void setOnRequestListener(RetrofitRequestCallback retrofitCallback)
    {
        this.retrofitCallback = retrofitCallback;
    }

}
