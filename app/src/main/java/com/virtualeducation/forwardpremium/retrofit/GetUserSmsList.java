package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CloudBackupSms;
import com.virtualeducation.forwardpremium.objects.SmsObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 25.08.2017.
 */

public interface GetUserSmsList {
    @GET("sms")
    Call<ArrayList<SmsObject>> sms(@Header("Authorization") String token);
}
