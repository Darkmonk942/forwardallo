package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CallObject;
import com.virtualeducation.forwardpremium.objects.CloudBackupCalls;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 25.08.2017.
 */

public interface GetUserCallsList {
    @GET("call-log/")
    Call<ArrayList<CallObject>> calls(@Header("Authorization") String token);
}
