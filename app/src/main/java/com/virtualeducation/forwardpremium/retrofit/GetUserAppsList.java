package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CloudBackupApps;
import com.virtualeducation.forwardpremium.objects.CloudBackupCalendar;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetUserAppsList {
    @GET("applications/")
    Call<List<CloudBackupApps>> apps(@Header("Authorization") String token);
}
