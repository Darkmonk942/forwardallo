package com.virtualeducation.forwardpremium.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PostReserveEmail {
    @FormUrlEncoded
    @POST("promocode/activate")
    Call<Void> sendEmail(@Header ("Authorization") String token,
                         @Field("promocode") String promocode);
}
