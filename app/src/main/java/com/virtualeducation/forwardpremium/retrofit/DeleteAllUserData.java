package com.virtualeducation.forwardpremium.retrofit;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

/**
 * Created by DarKMonK on 22.08.2017.
 */

public interface DeleteAllUserData {
    @Headers("Content-Type: application/json")
    @DELETE("delete-user-data/")
    Call<Void> deleteAllData(@Header("Authorization") String token);

}
