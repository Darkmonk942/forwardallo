package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CloudBackupDocs;
import com.virtualeducation.forwardpremium.objects.CloudBackupSettings;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetUserDocsList {
    @GET("documents/")
    Call<List<CloudBackupDocs>> docs(@Header("Authorization") String token);
}
