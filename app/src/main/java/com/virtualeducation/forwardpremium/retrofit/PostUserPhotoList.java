package com.virtualeducation.forwardpremium.retrofit;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by DarKMonK on 09.08.2017.
 */

public interface PostUserPhotoList {
    @Multipart
    @POST("photo/upload/")
    Call<Void> uploadMultipleFilesDynamic(@Header("Authorization") String token,
                                          @Part MultipartBody.Part photo,
                                          @Part("uid") String uid,
                                          @Part("overwrite") boolean isOverwrite);
}
