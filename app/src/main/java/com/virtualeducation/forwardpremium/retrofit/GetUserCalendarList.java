package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CalendarObject;
import com.virtualeducation.forwardpremium.objects.CloudBackupCalendar;
import com.virtualeducation.forwardpremium.objects.CloudBackupSms;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetUserCalendarList {
    @GET("calendars/")
    Call<ArrayList<CalendarObject>> calendar(@Header("Authorization") String token);
}
