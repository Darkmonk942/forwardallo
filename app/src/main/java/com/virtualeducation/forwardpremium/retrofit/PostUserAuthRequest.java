package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 04.07.2017.
 */

public interface PostUserAuthRequest {

    // Получение юзера по токену
    @POST("users/get-by-token")
    Call<User> userGet(@Header("Authorization") String token);

    // Авторизация email+пароль
    @FormUrlEncoded
    @POST("users/login")
    Call<User> userLogin(@Field("email") String email,
                         @Field("password") String password);

    // Регистрация нового пользователя эмейл+пароль
    @FormUrlEncoded
    @POST("users/register")
    Call<User> userRegistration(@Field("email") String email,
                                @Field("password") String password,
                                @Field("activation_key") String isPremium);

    // Регистрация нового пользователя: Код активации + телефон + пароль
    @FormUrlEncoded
    @POST("users/register-by-phone")
    Call<User> userPhoneRegistration(@Field("activation_key") String code,
                                     @Field("phone") String phone,
                                     @Field("password") String password);

    // Вход в приложение уже зарегистрированного пользователя
    @FormUrlEncoded
    @POST("users/login")
    Call<User> userAuthByPhone(@Field("login") String phone,
                               @Field("password") String password);

    // Запрос кода для восстановления пароля
    @FormUrlEncoded
    @POST("users/password-reset-code-request")
    Call<Void> userRequestSms(@Field("login") String phone);

    // Откправка кода и нового пароля
    @FormUrlEncoded
    @POST("users/set-new-password")
    Call<Void> userSetPassword(@Field("login") String phone,
                               @Field("password") String password,
                               @Field("code") String code);

    // Восстановление пароля по эмейлу
    @FormUrlEncoded
    @POST("user/request-password-reset")
    Call<Void> userRecoverPass(@Field("email") String email);

}
