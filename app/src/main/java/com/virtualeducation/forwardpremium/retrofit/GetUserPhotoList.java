package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CloudBackupPhoto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Streaming;

/**
 * Created by DarKMonK on 27.08.2017.
 */

public interface GetUserPhotoList {
    @GET("photos")
    Call<List<CloudBackupPhoto>> photo(@Header("Authorization") String token);
}
