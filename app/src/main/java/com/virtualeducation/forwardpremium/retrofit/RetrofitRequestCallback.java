package com.virtualeducation.forwardpremium.retrofit;

/**
 * Created by DarKMonK on 19.03.2017.
 */

public interface RetrofitRequestCallback {
    void onEventSuccess(Object response);

    void onEventFailure(Throwable error);
}
