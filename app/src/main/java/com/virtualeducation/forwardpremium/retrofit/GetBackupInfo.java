package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.BackupInfoObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 25.07.2017.
 */

public interface GetBackupInfo {
    @GET("user/backup-info")
    Call<BackupInfoObject> backupInfo(@Header("Authorization") String token);
}
