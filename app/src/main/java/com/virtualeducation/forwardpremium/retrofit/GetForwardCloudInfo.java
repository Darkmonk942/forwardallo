package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CloudInfoObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 21.07.2017.
 */

public interface GetForwardCloudInfo {
    @GET("my-cloud/")
    Call<CloudInfoObject> getDataCount(@Header("Authorization") String token);
}
