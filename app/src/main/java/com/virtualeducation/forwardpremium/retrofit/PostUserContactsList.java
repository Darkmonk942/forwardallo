package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.ContactObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 01.08.2017.
 */

public interface PostUserContactsList {
    @Headers("Content-Type: application/json")
    @POST("contacts/")
    Call<Void> sendContactsList(@Header("Authorization") String token, @Body ArrayList<ContactObject> contactsList);
}
