package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CloudBackupContacts;
import com.virtualeducation.forwardpremium.objects.ContactObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 11.08.2017.
 */

public interface GetUserContactsList {
    @GET("contacts")
    Call<ArrayList<ContactObject>> contacts(@Header("Authorization") String token);
}
