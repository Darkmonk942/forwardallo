package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CloudBackupApps;
import com.virtualeducation.forwardpremium.objects.CloudBackupSettings;
import com.virtualeducation.forwardpremium.objects.SettingsObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetUserSettingsList {
    @GET("settings/")
    Call<ArrayList<SettingsObject>> settings(@Header("Authorization") String token);
}
