package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CallObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 20.07.2017.
 */

public interface PostUserCallsList {
    @Headers("Content-Type: application/json")
    @POST("call-log/")
    Call<Void> sendCallsList(@Header("Authorization") String token,@Body ArrayList<CallObject> callsList);
}
