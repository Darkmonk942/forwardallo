package com.virtualeducation.forwardpremium.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 04.07.2017.
 */

public interface GetAgentValidationRequest {
    @FormUrlEncoded
    @POST("user/activ-purchase")
    Call<Void> checkAgentValidation(@Field("agent_uid") String agentId,
                                    @Field("purchase_id") String agentPass,
                                    @Field("imei") String imei,
                                    @Field("phone") String phoneModel);
}
