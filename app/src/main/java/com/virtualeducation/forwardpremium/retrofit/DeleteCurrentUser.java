package com.virtualeducation.forwardpremium.retrofit;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 22.08.2017.
 */

public interface DeleteCurrentUser {
    @DELETE("user/delete")
    Call<Void> deleteUser(@Header("Authorization") String token);
}
