package com.virtualeducation.forwardpremium.retrofit;

import com.virtualeducation.forwardpremium.objects.CalendarObject;
import com.virtualeducation.forwardpremium.objects.CallObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PostUserCalendarList {
    @Headers("Content-Type: application/json")
    @POST("calendars/")
    Call<Void> sendCalendarList(@Header("Authorization") String token, @Body ArrayList<CalendarObject> calendarObjects);
}
