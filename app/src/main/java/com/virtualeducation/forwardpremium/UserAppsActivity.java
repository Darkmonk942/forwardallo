package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.adapters.AppsListAdapter;
import com.virtualeducation.forwardpremium.adapters.CallListAdapter;
import com.virtualeducation.forwardpremium.databinding.ActivityUserAppsListBinding;
import com.virtualeducation.forwardpremium.objects.AppIconObject;
import com.virtualeducation.forwardpremium.objects.AppsBackupObject;
import com.virtualeducation.forwardpremium.objects.AppsObject;
import com.virtualeducation.forwardpremium.objects.CloudBackupApps;
import com.virtualeducation.forwardpremium.retrofit.GetAppsBackupIcons;
import com.virtualeducation.forwardpremium.retrofit.GetUserAppsList;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.services.AppInstallReceiver;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UserAppsActivity extends AppCompatActivity {
    private ActivityUserAppsListBinding activityBinding;
    private AppsListAdapter appsListAdapter;

    RetrofitApi retrofitApi;
    GetUserAppsList getUserAppsList;
    AppInstallReceiver appInstallReceiver;
    char[] accessToken;
    UserAppsAllGet userAppsAllGet;
    GetAppsBackupIcons appsBackupIcons;

    ArrayList<AppsObject> allApps=new ArrayList<>();
    ArrayList<CloudBackupApps> allBackupApps=new ArrayList<>();

    boolean alreadyDownload;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding= DataBindingUtil.setContentView(this,R.layout.activity_user_apps_list);

        // Получаем accessToken текущего пользователя
        accessToken= Hawk.get("accessToken","").toCharArray();

        // Получаем приложения на телефоне пользователя
        userAppsAllGet=new UserAppsAllGet();
        allApps= userAppsAllGet.getAllApps(this);

        // Получаем приложения, которые в бекапе
        retrofitApi=RetrofitApi.getInstance();
        getUserAppsList=retrofitApi.retrofit.create(GetUserAppsList.class);
        appsBackupIcons=retrofitApi.retrofit.create(GetAppsBackupIcons.class);
        recyclerViewInit();

        Call<List<CloudBackupApps>> backupApps=getUserAppsList.apps(String.copyValueOf(accessToken));

        backupApps.enqueue(new Callback<List<CloudBackupApps>>() {
            @Override
            public void onResponse(Call<List<CloudBackupApps>> call, Response<List<CloudBackupApps>> response) {
                activityBinding.progressBar.setVisibility(View.GONE);
                Log.e("Апочки", String.valueOf(response.code()));
                if(response.code() == 200){
                    String names="";
                    allBackupApps.addAll(response.body());

                    for (int i=0;i<allBackupApps.size();i++){
                        if(i==allBackupApps.size()-1){
                            names=names+allBackupApps.get(i).appsList.appPackage;
                        }else{
                            names=names+allBackupApps.get(i).appsList.appPackage+",";
                        }

                        for (int j=0;j<allApps.size();j++){
                            if(allBackupApps.get(i).appsList.appPackage.equals(allApps.get(j).appPackage)){
                                allBackupApps.get(i).appsList.isInstall=true;
                            }
                        }
                    }
                    Log.e("Апочки", String.valueOf(allBackupApps.size()));
                    appsListAdapter.notifyDataSetChanged();
                    //getAppIcons(names);

                }else{

                }

            }

            @Override
            public void onFailure(Call<List<CloudBackupApps>> call, Throwable t) {
                activityBinding.progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void backClick(View v){
        finish();
    }

    // Получаем иконки для приложений
    private void getAppIcons(String names){
//        Log.d("имена",names);
//        Call<List<AppIconObject>> backupAppIcons=appsBackupIcons.getIcons(String.copyValueOf(accessToken),names);
//        backupAppIcons.enqueue(new Callback<List<AppIconObject>>() {
//            @Override
//            public void onResponse(Call<List<AppIconObject>> call, Response<List<AppIconObject>> response) {
//
//                activityBinding.progressBar.setVisibility(View.GONE);
//                if(response.code()==200){
//                    List<AppIconObject> icons=response.body();
//                    alreadyDownload=true;
//                    for (int i=0;i<allBackupApps.size();i++){
//                        for (int j=0;j<icons.size();j++){
//                            Log.d("конки",allBackupApps.get(i).appPackage+ " "+icons.get(j).id);
//                            if(allBackupApps.get(i).appPackage.equals(icons.get(j).id)){
//                                allBackupApps.get(i).appIcon=icons.get(j).icon;
//                            }
//                        }
//                    }
//                    appsListAdapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<AppIconObject>> call, Throwable t) {
//
//            }
//        });

    }

    // Проверка на установленные приложения

    // Инициализация RecyclerView
    private void recyclerViewInit(){
        activityBinding.recyclerApps.setHasFixedSize(true);
        activityBinding.recyclerApps.setLayoutManager(new LinearLayoutManager(this));
        appsListAdapter=new AppsListAdapter(allBackupApps,activityBinding,this);
        appsListAdapter.setHasStableIds(true);
        activityBinding.recyclerApps.setAdapter(appsListAdapter);
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(alreadyDownload){
            allApps= userAppsAllGet.getAllApps(this);
            for (int i=0;i<allBackupApps.size();i++){
                for (int j=0;j<allApps.size();j++){
                    if(allBackupApps.get(i).appsList.appPackage.equals(allApps.get(j).appPackage)){
                        allBackupApps.get(i).appsList.isInstall=true;
                    }
                }
            }
            appsListAdapter.notifyDataSetChanged();
        }

    }
}
