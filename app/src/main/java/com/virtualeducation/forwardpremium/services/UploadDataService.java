package com.virtualeducation.forwardpremium.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.virtualeducation.forwardpremium.MainScreenActivity;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.ToastManager;
import com.virtualeducation.forwardpremium.eventbus.UploadDataEvent;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UploadDataService extends Service {
    NotificationCompat status;
    RetrofitApi retrofitApi;
    AtomicInteger currentCall=new AtomicInteger(0);
    ArrayList<Call<Void>> allPostCalls=new ArrayList();
    RemoteViews views;
    RemoteViews viewsExp;
    int progressPart;
    NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;
    boolean isCancelable;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        retrofitApi= RetrofitApi.getInstance();
        allPostCalls.addAll(MainScreenActivity.allPostCalls);

        Log.d("посты", String.valueOf(allPostCalls.size()));
        progressPart=Math.round(100/allPostCalls.size());
        if(progressPart==0){
            progressPart=1;
        }
        showNotification();
        uploadDataToServer(allPostCalls.get(0));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("сервис", String.valueOf(intent.getIntExtra("type",0)));
        if(intent.getIntExtra("type",0)==1){
            ToastManager.createToast(getApplicationContext(),"Бекап остановлен пользователем");
            EventBus.getDefault().post(new UploadDataEvent(false,true,200));
            isCancelable=true;
            this.stopSelf();
        }
        return START_NOT_STICKY;
    }

    private void showNotification() {
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    // Using RemoteViews to bind custom layouts into Notification
        views = new RemoteViews(getPackageName(),
                R.layout.status_bar_ext);
        viewsExp=new RemoteViews(getPackageName(),R.layout.status_bar_exp);

        Intent pauseIntent = new Intent(getApplicationContext(), UploadDataService.class);
        pauseIntent.putExtra("type", 1);
        PendingIntent pause = PendingIntent.getService(getApplicationContext(), 1, pauseIntent, 0);
        viewsExp.setOnClickPendingIntent(R.id.text_cancel, pause);

        mBuilder = new NotificationCompat.Builder(this, "1")
                .setSmallIcon(R.drawable.ic_splash_logo)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCustomContentView(views)
                .setVibrate(null)
                .setCustomBigContentView(viewsExp);

        mNotificationManager.notify(
                1,
                mBuilder.build());

        startForeground(1,mBuilder.build());
    }

    // Upload user data
    private void uploadDataToServer(Call<Void> postCall){
            retrofitApi.createRequest(postCall);
            retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
                @Override
                public void onEventSuccess(Object responseObject) {
                    if(isCancelable){
                        return;
                    }
                    Response<Void> response=(Response<Void>)responseObject;
                    try {
                        //JSONObject jObjError = new JSONObject(response.errorBody().string());
                    } catch (Exception e) {
                    }
                    if(response.code()==200){
                        if(currentCall.addAndGet(1)==allPostCalls.size()){
                            EventBus.getDefault().post(new UploadDataEvent(false,true,0));
                        }else{
                            // Отправляем следующий запрос
                            setProgress(currentCall.get());
                            EventBus.getDefault().post(new UploadDataEvent(false,false,currentCall.get()-1));
                            uploadDataToServer(allPostCalls.get(currentCall.get()));
                        }
                    }else{
                        EventBus.getDefault().post(new UploadDataEvent(true,false,0));
                    }
                }

                @Override
                public void onEventFailure(Throwable error) {
                    EventBus.getDefault().post(new UploadDataEvent(true,false,0));
                }
            });
        }

        private void setProgress(int currentCall){
            float percent = 100f/allPostCalls.size();
            int progress = Math.round(percent*currentCall);
            views.setTextViewText(R.id.text_progress,String.valueOf(progress)+"%");
            viewsExp.setTextViewText(R.id.text_progress,String.valueOf(progress)+"%");

            mNotificationManager.notify(
                    1,
                    mBuilder.build());
        }

}
