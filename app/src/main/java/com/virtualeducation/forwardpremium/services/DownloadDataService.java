package com.virtualeducation.forwardpremium.services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.virtualeducation.forwardpremium.ModelRecoverDataManager;
import com.virtualeducation.forwardpremium.MyCloudScreenActivity;
import com.virtualeducation.forwardpremium.R;
import com.virtualeducation.forwardpremium.ToastManager;
import com.virtualeducation.forwardpremium.eventbus.DownloadDataEvent;
import com.virtualeducation.forwardpremium.objects.CalendarObject;
import com.virtualeducation.forwardpremium.objects.CallObject;
import com.virtualeducation.forwardpremium.objects.CloudBackupDocs;
import com.virtualeducation.forwardpremium.objects.CloudBackupPhoto;
import com.virtualeducation.forwardpremium.objects.ContactObject;
import com.virtualeducation.forwardpremium.objects.SettingsObject;
import com.virtualeducation.forwardpremium.objects.SmsObject;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import retrofit2.Call;
import retrofit2.Response;

public class DownloadDataService extends Service {
    NotificationCompat status;
    ModelRecoverDataManager recoverDataManager;
    RetrofitApi retrofitApi;
    AtomicInteger currentCall=new AtomicInteger(0);
    ArrayList<Call<?>> allCalls=new ArrayList();
    int progressPart;
    float progressPercent;
    boolean isCancelable;
    boolean isPhotoPresent;
    RemoteViews views;
    RemoteViews viewsExp;
    NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        retrofitApi= RetrofitApi.getInstance();
        recoverDataManager=new ModelRecoverDataManager(this);
        allCalls.addAll(MyCloudScreenActivity.allCalls);

        progressPart=Math.round(100/allCalls.size());
        progressPercent = 1f;

        if(progressPart==0){
            progressPart=1;
        }

        showNotification();
        downloadDataFromServer(allCalls.get(0));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent.getIntExtra("type",0)==1){
            ToastManager.createToast(getApplicationContext(),"Бекап остановлен пользователем");
            EventBus.getDefault().post(new DownloadDataEvent(DownloadDataEvent.DOWNLOAD_STOP,0,0));
            isCancelable=true;
            this.stopSelf();
        }
        return START_NOT_STICKY;
    }

    private void showNotification() {
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Using RemoteViews to bind custom layouts into Notification
        views = new RemoteViews(getPackageName(),
                R.layout.status_bar_download);
        viewsExp=new RemoteViews(getPackageName(),R.layout.status_bar_download_exp);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "1")
                .setSmallIcon(R.drawable.ic_splash_logo)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCustomContentView(views)
                .setCustomBigContentView(viewsExp);

        mNotificationManager.notify(
                1,
                mBuilder.build());

        startForeground(1,mBuilder.build());
    }

    // Upload user data
    private void downloadDataFromServer(Call<?> postCall){
        retrofitApi.createRequest(postCall);
        retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {
                if(isCancelable){
                    return;
                }
                Response<?> response=(Response<?>)responseObject;
                if(response.code()!=200||response.body()==null){
                    //ToastManager.createToast(getApplicationContext(),"Ошибка");
                    //isRecovering.set(true);
                    EventBus.getDefault().post(new DownloadDataEvent(DownloadDataEvent.DOWNLOAD_FAILED,0,0));
                    return;
                }

                int progress=(currentCall.get()+1)*progressPart;
                float progressPartPerc = (currentCall.get()+1)*progressPercent;
                Log.e("Проц", String.valueOf(progressPartPerc));
                EventBus.getDefault().post(new DownloadDataEvent(DownloadDataEvent.DOWNLOAD_START,progress, progressPartPerc));
                //setCopyProgress((currentCallCount.get()+1)*partProgress,currentCallCount.get()+1==allCalls.size(),partProgress);

                ArrayList<?> checkedList = (ArrayList<?>) response.body();
                if(checkedList.size() > 0 ){

                    if(checkedList.get(0) instanceof ContactObject){
                        // Восстанавливаем контакты
                        ArrayList<ContactObject> backupContacts=(ArrayList<ContactObject>)response.body();
                        recoverDataManager.recoverContacts(backupContacts);
                    }
                    if(checkedList.get(0) instanceof CallObject){
                        // Восстанавливаем звонки
                        ArrayList<CallObject> backupCalls =(ArrayList<CallObject>)response.body();
                        recoverDataManager.recoverCalls(backupCalls);
                    }
                    if(checkedList.get(0) instanceof SmsObject){
                        // Восстанавливаем смс
                        ArrayList<SmsObject> backupSms=(ArrayList<SmsObject>)response.body();
                        recoverDataManager.recoverSms(backupSms);

                        // Восстанавливаем дефолтное смс приложение
                    /*
                    if(defaultSmsApp!=null){
                        setDefaultSmsApp();
                    }*/

                    }
                    if(checkedList.get(0) instanceof CalendarObject){
                        // Восстанавливаем календарь
                        ArrayList<CalendarObject> backupCalendar=(ArrayList<CalendarObject>)response.body();
                        recoverDataManager.recoverCalendar(backupCalendar);

                    }
                    if(checkedList.get(0) instanceof SettingsObject){
                        // Восстанавливаем настройки
                        ArrayList<SettingsObject> backupSettings=(ArrayList<SettingsObject>)response.body();
                        recoverDataManager.recoverSettings(backupSettings);
                    }
                    if(checkedList.get(0) instanceof CloudBackupPhoto){
                        isPhotoPresent = true;
                        ArrayList<CloudBackupPhoto> photoBackup=(ArrayList<CloudBackupPhoto>)response.body();
                        recoverDataManager.recoverPhoto(photoBackup);
                    }
                    if(checkedList.get(0) instanceof CloudBackupDocs){
                        ArrayList<CloudBackupDocs> docsBackup=(ArrayList<CloudBackupDocs>)response.body();
                        recoverDataManager.recoverDocuments(docsBackup);
                    }
                }


                if(currentCall.addAndGet(1)==allCalls.size()){
                    if(isPhotoPresent){
                        EventBus.getDefault().post(new DownloadDataEvent(DownloadDataEvent.DOWNLOAD_START,0,0));
                    }else{
                        EventBus.getDefault().post(new DownloadDataEvent(DownloadDataEvent.DOWNLOAD_FINISH,0,0));
                    }
                }else{
                    downloadDataFromServer(allCalls.get(currentCall.get()));
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                Log.e("Ошибка", error.getLocalizedMessage());
                EventBus.getDefault().post(new DownloadDataEvent(DownloadDataEvent.DOWNLOAD_FAILED,0,0));
                //ToastManager.createToast(getApplicationContext(),"Ошибка");
                //isRecovering.set(true);
            }
        });
    }

}
