package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by DarKMonK on 19.07.2017.
 */

public class ToastManager {

    // Создание тоаста
    public static void createToast(Context context,String text){
        Toast.makeText(context, text,
                Toast.LENGTH_SHORT).show();
    }
}
