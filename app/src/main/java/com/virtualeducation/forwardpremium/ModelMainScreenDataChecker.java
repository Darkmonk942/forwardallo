package com.virtualeducation.forwardpremium;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;

import com.virtualeducation.forwardpremium.databinding.ActivityMainScreenBinding;

/**
 * Created by DarKMonK on 21.07.2017.
 */

public class ModelMainScreenDataChecker {
    ActivityMainScreenBinding mainScreenBinding;
    private ObjectAnimator objectAnimator;
    private int animDuration=300;
    ArgbEvaluator argbEvaluator;
    // Colors
    int colorContacts;
    int colorCalls;
    int colorSms;
    int colorPhoto;
    int colorNotes;
    int colorCalendar;
    int colorApps;
    int colorSettings;
    int colorGray;

    public ModelMainScreenDataChecker(ActivityMainScreenBinding mainScreenBinding,
                                      int colorContacts,int colorCalls,int colorSms,int colorPhoto,int colorGray,
                                      int colorNotes,int colorCalendar,int colorApps,int colorSettings){
        this.mainScreenBinding=mainScreenBinding;
        this.colorContacts=colorContacts;
        this.colorCalls=colorCalls;
        this.colorSms=colorSms;
        this.colorPhoto=colorPhoto;
        this.colorGray=colorGray;
        this.colorApps=colorApps;
        this.colorCalendar=colorCalendar;
        this.colorNotes=colorNotes;
        this.colorSettings=colorSettings;
        argbEvaluator=new ArgbEvaluator();
    }

    public void setDataCheck(String dataType,boolean isChecked){

        switch (dataType){
            case "contacts":
                if(isChecked){
                    mainScreenBinding.bgStrokeContacts.setBackgroundResource(R.drawable.shape_round_card_contacts);
                    mainScreenBinding.icCircleContacts.setBackgroundResource(R.drawable.shape_circle_contacts_main);
                    mainScreenBinding.icCheckContacts.setVisibility(View.VISIBLE);
                    mainScreenBinding.icContactsLabel.setImageResource(R.drawable.ic_phone_contacts_on);
                }else{
                    mainScreenBinding.bgStrokeContacts.setBackgroundResource(R.drawable.shape_round_card_white);
                    mainScreenBinding.icCircleContacts.setBackgroundResource(R.drawable.shape_circle_gray_stroke);
                    mainScreenBinding.icCheckContacts.setVisibility(View.GONE);
                    mainScreenBinding.icContactsLabel.setImageResource(R.drawable.ic_phone_contacts_off);
                }
                setColorAnimation(mainScreenBinding.imgLabelContacts,colorContacts,isChecked);
                break;
            case "calls":
                if(isChecked){
                    mainScreenBinding.bgStrokeCalls.setBackgroundResource(R.drawable.shape_round_card_calls);
                    mainScreenBinding.icCircleCalls.setBackgroundResource(R.drawable.shape_circle_calls_main);
                    mainScreenBinding.icCheckCalls.setVisibility(View.VISIBLE);
                    mainScreenBinding.icCallsLabel.setImageResource(R.drawable.ic_phone_calls_on);
                }else{
                    mainScreenBinding.bgStrokeCalls.setBackgroundResource(R.drawable.shape_round_card_white);
                    mainScreenBinding.icCircleCalls.setBackgroundResource(R.drawable.shape_circle_gray_stroke);
                    mainScreenBinding.icCheckCalls.setVisibility(View.GONE);
                    mainScreenBinding.icCallsLabel.setImageResource(R.drawable.ic_phone_calls_off);
                }
                setColorAnimation(mainScreenBinding.imgLabelCalls,colorCalls,isChecked);
                break;
            case "sms":
                if(isChecked){
                    mainScreenBinding.bgStrokeSms.setBackgroundResource(R.drawable.shape_round_card_sms);
                    mainScreenBinding.icCircleSms.setBackgroundResource(R.drawable.shape_circle_sms_main);
                    mainScreenBinding.icCheckSms.setVisibility(View.VISIBLE);
                    mainScreenBinding.icSmsLabel.setImageResource(R.drawable.ic_phone_sms_on);
                }else{
                    mainScreenBinding.bgStrokeSms.setBackgroundResource(R.drawable.shape_round_card_white);
                    mainScreenBinding.icCircleSms.setBackgroundResource(R.drawable.shape_circle_gray_stroke);
                    mainScreenBinding.icCheckSms.setVisibility(View.GONE);
                    mainScreenBinding.icSmsLabel.setImageResource(R.drawable.ic_phone_sms_off);
                }
                setColorAnimation(mainScreenBinding.imgLabelSms,colorSms,isChecked);
                break;
            case "photo":
                if(isChecked){
                    mainScreenBinding.bgStrokePhoto.setBackgroundResource(R.drawable.shape_round_card_photo);
                    mainScreenBinding.icCirclePhoto.setBackgroundResource(R.drawable.shape_circle_photo_main);
                    mainScreenBinding.icCheckPhoto.setVisibility(View.VISIBLE);
                    mainScreenBinding.icPhotoLabel.setImageResource(R.drawable.ic_phone_gallery_on);
                }else{
                    mainScreenBinding.bgStrokePhoto.setBackgroundResource(R.drawable.shape_round_card_white);
                    mainScreenBinding.icCirclePhoto.setBackgroundResource(R.drawable.shape_circle_gray_stroke);
                    mainScreenBinding.icCheckPhoto.setVisibility(View.GONE);
                    mainScreenBinding.icPhotoLabel.setImageResource(R.drawable.ic_phone_gallery_off);
                }
                setColorAnimation(mainScreenBinding.imgLabelPhoto,colorPhoto,isChecked);
                break;
            case "calendar":
                if(isChecked){
                    mainScreenBinding.bgStrokeCalendar.setBackgroundResource(R.drawable.shape_round_card_calendar);
                    mainScreenBinding.icCircleCalendar.setBackgroundResource(R.drawable.shape_circle_calendar_main);
                    mainScreenBinding.icCheckCalendar.setVisibility(View.VISIBLE);
                    mainScreenBinding.icPhotoCalendar.setImageResource(R.drawable.ic_calendar_on);
                }else{
                    mainScreenBinding.bgStrokeCalendar.setBackgroundResource(R.drawable.shape_round_card_white);
                    mainScreenBinding.icCircleCalendar.setBackgroundResource(R.drawable.shape_circle_gray_stroke);
                    mainScreenBinding.icCheckCalendar.setVisibility(View.GONE);
                    mainScreenBinding.icPhotoCalendar.setImageResource(R.drawable.ic_calendar_off);
                }
                setColorAnimation(mainScreenBinding.imgLabelCalendar,colorCalendar,isChecked);
                break;
            case "apps":
                if(isChecked){
                    mainScreenBinding.bgStrokeApps.setBackgroundResource(R.drawable.shape_round_card_apps);
                    mainScreenBinding.icCircleApps.setBackgroundResource(R.drawable.shape_circle_apps_main);
                    mainScreenBinding.icCheckApps.setVisibility(View.VISIBLE);
                    mainScreenBinding.icAppsLabel.setImageResource(R.drawable.ic_apps_on);
                }else{
                    mainScreenBinding.bgStrokeApps.setBackgroundResource(R.drawable.shape_round_card_white);
                    mainScreenBinding.icCircleApps.setBackgroundResource(R.drawable.shape_circle_gray_stroke);
                    mainScreenBinding.icCheckApps.setVisibility(View.GONE);
                    mainScreenBinding.icAppsLabel.setImageResource(R.drawable.ic_apps_off);
                }
                setColorAnimation(mainScreenBinding.imgLabelApps,colorApps,isChecked);
                break;
            case "notes":
                if(isChecked){
                    mainScreenBinding.bgStrokeNotes.setBackgroundResource(R.drawable.shape_round_card_notes);
                    mainScreenBinding.icCircleNotes.setBackgroundResource(R.drawable.shape_circle_notes_main);
                    mainScreenBinding.icCheckNotes.setVisibility(View.VISIBLE);
                    mainScreenBinding.icNotesLabel.setImageResource(R.drawable.ic_notes_on);
                }else{
                    mainScreenBinding.bgStrokeNotes.setBackgroundResource(R.drawable.shape_round_card_white);
                    mainScreenBinding.icCircleNotes.setBackgroundResource(R.drawable.shape_circle_gray_stroke);
                    mainScreenBinding.icCheckNotes.setVisibility(View.GONE);
                    mainScreenBinding.icNotesLabel.setImageResource(R.drawable.ic_notes_off);
                }
                setColorAnimation(mainScreenBinding.imgLabelNotes,colorNotes,isChecked);
                break;
            case "settings":
                if(isChecked){
                    mainScreenBinding.bgStrokeSettings.setBackgroundResource(R.drawable.shape_round_card_settings);
                    mainScreenBinding.icCircleSettings.setBackgroundResource(R.drawable.shape_circle_settings_main);
                    mainScreenBinding.icCheckSettings.setVisibility(View.VISIBLE);
                    mainScreenBinding.icPhotoSettings.setImageResource(R.drawable.ic_settings_on);
                }else{
                    mainScreenBinding.bgStrokeSettings.setBackgroundResource(R.drawable.shape_round_card_white);
                    mainScreenBinding.icCircleSettings.setBackgroundResource(R.drawable.shape_circle_gray_stroke);
                    mainScreenBinding.icCheckSettings.setVisibility(View.GONE);
                    mainScreenBinding.icPhotoSettings.setImageResource(R.drawable.ic_settings_off);
                }
                setColorAnimation(mainScreenBinding.imgLabelSettings,colorSettings,isChecked);
                break;
        }
    }

    // Анимация перекрашивания лейбла категории
    private void setColorAnimation(ImageView labelCategory,int colorLabel,boolean isChecked){
        if(isChecked) {
            objectAnimator = ObjectAnimator.ofObject(labelCategory, "backgroundColor",
                    argbEvaluator,
                    colorGray,
                    colorLabel);
        }else{
            objectAnimator = ObjectAnimator.ofObject(labelCategory, "backgroundColor",
                    argbEvaluator,
                    colorLabel,
                    colorGray);
        }
        objectAnimator.setDuration(animDuration);
        objectAnimator.start();
    }
}
