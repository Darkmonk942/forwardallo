package com.virtualeducation.forwardpremium;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.FrameLayout;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.databinding.ActivityAgentScreenBinding;
import com.virtualeducation.forwardpremium.retrofit.GetAgentValidationRequest;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import butterknife.BindString;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 29.06.2017.
 */

public class AgentScreenActivity extends AppCompatActivity {
    ActivityAgentScreenBinding agentBinding;
    GetAgentValidationRequest agentRequest;
    ProgressDialog alertDialog;

    int tenPxHeight;
    // Strings
    @BindString(R.string.alert_agent_screen_login_failed)
    String alertAgentValidFailed;
    String alertRequestFailed = "Ошибка регистрации агента";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        agentBinding = DataBindingUtil.setContentView(this, R.layout.activity_agent_screen);

        // Получаем параметры экрана
        int screenHeight = Hawk.get("height", 0);
        tenPxHeight = screenHeight / 192;

        // Оптимизация интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(agentBinding.frameInner);
        optimization.Optimization(agentBinding.frameAgentNumber);
        optimization.Optimization(agentBinding.frameAgentPass);


        // Формируем запрос на проверку валидности агента
        agentRequest = RetrofitApi.getInstance().retrofit.create(GetAgentValidationRequest.class);

        // Добавляем пустую зону в Скрол при открытой клавиатуре
        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        Log.d("Клава", String.valueOf(isOpen));
                        if (isOpen) {
                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                            params.setMargins(0, -1 * tenPxHeight * 30, 0, 0);
                            agentBinding.frameInner.setLayoutParams(params);
                        } else {
                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                            params.setMargins(0, 0, 0, 0);
                            agentBinding.frameInner.setLayoutParams(params);
                        }
                    }
                });

        agentBinding.editAgentPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
               /* if(editable.length()==3&&editable.length()>0){

                    if ('/' == editable.charAt(editable.length()-1)) {
                        editable.delete(editable.length() - 1, editable.length());
                    }
                }

                if(editable.length()==3&&editable.length()>0){
                    editable.insert(editable.length()-1,"/");
                }*/

            }
        });
    }

    // Клик, для начала проверки параметров агента
    public void checkAgentDataClick(View v) {
        String agentId = agentBinding.editAgentId.getText().toString();
        String agentPass = agentBinding.editAgentPass.getText().toString();

        if (!agentId.equals("") && !agentPass.equals("")) {
            // Отправляем запрос на сервер
            checkAgentData(agentId, agentPass);
        } else {
            ToastManager.createToast(getApplicationContext(), alertAgentValidFailed);
        }

    }

    // Запрос к api для проверки введённых параметров агента
    private void checkAgentData(String agentId, String agentPass) {
        createProgressAlert(true);

        Pair<String, String> phoneInfo = getUserPhone();

        Call<Void> agentRequestCall = agentRequest.checkAgentValidation(agentId, agentPass, phoneInfo.first, phoneInfo.second);
        RetrofitApi.getInstance().createRequest(agentRequestCall);
        RetrofitApi.getInstance().setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {
                createProgressAlert(false);
                Response<Void> response = (Response<Void>) responseObject;

                if (response.code() == 200) {
                    agentPurchaseAccept();
                } else {
                    ToastManager.createToast(getApplicationContext(), alertRequestFailed);
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                createProgressAlert(false);
                ToastManager.createToast(getApplicationContext(), alertRequestFailed);
            }
        });
    }

    // Получаем imei и марку телефона
    private Pair<String, String> getUserPhone() {
        String imei = "";

        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                imei = telephonyManager.getDeviceId();
            }

        }catch (Exception e){}

        String deviceCompany = Build.MANUFACTURER;
        String deviceModel = Build.MODEL;

        return new Pair<>(imei,deviceCompany+" "+deviceModel);
    }

    // Если введенная дата агента верна, то подтверждаем продажу агентом
    private void agentPurchaseAccept(){
        startActivity(new Intent(this,AuthScreenActivity.class));
        Hawk.put("userAuthStage","auth");
        finish();
    }

    public void createProgressAlert(boolean setVisibility){

        if(setVisibility){
            alertDialog = ProgressDialog.show(this, "",
                    "Проверка...", true);
        }else {
            if(alertDialog!=null) {
                alertDialog.dismiss();
            }
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if ( alertDialog!=null && alertDialog.isShowing() ){
            alertDialog.dismiss();
        }
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnerInfo() {

    }

}
