package com.virtualeducation.forwardpremium;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;

import com.virtualeducation.forwardpremium.objects.CalendarObject;

import java.util.ArrayList;
import java.util.Calendar;

public class UserCalendarEventGetAll {
    ArrayList<CalendarObject> allEventsList=new ArrayList<>();

    public ArrayList<CalendarObject> getCalendarEvents(Context context) {
        String[] projection = new String[]{CalendarContract.Events.CALENDAR_ID, CalendarContract.Events.TITLE, CalendarContract.Events.DESCRIPTION, CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND, CalendarContract.Events.ALL_DAY, CalendarContract.Events.EVENT_LOCATION,CalendarContract.Events.ACCOUNT_TYPE,
                CalendarContract.Events.ACCOUNT_NAME,CalendarContract.Events.EVENT_TIMEZONE};

        // 0 = January, 1 = February, ...

        Calendar startTime = Calendar.getInstance();
        startTime.set(2010, 00, 01, 00, 00);

        Calendar endTime = Calendar.getInstance();
        endTime.set(2020, 8, 01, 00, 00);


        String selection = "(( " + CalendarContract.Events.DTSTART + " >= " + startTime.getTimeInMillis() + " ) AND ( " + CalendarContract.Events.DTSTART + " <= " + endTime.getTimeInMillis() + " ))";

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return allEventsList;
        }

        try{
            Cursor cursor = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI, projection, selection, null, null);


            if (cursor.moveToFirst()) {
                do {
                    allEventsList.add(new CalendarObject(cursor.getString(1),cursor.getString(2),cursor.getLong(3),cursor.getLong(4),cursor.getInt(0),
                            cursor.getString(9),cursor.getString(7),cursor.getString(8)));
                    //Log.d("календарь","Id: "+ cursor.getString(0)+" Title: " + cursor.getString(1) + " Start-Time: " + (new Date(cursor.getLong(3))).toString()+" End-Time "+(new Date(cursor.getLong(4))).toString());
                } while ( cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return allEventsList;
    }

    private void addCal(){
/*long calID = 3;
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2018, 9, 15, 7, 30);
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime1 = Calendar.getInstance();
        endTime1.set(2018, 9, 15, 8, 45);
        endMillis = endTime1.getTimeInMillis();

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, "Новый тайтл2");
        values.put(CalendarContract.Events.DESCRIPTION, "Group workout");
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Los_Angeles");
        Uri uri = CalendarContract.Events.CONTENT_URI;
        uri = uri.buildUpon().appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER,"true")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, "darkmonk942@gmail.com")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, "com.google").build();
        cr.insert(uri, values);*/
    }
}
