package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.virtualeducation.forwardpremium.eventbus.PhotoListEvent;
import com.virtualeducation.forwardpremium.objects.PhotoObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 25.07.2017.
 */

public class UserPhotoGetAll {
    private Thread photoThread;
    public void getAllPhoto(Context context){
        Uri allImagesUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        final Cursor cursorImages = context.getContentResolver().query(allImagesUri, null, null, null,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME + " ASC");

        // Получаем номера нужных столбцов из таблицы фото
        final int columnIndexId=cursorImages.getColumnIndex(MediaStore.Images.Media._ID);
        final int columnIndexPhotoUri=cursorImages.getColumnIndex(MediaStore.Images.Media.DATA);
        final int columnPhotoName=cursorImages.getColumnIndex(MediaStore.Images.Media.TITLE);
        final int columnPhotoCategory=cursorImages.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        final int columnIndexThumbnailUri=cursorImages.getColumnIndex(MediaStore.Images.Thumbnails.DATA);

        // Создаём новый поток для получения списка звонков
        photoThread = new Thread(
                new Runnable() {
                    public void run() {
                        final ArrayList<PhotoObject> allPhotoList=new ArrayList();
                        while (cursorImages.moveToNext()){

                            try {
                                PhotoObject photoObject = new PhotoObject();
                                photoObject.id = cursorImages.getString(columnIndexId);
                                photoObject.photoUri = cursorImages.getString(columnIndexPhotoUri);
                                photoObject.photoName = cursorImages.getString(columnPhotoName);
                                photoObject.photoCategory = cursorImages.getString(columnPhotoCategory);
                                photoObject.photoThumbnail=cursorImages.getString(columnIndexThumbnailUri);

                                allPhotoList.add(photoObject);
                            }catch (Exception e){

                            }
                        }

                        EventBus.getDefault().post(new PhotoListEvent(allPhotoList,false));
                        photoThread.interrupt();
                        cursorImages.close();
                    }
                }
        );
        photoThread.start();
    }
}
