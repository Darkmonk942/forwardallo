package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;

import com.virtualeducation.forwardpremium.eventbus.CallsListEvent;
import com.virtualeducation.forwardpremium.objects.CallObject;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by DarKMonK on 11.07.2017.
 */

public class UserCallsGetAll {

    // Получаем список всех звонков пользователя
    public void getAllCalls(Context context){
        Uri allCallsUri = Uri.parse("content://call_log/calls");
        final Cursor cursorCalls = context.getContentResolver().query(allCallsUri, null, null, null,
                CallLog.Calls.DATE+" DESC");

        // Получаем номера нужных колонок из таблицы звонков
        final int columnIndexNumber=cursorCalls.getColumnIndex(CallLog.Calls.NUMBER);
        final int columnIndexType=cursorCalls.getColumnIndex(CallLog.Calls.TYPE);
        final int columnIndexDate=cursorCalls.getColumnIndex(CallLog.Calls.DATE);
        final int columnIndexDuration=cursorCalls.getColumnIndex(CallLog.Calls.DURATION);
        final int columnIndexId=cursorCalls.getColumnIndex(CallLog.Calls._ID);
        final int columnIndexName=cursorCalls.getColumnIndex(CallLog.Calls.CACHED_NAME);

        // Создаём новый поток для получения списка звонков
        Thread callsThread = new Thread(
                new Runnable() {
                    public void run() {
                        final ArrayList<CallObject> allCallsList=new ArrayList();
                        // Получаем данные по звонкам
                        while (cursorCalls.moveToNext()){

                            if(allCallsList.size()>=500){ // Обработка только 500 контактов
                                break;
                            }
                            try {
                                CallObject callObject = new CallObject();
                                callObject.callNumber = cursorCalls.getString(columnIndexNumber);
                                callObject.callType = cursorCalls.getString(columnIndexType);
                                callObject.callDate = cursorCalls.getString(columnIndexDate);
                                callObject.callDuration = cursorCalls.getString(columnIndexDuration);
                                callObject.id = cursorCalls.getString(columnIndexId);
                                callObject.callName = cursorCalls.getString(columnIndexName);

                                // Переводим дату в формат 8 января(16:50)
                                Date date = new Date(Long.valueOf(callObject.callDate));
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM HH:mm");
                                callObject.callDateDayTime = dateFormat.format(date);

                                // Добавляем данные в лист
                                allCallsList.add(callObject);
                            }catch (Exception e){

                            }
                        }

                        EventBus.getDefault().post(new CallsListEvent(allCallsList,false));
                        cursorCalls.close();
                    }
                }
        );

        callsThread.start();
    }
}
