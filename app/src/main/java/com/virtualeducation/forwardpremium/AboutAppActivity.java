package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.databinding.ActivityAboutAppBinding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import butterknife.BindString;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 07.08.2017.
 */

public class AboutAppActivity extends AppCompatActivity {
    ActivityAboutAppBinding aboutAppBinding;
    AlertDialog alertDialog;

    Optimization optimization;
    String bodyText="";
    @BindString(R.string.url_facebook) String urlFacebook;
    @BindString(R.string.url_instagram) String urlInstagram;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        aboutAppBinding= DataBindingUtil.setContentView(this,R.layout.activity_about_app);

        setPartnersInfo();

        // Оптимизация интерфейса
        optimization=new Optimization();
        optimization.Optimization(aboutAppBinding.frameMain);

        getAboutTextFromFile();

        // Определяем тип пользователя
        if(Hawk.get("premium",false)){
            aboutAppBinding.icCloudBlue.setImageResource(R.drawable.ic_main_logo_blue);
        }
    }

    // Переход на предыдущий главный экран
    public void backClick(View v){
        finish();
    }

    // Переход на пользовательское соглашение
    public void termsUseClick(View v){
        if(alertDialog!=null){
            alertDialog.show();
        }
    }

    // Переход на маркет для оценки приложения
    public void rateAppClick(View v){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=com.virtualeducation.forwardpremium"));
        startActivity(intent);
    }

    // Переходы на соцсети приложения
    public void socialClick(View v){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        switch (v.getId()){
            case R.id.ic_facebook:
                intent.setData(Uri.parse(urlFacebook));
                break;
            case R.id.ic_instagram:
                intent.setData(Uri.parse(urlInstagram));
                break;
        }
        startActivity(intent);
    }

    // Получаем текст из файла для пользовательского соглашения
    private void getAboutTextFromFile(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("terms.txt"), "UTF-8"));

            String termsText="";
            // do reading, usually loop until end of file reading
            while(termsText != null){
                termsText=reader.readLine();
                bodyText=bodyText+termsText+"\n"+"\n";
            }
            createTermsDialog(bodyText.replace("null","").trim());

        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
    }

    // Создаём диалог для отображения пользовательского соглашения
    private void createTermsDialog(String bodyText){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        FrameLayout dialogView = (FrameLayout)getLayoutInflater().inflate(R.layout.alert_about_app,null);
        optimization.Optimization(dialogView);
        alert.setView(dialogView);

        TextView termsText = (TextView) dialogView.findViewById(R.id.text_body);
        termsText.setText(bodyText);
        alertDialog = alert.create();
        alertDialog.setCancelable(true);
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnersInfo() {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                aboutAppBinding.frameHeader.setBackgroundColor(getResources().getColor(R.color.colorSulpak));
                aboutAppBinding.textData.setTextColor(getResources().getColor(R.color.colorSulpak));
                aboutAppBinding.star.setImageResource(R.drawable.ic_star_sulpak);
                aboutAppBinding.icCloudBlue.setImageResource(R.drawable.ic_sulpak_red);
                aboutAppBinding.terms.setImageResource(R.drawable.ic_note_sulpak);
                aboutAppBinding.logo.setImageResource(R.drawable.ic_logo_sulpak);
                break;
        }
    }
}
