package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.virtualeducation.forwardpremium.adapters.CallListAdapter;
import com.virtualeducation.forwardpremium.databinding.ActivityUserCallsListBinding;
import com.virtualeducation.forwardpremium.eventbus.CallsListEvent;
import com.virtualeducation.forwardpremium.objects.CallObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 09.07.2017.
 */

public class UserCallsListActivity extends AppCompatActivity {
    ActivityUserCallsListBinding userCallsListBinding;
    Optimization optimization;
    CallListAdapter callListAdapter;
    ArrayList<CallObject> allCallsListBackup=new ArrayList();
    ArrayList<CallObject> allCallsList=new ArrayList();
    int callsCount=0;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        userCallsListBinding= DataBindingUtil.setContentView(this,R.layout.activity_user_calls_list);

        // Оптимизация интерфейса
        optimization=new Optimization();
        optimization.Optimization(userCallsListBinding.frameMain);

        getAllCalls();
    }

    // Инициализация RecyclerView
    private void recyclerViewInit(){
        userCallsListBinding.recyclerCall.setHasFixedSize(true);
        userCallsListBinding.recyclerCall.setLayoutManager(new LinearLayoutManager(this));
        callListAdapter=new CallListAdapter(allCallsList,userCallsListBinding,optimization);
        callListAdapter.setHasStableIds(true);
        userCallsListBinding.recyclerCall.setAdapter(callListAdapter);
    }

    // Переход на главный экран
    public void backClick(View v) {
        EventBus.getDefault().post(new CallsListEvent(allCallsListBackup,true));
        finish();
    }

    // Подтверждаем выборанные звонки
    public void btnConfirmClick(View v){
        EventBus.getDefault().post(new CallsListEvent(allCallsList,true));
        finish();
    }

    // Выбираем/Отключаем все звонки
    public void chooseAllCallsClick(View v){
        if(userCallsListBinding.icCallsCheck.getVisibility()==View.VISIBLE){
            callListAdapter.currentCallsCount=0;
            userCallsListBinding.textCallsCount.setText("0/"+String.valueOf(callsCount));
            userCallsListBinding.icCallsCheck.setVisibility(View.GONE);
            setAllCallsCheck(false);
        }else{
            callListAdapter.currentCallsCount=callsCount;
            userCallsListBinding.textCallsCount.setText(String.valueOf(callsCount)+"/"+String.valueOf(callsCount));
            userCallsListBinding.icCallsCheck.setVisibility(View.VISIBLE);
            setAllCallsCheck(true);
        }
    }

    // Изменяем чекер на всех звонках
    private void setAllCallsCheck(boolean isCheck){
        for (int i=0;i<allCallsList.size();i++){
            allCallsList.get(i).isChecked=isCheck;
        }
        callListAdapter.notifyDataSetChanged();
    }

    // Получаем список звонков
    private void getAllCalls(){
        allCallsList.addAll(MainScreenActivity.allCallsList);
        callsCount=allCallsList.size();

        // Сохраняем весь список звонков для восстановления после изменений
        cloneCallsList();

        // Инициализация RecyclerView
        recyclerViewInit();

        // Получаем все выбранные вызовы
        for (int i=0;i<allCallsList.size();i++) {
            if (allCallsList.get(i).isChecked) {
                callListAdapter.currentCallsCount = callListAdapter.currentCallsCount + 1;
            }
        }

        userCallsListBinding.textCallsCount.setText(String.valueOf(callListAdapter.currentCallsCount)+"/"+String.valueOf(callsCount));
    }

    // Сохраняем весь список звонков для восстановления после изменений
    private void cloneCallsList(){
        Thread callsThread = new Thread(
                new Runnable() {
                    public void run() {
                        for (CallObject callObject:allCallsList){
                            allCallsListBackup.add(callObject.clone());
                        }
                    }
                }
        );
        callsThread.start();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    // Передаём event на главную страницу в виде листа выбранных звонков
    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new CallsListEvent(allCallsListBackup,true));
        finish();
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
