package com.virtualeducation.forwardpremium;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.objects.User;
import com.virtualeducation.forwardpremium.retrofit.PostUserAuthRequest;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Получаем текущий язык
        if(Hawk.get("language","ru").equals("ru")){
            Locale locale = new Locale("ru");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }else{
            Locale locale = new Locale("ua");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }

        // Получаем текущий прогресс использования приложения
        switch (Hawk.get("userAuthStage","language")){
            case "language":
                startActivity(new Intent(this,ChooseLanguageActivity.class));
                finish();
                break;
            case "tutorial":
                startActivity(new Intent(this,TutorialScreenActivity.class));
                finish();
                break;
            case "promocode":
                startActivity(new Intent(this,PromocodeActivity.class));
                finish();
                break;
            case "agent":
                startActivity(new Intent(this,AgentScreenActivity.class));
                finish();
                break;
            case "auth":
                startActivity(new Intent(this,AuthScreenActivity.class));
                finish();
                break;
            case "inUse":
                //startActivity(new Intent(this,AuthScreenActivity.class));
                userAuth(getIntent().getBooleanExtra("isPush",false));
                break;
        }
    }

    private void userAuth(final boolean isPush){
        if(!CheckInternetConnection.isConnected(this)){
            startActivity(new Intent(this,AuthScreenActivity.class));
            ToastManager.createToast(this,"Отсутствует интернет соединение");
            return;
        }



        RetrofitApi retrofitApi=RetrofitApi.getInstance();
        PostUserAuthRequest authRequest= retrofitApi.retrofit.create(PostUserAuthRequest.class);
        authRequest.userGet(Hawk.get("accessToken","")).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.code()!=200||response.body()==null){
                    startActivity(new Intent(SplashScreen.this,AuthScreenActivity.class));
                }else{
//                    Hawk.put("premium",response.body().isPremium);
                    Hawk.put("userData",response.body());
                    startActivity(new Intent(SplashScreen.this,MainScreenActivity.class).putExtra("isPush",isPush));
                }
                finish();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Hawk.put("userAuthStage","auth");
                startActivity(new Intent(SplashScreen.this,AuthScreenActivity.class));
                finish();
            }
        });
    }
}
