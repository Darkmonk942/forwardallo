package com.virtualeducation.forwardpremium;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.VideoView;

import com.amplitude.api.Amplitude;
import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.adapters.TutorialAdapter;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 02.08.2017.
 */

public class TutorialScreenActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.video_player) VideoView videoView;
    @BindView(R.id.btn_start) Button btnStart;
    @BindView(R.id.frame_main) FrameLayout mainFrame;

    // Strings
    @BindString(R.string.version_type) String versionType;
    @BindString(R.string.version_type_allo) String versionTypeAllo;
    String dangerPerms[]={Manifest.permission.READ_CONTACTS,Manifest.permission.READ_SMS,
            Manifest.permission.WRITE_CONTACTS,Manifest.permission.SEND_SMS,Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_CALL_LOG,Manifest.permission.READ_CALL_LOG,Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_CALENDAR,Manifest.permission.WRITE_CALENDAR};
    final int PERMS_PACK=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_screen);
        ButterKnife.bind(this);

        // Получаем параметры экрана
        getScreenParams();

        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(mainFrame);

        // VideoView для отображения анимированного лого
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() +"/"+R.raw.video_logo));
        videoView.start();

        // Подключаем адаптер для пейджера (Туториал)
        TutorialAdapter tutorialAdapter=new TutorialAdapter(this,versionType.equals("AlloAgentOn"));
        viewPager.setAdapter(tutorialAdapter);

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                viewPager.setVisibility(View.VISIBLE);
                btnStart.setVisibility(View.VISIBLE);

                // Запрашиваем нужные разрешения
                requestPermissions();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        btnStart.setBackgroundResource(R.drawable.shape_btn_tutor_start1);
                        break;
                    case 1:
                        btnStart.setBackgroundResource(R.drawable.shape_btn_tutor_start2);
                        break;
                    case 2:
                        btnStart.setBackgroundResource(R.drawable.shape_btn_tutor_start3);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Чекаем нового пользователя
        if(Hawk.get("newUser",true)&&versionType.equals("AlloAgentOff")) {
            if(!versionTypeAllo.equals("AlloAgentOn")){
                Amplitude.getInstance().logEvent("NEW_USER");
            }
            Hawk.put("newUser",false);
        }else{
            Hawk.put("newUser",false);
        }

        // Чекаем тип версии
    }

    // Запрашиваем нужные разрешения
    private void requestPermissions(){
        if (EasyPermissions.hasPermissions(this, dangerPerms)) {
        } else {
            EasyPermissions.requestPermissions(this, "Подтвердите ",
                    PERMS_PACK, dangerPerms);
        }
    }

    // Пропускаем туториал
    public void skipTutorClick(View v){

        if(versionTypeAllo.equals("AlloAgentOn")){
            Hawk.put("userAuthStage","agent");
            startActivity(new Intent(this, AgentScreenActivity.class));
            finish();
        }else{
            if(versionType.equals("PromoCode")){
                startActivity(new Intent(this,PromocodeActivity.class));
                Hawk.put("userAuthStage","promocode");
                finish();
            }
            if(versionType.equals("GooglePlay")||versionType.equals("AlloAgentOff")){
                startActivity(new Intent(this,AuthScreenActivity.class));
                Hawk.put("userAuthStage","auth");
                finish();
            }
        }


    }

    // Получаем параметры экрана
    public void getScreenParams(){
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display =wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height=size.y;
        int width=size.x;

        // Сохраняем параметры экрана
        if(Hawk.get("height",0)==0){
            Hawk.put("height",height);
            Hawk.put("width",width);
        }
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    // Обработка результата полученных разрешений
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if(perms.size()>0){
            finish();
        }
    }
}
