package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.virtualeducation.forwardpremium.objects.AppsObject;

import java.util.ArrayList;
import java.util.List;

public class UserAppsAllGet {

    public ArrayList<AppsObject> getAllApps(Context context){
        ArrayList<AppsObject> appsList=new ArrayList<>();

        int flags = PackageManager.GET_META_DATA |
                PackageManager.GET_SHARED_LIBRARY_FILES |
                PackageManager.GET_UNINSTALLED_PACKAGES;

        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> applications = pm.getInstalledApplications(flags);
        for (ApplicationInfo appInfo : applications) {
            if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                // System application
            } else {
                AppsObject appsObject=new AppsObject();
                appsObject.appName=pm.getApplicationLabel(appInfo).toString();
                appsObject.appPackage=appInfo.packageName;
                appsObject.appVendor=appInfo.packageName;
                appsList.add(appsObject);

                Log.d("апка",pm.getApplicationLabel(appInfo).toString() +"  "+appInfo.packageName);
            }
        }
        return appsList;
    }
}
