package com.virtualeducation.forwardpremium;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import com.amplitude.api.Amplitude;
import com.bugsnag.android.Bugsnag;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by DarKMonK on 10.07.2017.
 */

public class AppForward extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.createNotificationChannel(new NotificationChannel("1", "Upload Service", NotificationManager.IMPORTANCE_DEFAULT));
            }

        // Alarm manager
        //JobManager.create(this).addJobCreator(new AlarmJobCreator());
        Bugsnag.init(this);

        // NoSql db manager
        Hawk.init(this)
                .setEncryption(new NoEncryption())
                .build();

        // Analytics
        Amplitude.getInstance().initialize(this, "16d8c78bf81ceea02705caca3ca1ffe5").enableForegroundTracking(this);

        // Custom fonts
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/roboto_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
