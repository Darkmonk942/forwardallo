package com.virtualeducation.forwardpremium;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.databinding.ActivityMyCloudScreenBinding;
import com.virtualeducation.forwardpremium.eventbus.DownloadDataEvent;
import com.virtualeducation.forwardpremium.eventbus.PhotoDownloadEvent;
import com.virtualeducation.forwardpremium.eventbus.UploadDataEvent;
import com.virtualeducation.forwardpremium.objects.CalendarObject;
import com.virtualeducation.forwardpremium.objects.CallObject;
import com.virtualeducation.forwardpremium.objects.CloudBackupCalendar;
import com.virtualeducation.forwardpremium.objects.CloudBackupCalls;
import com.virtualeducation.forwardpremium.objects.CloudBackupContacts;
import com.virtualeducation.forwardpremium.objects.CloudBackupDocs;
import com.virtualeducation.forwardpremium.objects.CloudBackupPhoto;
import com.virtualeducation.forwardpremium.objects.CloudBackupSettings;
import com.virtualeducation.forwardpremium.objects.CloudBackupSms;
import com.virtualeducation.forwardpremium.objects.CloudInfoObject;
import com.virtualeducation.forwardpremium.objects.ContactObject;
import com.virtualeducation.forwardpremium.objects.SettingsObject;
import com.virtualeducation.forwardpremium.objects.SmsObject;
import com.virtualeducation.forwardpremium.retrofit.GetForwardCloudInfo;
import com.virtualeducation.forwardpremium.retrofit.GetUserAppsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserCalendarList;
import com.virtualeducation.forwardpremium.retrofit.GetUserCallsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserContactsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserDocsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserPhotoList;
import com.virtualeducation.forwardpremium.retrofit.GetUserSettingsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserSmsList;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;
import com.virtualeducation.forwardpremium.services.DownloadDataService;
import com.virtualeducation.forwardpremium.services.UploadDataService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 18.07.2017.
 */

public class MyCloudScreenActivity extends AppCompatActivity {
    ActivityMyCloudScreenBinding cloudScreenBinding;

    RetrofitApi retrofitApi;
    GetForwardCloudInfo getForwardCloudInfo;
    GetUserContactsList getUserContactsList;
    GetUserSmsList getUserSmsList;
    GetUserCallsList getUserCallsList;
    GetUserPhotoList getUserPhotoList;
    GetUserAppsList getUserAppsList;
    GetUserCalendarList getUserCalendarList;
    GetUserSettingsList getUserSettingsList;
    GetUserDocsList getUserDocsList;

    ModelRecoverDataManager recoverDataManager;
    Intent serviceIntent;
    // Примитивы
    public static List<Call<?>> allCalls;
    private static final int DEF_SMS_REQ = 0;
    AtomicInteger currentCallCount=new AtomicInteger(0);
    char[] accessToken;
    public final ObservableBoolean isRecovering=new ObservableBoolean();
    boolean isContactsRecover;
    boolean isSmsRecover;
    boolean isPhotoRecover;
    boolean isCallsRecover;
    boolean isDocsRecover;
    boolean isSettingsRecover;
    boolean isCalendarRecover;
    float lastProgress = 0f;
    String defaultSmsApp;
    ObjectAnimator copyAnimation;
    final Handler handler = new Handler();
    // Colors
    @BindColor(R.color.colorMainContacts) int colorContacts;
    @BindColor(R.color.colorMainCalls) int colorCalls;
    @BindColor(R.color.colorMainPhoto) int colorPhoto;
    @BindColor(R.color.colorMainSms) int colorSms;
    @BindColor(R.color.colorMainApps) int colorApps;
    @BindColor(R.color.colorMainSettings) int colorSettings;
    @BindColor(R.color.colorMainNotes) int colorDocs;
    @BindColor(R.color.colorMainCalendar) int colorCalendar;
    @BindColor(R.color.colorBlue) int colorBlue;
    @BindColor(R.color.colorGray) int colorGray;
    // Strings
    @BindString(R.string.alert_wi_fi) String wifiAlertString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        cloudScreenBinding= DataBindingUtil.setContentView(this,R.layout.activity_my_cloud_screen);
        recoverDataManager=new ModelRecoverDataManager(this);
        isRecovering.set(true);
        cloudScreenBinding.setIsRecovering(isRecovering);

        setPartnersInfo();

        serviceIntent = new Intent(this, DownloadDataService.class);

        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(cloudScreenBinding.frameMain);
        optimization.Optimization(cloudScreenBinding.frameHeaderCopy);
        optimization.Optimization(cloudScreenBinding.frameHeader);
        optimization.OptimizationLinear(cloudScreenBinding.linearProgress);
        optimization.OptimizationLinear(cloudScreenBinding.linearTypes);

        retrofitApi=RetrofitApi.getInstance();

        // Получаем accessToken текущего пользователя
        accessToken=Hawk.get("accessToken","").toCharArray();

        // Формируем запросы для получения информации по облаку
        getForwardCloudInfo=retrofitApi.retrofit.create(GetForwardCloudInfo.class);
        getUserContactsList=retrofitApi.retrofit.create(GetUserContactsList.class);
        getUserSmsList=retrofitApi.retrofit.create(GetUserSmsList.class);
        getUserCallsList=retrofitApi.retrofit.create(GetUserCallsList.class);
        getUserPhotoList=retrofitApi.retrofit.create(GetUserPhotoList.class);
        getUserAppsList=retrofitApi.retrofit.create(GetUserAppsList.class);
        getUserSettingsList=retrofitApi.retrofit.create(GetUserSettingsList.class);
        getUserCalendarList=retrofitApi.retrofit.create(GetUserCalendarList.class);
        getUserDocsList=retrofitApi.retrofit.create(GetUserDocsList.class);

        // Получаем краткую информацию по бэкапу
        getCloudData();

        /*
        // Возвращаем предыдущее дефолтное смс приложение
        Intent intentDefaultSmsApp = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
        intentDefaultSmsApp.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, defaultSmsApp);
        startActivity(intentDefaultSmsApp);*/
    }

    // Переход назад на главный экран
    public void backClick(View v){
        finish();
    }

    // Возврат дефолтного смс приложения
    private void setDefaultSmsApp(){
        if(defaultSmsApp!=null){
            Intent intentDefaultSmsApp = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intentDefaultSmsApp.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, defaultSmsApp);
            startActivity(intentDefaultSmsApp);
        }
    }

    //  Проверки перед восстановлением данных
    public void btnRecoverDataClick(View v){

        // Проверка на отправку только по Wi-Fi
        if(Hawk.get("settingsWifi",true)){
            if(!CheckInternetConnection.isConnectedWifi(this)){
                ToastManager.createToast(this,wifiAlertString);
                return;
            }
        }

        // Проверка на настройки для восстановления настроек
        if(!Hawk.get("seetings_write",false)){
            ToastManager.createToast(this,"Разрешите приложению управлять настройками телефона");
            Hawk.put("seetings_write",true);
            openAndroidPermissionsMenu();
            return;
        }

        // Проверка на возможность восстановления
        if(!isContactsRecover&&!isCallsRecover&&!isPhotoRecover&&!isSmsRecover&&!isDocsRecover&&!isCalendarRecover&&!isSettingsRecover){
            return;
        }

        allCalls=new ArrayList();
        currentCallCount.set(0);
        cloudScreenBinding.progressCopy.setProgress(0);
        cloudScreenBinding.textProgress.setText("0");

        // Создаём колы для нужных запросов
        if (isContactsRecover){
            Call<ArrayList<ContactObject>> contactsCall=getUserContactsList.contacts(String.copyValueOf(accessToken));
            allCalls.add(contactsCall);
        }
        if(isSmsRecover){
            Call<ArrayList<SmsObject>> smsCall=getUserSmsList.sms(String.copyValueOf(accessToken));
            allCalls.add(smsCall);
        }
        if(isCallsRecover){
            Call<ArrayList<CallObject>> callsCall=getUserCallsList.calls(String.copyValueOf(accessToken));
            allCalls.add(callsCall);
        }
        if(isPhotoRecover){
            Call<List<CloudBackupPhoto>> photoCall=getUserPhotoList.photo(String.copyValueOf(accessToken));
            allCalls.add(photoCall);
        }
        if(isSettingsRecover){
            Call<ArrayList<SettingsObject>> settingsCall=getUserSettingsList.settings(String.copyValueOf(accessToken));
            allCalls.add(settingsCall);
        }
        if(isCalendarRecover){
            Call<ArrayList<CalendarObject>> calendarCall=getUserCalendarList.calendar(String.copyValueOf(accessToken));
            allCalls.add(calendarCall);
        }
        if(isDocsRecover){
            Call<List<CloudBackupDocs>> docsCall=getUserDocsList.docs(String.copyValueOf(accessToken));
            allCalls.add(docsCall);
        }

        // Проверка на восстановление СМС
        if(isSmsRecover&&!Telephony.Sms.getDefaultSmsPackage(this).equals(this.getPackageName())) {
            defaultSmsApp=Telephony.Sms.getDefaultSmsPackage(this);
            // Запрос на изменение дефолтного смс приложения
            Intent intentNewDefaultSmsApp = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intentNewDefaultSmsApp.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, this.getPackageName());
            startActivityForResult(intentNewDefaultSmsApp, DEF_SMS_REQ);
            return;
        }

        if(allCalls.size()>0) {
            isRecovering.set(false);
            startService(serviceIntent);
            //restoreCloudData(allCalls.get(0));
        }
    }

    // Получаем ивенты о текущем восстановлении данных
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void uploadEvent(DownloadDataEvent dataEvent){
        switch (dataEvent.type){
            case DownloadDataEvent.DOWNLOAD_FAILED:
                ToastManager.createToast(getApplicationContext(),"Ошибка");
                isRecovering.set(true);
                allCalls.clear();
                stopService(serviceIntent);
                break;
            case DownloadDataEvent.DOWNLOAD_FINISH:
                Log.e("Проц", "Finish");
                endOfCopy();
                stopService(serviceIntent);
                    break;
            case DownloadDataEvent.DOWNLOAD_START:

                setCopyProgress(dataEvent.progress,false, dataEvent.progressFloat);
                break;
            case DownloadDataEvent.DOWNLOAD_STOP:
                break;
        }
    }

    /*private void restoreCloudData(Call<?> currentCall){
        retrofitApi.createRequest(currentCall);
        retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {

                Response<?> response=(Response<?>)responseObject;
                if(response.code()!=200&&response.body()==null){
                    ToastManager.createToast(getApplicationContext(),"Ошибка");
                    isRecovering.set(true);
                    return;
                }

                int partProgress=Math.round(100/allCalls.size());
                setCopyProgress((currentCallCount.get()+1)*partProgress,currentCallCount.get()+1==allCalls.size(),partProgress);

                if(response.body() instanceof CloudBackupContacts){
                    // Восстанавливаем контакты
                    CloudBackupContacts backupContacts=(CloudBackupContacts)response.body();
                    recoverDataManager.recoverContacts(backupContacts.contactsList);
                }
                if(response.body() instanceof CloudBackupCalls){
                    // Восстанавливаем звонки
                    CloudBackupCalls backupCalls=(CloudBackupCalls)response.body();
                    recoverDataManager.recoverCalls(backupCalls.callsList);
                }
                if(response.body() instanceof CloudBackupSms){
                    // Восстанавливаем смс
                    CloudBackupSms backupSms=(CloudBackupSms)response.body();
                    recoverDataManager.recoverSms(backupSms.smsList);

                    // Восстанавливаем дефолтное смс приложение

                    if(defaultSmsApp!=null){
                        setDefaultSmsApp();
                    }

                }
                if(response.body() instanceof CloudBackupCalendar){
                    // Восстанавливаем календарь
                    CloudBackupCalendar backupCalendar=(CloudBackupCalendar)response.body();
                    recoverDataManager.recoverCalendar(backupCalendar.calendarList);

                }
                if(response.body() instanceof CloudBackupSettings){
                    // Восстанавливаем настройки
                    CloudBackupSettings backupSettings=(CloudBackupSettings)response.body();
                    recoverDataManager.recoverSettings(backupSettings.settingsList);
                }
                if(response.body() instanceof List){

                    // Восстанавливаем фото
                    List<Object> backupFiles=(List<Object>) response.body();

                    if(backupFiles.size()>0){
                        if(backupFiles.get(0) instanceof CloudBackupPhoto){
                            List<CloudBackupPhoto> photoBackup=(List<CloudBackupPhoto>)response.body();
                            recoverDataManager.recoverPhoto(photoBackup);
                        }
                        if(backupFiles.get(0) instanceof CloudBackupDocs){
                            List<CloudBackupDocs> docsBackup=(List<CloudBackupDocs>)response.body();
                            recoverDataManager.recoverDocuments(docsBackup);
                        }
                    }
                }

                if(currentCallCount.addAndGet(1)==allCalls.size()){
                    if(!isPhotoRecover){
                        endOfCopy();
                    }

                }else{
                    restoreCloudData(allCalls.get(currentCallCount.get()));
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                Log.d("Ошибочка",error.toString());
                ToastManager.createToast(getApplicationContext(),"Ошибка");
                isRecovering.set(true);
            }
        });
    }*/

    // Отображение прогресса получения
    private void setCopyProgress(int progress,boolean isLast, float percent){

        if(copyAnimation!=null){
            if(copyAnimation.isRunning()){
                copyAnimation.pause();
            }
        }

        if(isLast){
            if(isPhotoRecover){
                progress=90;
            }else{
                progress=100;
            }
            //percent=100;
        }
        Log.d("Проггресс",String.valueOf(percent));
        if(percent > 99) {
            percent = 99;
        }

        if(percent <= lastProgress){
            return;
        }
        lastProgress = percent;
        copyAnimation = ObjectAnimator.ofInt(cloudScreenBinding.progressCopy, "progress",Math.round(percent));
        copyAnimation.setDuration(100);
        copyAnimation.setInterpolator(new DecelerateInterpolator());
        copyAnimation.start();

        cloudScreenBinding.textProgress.setText(String.valueOf(Math.round(percent)));
//        cloudScreenBinding.textProgress
//                .setAnimationDuration(400)
//                .countAnimation(Integer.valueOf(cloudScreenBinding.textProgress.getText().toString()), Math.round(percent));

    }

    // Завершаем всю выкачку на сервер
    private void endOfCopy(){
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(Environment.getExternalStorageDirectory() + "/ForwardBackup");
        Uri contentUri = Uri.fromFile(file); // out is your output file
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Заканчиваем выкачку на сервер
                isRecovering.set(true);
                ToastManager.createToast(getApplicationContext(),"Данные успешно восстановлены");
            }
        }, 400);
        setDefaultSmsApp();
    }

    @Subscribe
    public void handlePhotoEvent(PhotoDownloadEvent photoDownloadEvent) {
        if(photoDownloadEvent.isLast){
            if(copyAnimation!=null){
                if(copyAnimation.isRunning()){
                    copyAnimation.pause();
                }
            }

            copyAnimation = ObjectAnimator.ofInt(cloudScreenBinding.progressCopy, "progress",100);
            copyAnimation.setDuration(1000);
            copyAnimation.setInterpolator(new DecelerateInterpolator());
            //copyAnimation.start();

//            cloudScreenBinding.textProgress
//                    .setAnimationDuration(1000)
//                    .countAnimation(Integer.valueOf(cloudScreenBinding.textProgress.getText().toString()), 100);

            endOfCopy();
            stopService(serviceIntent);
        }
    }

    // Получаем всю информацию о Бэкапе
    private void getCloudData(){
        Call<CloudInfoObject> getCloudInfoCall=getForwardCloudInfo.getDataCount(String.copyValueOf(accessToken));

        retrofitApi.createRequest(getCloudInfoCall);
        retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {
                cloudScreenBinding.progressBar.setVisibility(View.GONE);
                cloudScreenBinding.linearTypes.setVisibility(View.VISIBLE);
                cloudScreenBinding.btnRestoreData.setVisibility(View.VISIBLE);
                Response<CloudInfoObject> response=(Response<CloudInfoObject>)responseObject;

                if(response.code()!=200||response.body()==null){
                    return;
                }
                Log.d("Облако", String.valueOf(response.code()));
                // Отображаем кол-во данных полученных с сервера
                CloudInfoObject cloudInfoObject=response.body();
                cloudScreenBinding.linearTypes.setVisibility(View.VISIBLE);
                cloudScreenBinding.textCloudContacts.setText(String.valueOf(cloudInfoObject.contactsCount));
                cloudScreenBinding.textCloudSms.setText(String.valueOf(cloudInfoObject.smsCount));
                cloudScreenBinding.textCloudCalls.setText(String.valueOf(cloudInfoObject.callsCount));
                cloudScreenBinding.textCloudPhoto.setText(String.valueOf(cloudInfoObject.photoCount));
                cloudScreenBinding.textCloudCalendar.setText(String.valueOf(cloudInfoObject.calendarCount));
                cloudScreenBinding.textCloudDocs.setText(String.valueOf(cloudInfoObject.docsCount));
                cloudScreenBinding.textCloudSettings.setText(String.valueOf(cloudInfoObject.settingsCount));
                cloudScreenBinding.textCloudApp.setText(String.valueOf(cloudInfoObject.appsCount));

                // Выстраиваем круговую диаграмму
                cloudScreenBinding.chartCloud.setVisibility(View.VISIBLE);
                createDataChart(cloudInfoObject);
            }

            @Override
            public void onEventFailure(Throwable error) {
                Log.d("Облако", error.toString());
            }
        });
    }

    // Создаём круговую диаграмму для отображения всех файлов в облаке
    private void createDataChart(CloudInfoObject cloudInfoObject){
        cloudScreenBinding.chartCloud.setHoleRadius(75f);
        cloudScreenBinding.chartCloud.setDrawCenterText(false);
        cloudScreenBinding.chartCloud.setRotationEnabled(true);
        cloudScreenBinding.chartCloud.setHoleColor(Color.TRANSPARENT);
        cloudScreenBinding.chartCloud.getDescription().setEnabled(false);
        cloudScreenBinding.chartCloud.setExtraOffsets(0,2.f,0,2.f);

        Legend l = cloudScreenBinding.chartCloud.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setEnabled(false);

        setChartData(cloudInfoObject);

        cloudScreenBinding.chartCloud.animateY(1000, Easing.EasingOption.EaseInOutQuad);

    }

    private void setChartData(CloudInfoObject cloudInfoObject){

        int totalCount=cloudInfoObject.callsCount+cloudInfoObject.contactsCount
                +cloudInfoObject.photoCount+cloudInfoObject.smsCount+
                cloudInfoObject.calendarCount+cloudInfoObject.appsCount+
                cloudInfoObject.docsCount+cloudInfoObject.settingsCount;

        float smsPart=((float)cloudInfoObject.smsCount/(float)totalCount)*100;
        float contactsPart=((float)cloudInfoObject.contactsCount/(float)totalCount)*100;
        float callsPart=((float)cloudInfoObject.callsCount/(float)totalCount)*100;
        float photoPart=((float)cloudInfoObject.photoCount/(float)totalCount)*100;
        float calendarPart=((float)cloudInfoObject.calendarCount/(float)totalCount)*100;
        float settingsPart=((float)cloudInfoObject.settingsCount/(float)totalCount)*100;
        float appsPart=((float)cloudInfoObject.appsCount/(float)totalCount)*100;
        float docsPart=((float)cloudInfoObject.docsCount/(float)totalCount)*100;

        ArrayList<PieEntry> entries = new ArrayList();
        entries.add(new PieEntry(contactsPart,""));
        entries.add(new PieEntry(callsPart,""));
        entries.add(new PieEntry(photoPart,""));
        entries.add(new PieEntry(smsPart,""));
        entries.add(new PieEntry(calendarPart,""));
        entries.add(new PieEntry(settingsPart,""));
        entries.add(new PieEntry(appsPart,""));
        entries.add(new PieEntry(docsPart,""));

        PieDataSet dataSet = new PieDataSet(entries, " ");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(0f);

        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(colorContacts);
        colors.add(colorCalls);
        colors.add(colorPhoto);
        colors.add(colorSms);
        colors.add(colorCalendar);
        colors.add(colorSettings);
        colors.add(colorApps);
        colors.add(colorDocs);

        dataSet.setColors(colors);

        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        dataSet.setValueTextColor(Color.TRANSPARENT);
        dataSet.setValueLineColor(Color.TRANSPARENT);

        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.TRANSPARENT);
        //data.setValueTextColors(colors);
        cloudScreenBinding.chartCloud.setData(data);

        // undo all highlights
        cloudScreenBinding.chartCloud.highlightValues(null);

        cloudScreenBinding.chartCloud.invalidate();
    }

    // Чекеры для выделения категорий восстановления данных
    public void setCheckClick(View v){
        Log.e("чек",v.getTag().toString());

        switch (v.getTag().toString()){
            case "contacts":
                if(isContactsRecover){
                    isContactsRecover=false;
                    ((ImageView)v).setImageResource(R.drawable.ic_uncheck_cloud);
                    cloudScreenBinding.icContacts.setImageResource(R.drawable.ic_phone_contacts_off);
                    cloudScreenBinding.textContacts.setTextColor(colorGray);
                    cloudScreenBinding.textCloudContacts.setTextColor(colorGray);
                }else{
                    if(cloudScreenBinding.textCloudContacts.getText().toString().equals("0"))
                        return;
                    isContactsRecover=true;

                    setIcon("contact");
                    //cloudScreenBinding.lottieContactsCheck.playAnimation();

                }
                break;
            case "calls":
                if(isCallsRecover){
                    isCallsRecover=false;
                    ((ImageView)v).setImageResource(R.drawable.ic_uncheck_cloud);
                    cloudScreenBinding.icCalls.setImageResource(R.drawable.ic_phone_calls_off);
                    cloudScreenBinding.textCalls.setTextColor(colorGray);
                    cloudScreenBinding.textCloudCalls.setTextColor(colorGray);
                }else{
                    if(cloudScreenBinding.textCloudCalls.getText().toString().equals("0"))
                        return;
                    isCallsRecover=true;

                    setIcon("call");
                }
                break;
            case "sms":
                if(isSmsRecover){
                    isSmsRecover=false;
                    ((ImageView)v).setImageResource(R.drawable.ic_uncheck_cloud);
                    cloudScreenBinding.icSms.setImageResource(R.drawable.ic_phone_sms_off);
                    cloudScreenBinding.textSms.setTextColor(colorGray);
                    cloudScreenBinding.textCloudSms.setTextColor(colorGray);
                }else{
                    if(cloudScreenBinding.textCloudSms.getText().toString().equals("0"))
                        return;
                    isSmsRecover=true;

                    setIcon("sms");
                }
                break;
            case "photo":
                if(isPhotoRecover){
                    isPhotoRecover=false;
                    ((ImageView)v).setImageResource(R.drawable.ic_uncheck_cloud);
                    cloudScreenBinding.icPhoto.setImageResource(R.drawable.ic_phone_gallery_off);
                    cloudScreenBinding.textPhoto.setTextColor(colorGray);
                    cloudScreenBinding.textCloudPhoto.setTextColor(colorGray);
                }else{
                    if(cloudScreenBinding.textCloudPhoto.getText().toString().equals("0"))
                        return;
                    isPhotoRecover=true;

                    setIcon("photo");
                }
                break;
            case "docs":
                if(isDocsRecover){
                    isDocsRecover=false;
                    ((ImageView)v).setImageResource(R.drawable.ic_uncheck_cloud);
                    cloudScreenBinding.icDocs.setImageResource(R.drawable.ic_recover_docs_off);
                    cloudScreenBinding.textDocs.setTextColor(colorGray);
                    cloudScreenBinding.textCloudDocs.setTextColor(colorGray);
                }else{
                    if(cloudScreenBinding.textCloudDocs.getText().toString().equals("0"))
                        return;
                    isDocsRecover=true;

                    setIcon("doc");
                }
                break;
            case "calendar":
                if(isCalendarRecover){
                    isCalendarRecover=false;
                    ((ImageView)v).setImageResource(R.drawable.ic_uncheck_cloud);
                    cloudScreenBinding.icCalendar.setImageResource(R.drawable.ic_recover_calendar_off);
                    cloudScreenBinding.textCalendar.setTextColor(colorGray);
                    cloudScreenBinding.textCloudCalendar.setTextColor(colorGray);
                }else{
                    if(cloudScreenBinding.textCloudCalendar.getText().toString().equals("0"))
                        return;
                    isCalendarRecover=true;

                    setIcon("calendar");
                }
                break;
            case "settings":
                if(isSettingsRecover){
                    isSettingsRecover=false;
                    ((ImageView)v).setImageResource(R.drawable.ic_uncheck_cloud);
                    cloudScreenBinding.icSettings.setImageResource(R.drawable.ic_recover_settinngs_off);
                    cloudScreenBinding.textSettings.setTextColor(colorGray);
                    cloudScreenBinding.textCloudSettings.setTextColor(colorGray);
                }else{
                    if(cloudScreenBinding.textCloudSettings.getText().toString().equals("0"))
                        return;
                    isSettingsRecover=true;

                    setIcon("setting");
                }
                break;
        }
    }

    // Переход на отдельную категорию бэкапа со списком эл-тов
    public void infoCloudClick(View v){
        switch (v.getTag().toString()){
            case "apps":
                startActivity(new Intent(this,UserAppsActivity.class));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            case DEF_SMS_REQ:
                if(resultCode == Activity.RESULT_OK){
                    ToastManager.createToast(MyCloudScreenActivity.this,"Теперь вы можете восстановить свои смс из облака");
                }
            break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // Cancel current call
        if(!isRecovering.get()){
            if(allCalls!=null) {
                if(currentCallCount.get()!=0){
                    if (allCalls.get(currentCallCount.get() - 1) != null) {
                        allCalls.get(currentCallCount.get() - 1).cancel();
                        isRecovering.set(true);
                    }
                }
            }
        }
    }

    private void openAndroidPermissionsMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + "com.virtualeducation.forwardpremium"));
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        stopService(serviceIntent);
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnersInfo() {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                cloudScreenBinding.frameTop.setBackgroundColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.frameHeader.setBackgroundColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.icApps.setImageResource(R.drawable.ic_apps_sulpak);
                cloudScreenBinding.icLogo.setImageResource(R.drawable.ic_cloud_chart_sulpak);
                cloudScreenBinding.btnRestoreData.setBackgroundResource(R.drawable.shape_btn_start_sulpak);
                cloudScreenBinding.icLogoApp.setImageResource(R.drawable.ic_cloud_chart_sulpak);
                cloudScreenBinding.progressCopy.setProgressDrawable(getResources().getDrawable(R.drawable.gradient_progress_sulpak));
                cloudScreenBinding.icChevron.setImageResource(R.drawable.ic_chevron_sulpak);
                break;
        }
    }

    private void setIcon(String type) {
        switch (type){
            case "sms":
                cloudScreenBinding.icSms.setImageResource(R.drawable.ic_sms_sulpak);
                cloudScreenBinding.textCloudSms.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.textSms.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.icSmsCheck.setImageResource(R.drawable.ic_cloud_check_sulpak);
                break;
            case "contact":
                cloudScreenBinding.icContacts.setImageResource(R.drawable.ic_contacts_sulpak);
                cloudScreenBinding.textCloudContacts.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.textContacts.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.icContactsCheck.setImageResource(R.drawable.ic_cloud_check_sulpak);
                break;
            case "calendar":
                cloudScreenBinding.icCalendar.setImageResource(R.drawable.ic_calendar_sulpak);
                cloudScreenBinding.textCloudCalendar.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.textCalendar.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.icCalendarCheck.setImageResource(R.drawable.ic_cloud_check_sulpak);
                break;
            case "call":
                cloudScreenBinding.icCalls.setImageResource(R.drawable.ic_calls_sulpak);
                cloudScreenBinding.textCloudCalls.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.textCalls.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.icCallsCheck.setImageResource(R.drawable.ic_cloud_check_sulpak);
                break;
            case "photo":
                cloudScreenBinding.icPhoto.setImageResource(R.drawable.ic_photo_sulpak);
                cloudScreenBinding.textCloudPhoto.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.textPhoto.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.icPhotoCheck.setImageResource(R.drawable.ic_cloud_check_sulpak);
                break;
            case "doc":
                cloudScreenBinding.icDocs.setImageResource(R.drawable.ic_docs_sulpak);
                cloudScreenBinding.textCloudDocs.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.textDocs.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.icDocsCheck.setImageResource(R.drawable.ic_cloud_check_sulpak);
                break;
            case "setting":
                cloudScreenBinding.icSettings.setImageResource(R.drawable.ic_settings_sulpak);
                cloudScreenBinding.textCloudSettings.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.textSettings.setTextColor(getResources().getColor(R.color.colorSulpak));
                cloudScreenBinding.icSettingsCheck.setImageResource(R.drawable.ic_cloud_check_sulpak);
                break;
        }
    }
}
