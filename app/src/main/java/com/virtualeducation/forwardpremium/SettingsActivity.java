package com.virtualeducation.forwardpremium;

import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.alarms.JobSchedulerService;
import com.virtualeducation.forwardpremium.databinding.ActivitySettingsScreenBinding;
import com.virtualeducation.forwardpremium.objects.User;
import com.virtualeducation.forwardpremium.retrofit.DeleteCurrentUser;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 06.08.2017.
 */

public class SettingsActivity extends AppCompatActivity {
    ActivitySettingsScreenBinding settingsScreenBinding;
    ProgressDialog alertDialog;

    private RetrofitApi retrofitApi;
    private DeleteCurrentUser deleteCurrentUser;

    private boolean isWifiChecked;
    private boolean isStatsChecked;
    private boolean isNotificationsChecked;

    private char[] accessToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingsScreenBinding= DataBindingUtil.setContentView(this,R.layout.activity_settings_screen);

        setPartnersInfo();

        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(settingsScreenBinding.frameMain);

        // Получаем токен текущего пользователя
        accessToken= Hawk.get("accessToken","").toCharArray();

        // NetworkManager
        retrofitApi=RetrofitApi.getInstance();

        // Компануем запрос на удаление текущего пользователя
        deleteCurrentUser=retrofitApi.retrofit.create(DeleteCurrentUser.class);

        // Получаем сохранённые данные
        isWifiChecked= Hawk.get("settingsWifi",true);
        isStatsChecked= Hawk.get("settingsStats",true);
        isNotificationsChecked=Hawk.get("backupNotify",true);

        settingsScreenBinding.switchSyncWifi.setChecked(isWifiChecked);
        settingsScreenBinding.switchStats.setChecked(isStatsChecked);
        settingsScreenBinding.switchNotifications.setChecked(isNotificationsChecked);

        // Слушатели на свичи
        spinnersListeners();

        // Определяем тип пользователя
        if(Hawk.get("premium",false)){
            settingsScreenBinding.icCloudBlue.setImageResource(R.drawable.ic_main_logo_blue);
        }

        // Определяем текущий язык
        if(Hawk.get("language","ru").equals("ru")){
            settingsScreenBinding.textLanguageUa.setBackgroundResource(R.drawable.img_btn_unactive);
        }else{
            settingsScreenBinding.textLanguageRu.setBackgroundResource(R.drawable.img_btn_unactive);
        }
    }

    // Переход на главный экран
    public void backClick(View v){
        finish();
    }

    // Слушатели на свичи
    private void spinnersListeners(){
        settingsScreenBinding.switchSyncWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isEnable) {
                isWifiChecked=isEnable;
            }
        });
        settingsScreenBinding.switchStats.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isEnable) {
                isStatsChecked=isEnable;
            }
        });

        settingsScreenBinding.switchNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isEnable) {
                notificationManager(isEnable);
            }
        });
    }

    // Переключение языков
    public void setCurrentLanguage(View v){
        if(v.getId()==R.id.text_language_ru){
            Locale locale = new Locale("ru");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            Hawk.put("newLanguage",true);
            Hawk.put("language","ru");
        }else{
            Locale locale = new Locale("ua");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            Hawk.put("newLanguage",true);
            Hawk.put("language","ua");
        }
        recreate();
    }

    // Вкл/выкл нотификаций копирования
    private void notificationManager(boolean isEnable){
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancelAll();
        Long currentPeriodic= TimeUnit.DAYS.toMillis(1);
        switch (Hawk.get("backupCalendarType",1)){
            case 1:
                currentPeriodic=TimeUnit.DAYS.toMillis(1);
                break;
            case 2:
                currentPeriodic=TimeUnit.DAYS.toMillis(7);
                break;
            case 3:
                currentPeriodic=TimeUnit.DAYS.toMillis(30);
                break;
        }

        if(isEnable){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                jobScheduler.schedule(new JobInfo.Builder(1,
                        new ComponentName(this, JobSchedulerService.class))
                        .setPersisted(true)
                        .setPeriodic(currentPeriodic,JobInfo.getMinFlexMillis())
                        .build());
            }else{
                jobScheduler.schedule(new JobInfo.Builder(1,
                        new ComponentName(this, JobSchedulerService.class))
                        .setPersisted(true)
                        .setPeriodic(TimeUnit.MINUTES.toMillis(15))
                        .build());
            }
        }
        Hawk.put("backupNotify",isEnable);
    }

    // Удаление текущего аккаунта
    public void accountDeleteClick(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Удалить текущий аккаунт?")
                .setCancelable(true)
                .setPositiveButton("Да",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                                accountDelete();
                            }
                        })
                .setNegativeButton("Нет",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
        builder.show();
    }

    private void accountDelete(){
        createProgressAlert(true);
        Call<Void> deleteUserCall=deleteCurrentUser.deleteUser(String.copyValueOf(accessToken));
        retrofitApi.createRequest(deleteUserCall);
        retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {
                createProgressAlert(false);
                Response<Void> response=(Response<Void>)responseObject;
                if(response.code()==200){
                    Hawk.put("userData",new User());
                    Hawk.put("userAuthStage","auth");
                    Intent startScreenIntent = new Intent(SettingsActivity.this, AuthScreenActivity.class);
                    startScreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(startScreenIntent);
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                createProgressAlert(false);
                Log.d("юзер",error.toString());

            }
        });
    }

    // Сохраняем изменение в графике
    @Override
    public void onPause() {
        super.onPause();
        Hawk.put("settingsWifi",isWifiChecked);
        Hawk.put("settingsStats",isStatsChecked);
        Hawk.put("backupNotify",isNotificationsChecked);
    }

    // Менеджер АлертДиалога
    public void createProgressAlert(boolean setVisibility){

        if(setVisibility){
            alertDialog = ProgressDialog.show(this, "",
                    "Подождите...", true);
        }else {
            if(alertDialog!=null) {
                alertDialog.dismiss();
            }
        }
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnersInfo() {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                settingsScreenBinding.bgHeader.setBackgroundColor(getResources().getColor(R.color.colorSulpak));
                settingsScreenBinding.icCloudBlue.setImageResource(R.drawable.ic_sulpak_red);
                settingsScreenBinding.icDelete.setImageResource(R.drawable.ic_delete_ac_sulpak);
                settingsScreenBinding.icWifi.setImageResource(R.drawable.ic_wifi_sulpak);
                settingsScreenBinding.icStats.setImageResource(R.drawable.ic_stats_sulpak);
                settingsScreenBinding.icNotify.setImageResource(R.drawable.ic_notify_sulpak);
                settingsScreenBinding.textData.setTextColor(getResources().getColor(R.color.colorSulpak));
                break;
        }
    }
}
