package com.virtualeducation.forwardpremium;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.virtualeducation.forwardpremium.objects.DocumentObject;

import java.util.ArrayList;

public class UserDocumentsGetAll {
    ArrayList<DocumentObject> allDocs=new ArrayList<>();
    public ArrayList<DocumentObject> getAllDocuments(Context context) {
        Uri uri = MediaStore.Files.getContentUri("external");
        String[] projection = {"title","_data"};
        String selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "=? or "+MediaStore.Files.FileColumns.MIME_TYPE + "=? or "+
                MediaStore.Files.FileColumns.MIME_TYPE + "=? or "+MediaStore.Files.FileColumns.MIME_TYPE + "=? or "+
                MediaStore.Files.FileColumns.MIME_TYPE + "=? or "+MediaStore.Files.FileColumns.MIME_TYPE + "=?";
        String mimeTypePdf = MimeTypeMap.getSingleton().getMimeTypeFromExtension("pdf");
        String mimeTypeDoc = MimeTypeMap.getSingleton().getMimeTypeFromExtension("doc");
        String mimeTypeDocx = MimeTypeMap.getSingleton().getMimeTypeFromExtension("docx");
        String mimeTypeXls = MimeTypeMap.getSingleton().getMimeTypeFromExtension("xls");
        String mimeTypeXlsx = MimeTypeMap.getSingleton().getMimeTypeFromExtension("xlsx");
        String mimeTypeTxt = MimeTypeMap.getSingleton().getMimeTypeFromExtension("txt");
        String[] selectionArgsPdf = new String[]{mimeTypePdf,mimeTypeDoc,mimeTypeDocx,mimeTypeXls,mimeTypeXlsx,mimeTypeTxt};

        try{
            Cursor cursor = context.getContentResolver().query(uri, projection, selectionMimeType, selectionArgsPdf, null);

            if (cursor.moveToFirst()) {
                do {
                    allDocs.add(new DocumentObject(cursor.getString(0),cursor.getString(1)));
                } while (cursor.moveToNext());
            }
        }catch (Exception e){
            return allDocs;
        }

        return allDocs;
    }

}
