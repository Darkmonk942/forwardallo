package com.virtualeducation.forwardpremium;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import java.util.Locale;

public class ChooseLanguageActivity extends AppCompatActivity {
    String currentLanguage="ua";

    Button textUa;
    Button textRu;

    TextView textChooseLanguage;

    ImageView icLogo;

    Button btnNext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);
        textRu=(Button)findViewById(R.id.text_ru);
        textUa=(Button)findViewById(R.id.text_ua);
        btnNext=(Button)findViewById(R.id.btn_next);
        icLogo=(ImageView)findViewById(R.id.ic_logo);
        textChooseLanguage=(TextView)findViewById(R.id.text_choose_language);

        setPartnersInfo();
    }

    // Выбор языка
    public void setCurrentLanguage(View v){
        if(v.getId()==R.id.text_ua){
            textRu.setBackgroundResource(R.drawable.btn_off);
            textUa.setBackgroundResource(R.drawable.btn_on);
            currentLanguage="ua";
            textChooseLanguage.setText("Оберiть мову");
        }else{
            textRu.setBackgroundResource(R.drawable.btn_on);
            textUa.setBackgroundResource(R.drawable.btn_off);
            currentLanguage="ru";
            textChooseLanguage.setText("Выберите язык");
        }
    }

    // Переход на экран туториала
    public void applyCurrentLanguage(View v){
        Locale locale = new Locale(currentLanguage);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        Hawk.put("language",currentLanguage);
        Hawk.put("userAuthStage","tutorial");
        startActivity(new Intent(this,TutorialScreenActivity.class));
        finish();
    }

    private void setPartnersInfo() {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                btnNext.setBackgroundResource(R.drawable.shape_btn_start_sulpak);
                icLogo.setImageResource(R.drawable.ic_logo_sulpak);
                break;
        }
    }

    private void setPartnersBtnLanguage() {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                btnNext.setBackgroundResource(R.drawable.shape_btn_start_sulpak);
                break;
        }
    }
}
