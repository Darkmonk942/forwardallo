package com.virtualeducation.forwardpremium;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.forwardpremium.databinding.ActivityRecoverScreenBinding;
import com.virtualeducation.forwardpremium.eventbus.PhotoDownloadEvent;
import com.virtualeducation.forwardpremium.objects.CalendarObject;
import com.virtualeducation.forwardpremium.objects.CallObject;
import com.virtualeducation.forwardpremium.objects.CloudBackupCalendar;
import com.virtualeducation.forwardpremium.objects.CloudBackupCalls;
import com.virtualeducation.forwardpremium.objects.CloudBackupContacts;
import com.virtualeducation.forwardpremium.objects.CloudBackupDocs;
import com.virtualeducation.forwardpremium.objects.CloudBackupPhoto;
import com.virtualeducation.forwardpremium.objects.CloudBackupSettings;
import com.virtualeducation.forwardpremium.objects.CloudBackupSms;
import com.virtualeducation.forwardpremium.objects.CloudInfoObject;
import com.virtualeducation.forwardpremium.objects.ContactObject;
import com.virtualeducation.forwardpremium.objects.SettingsBackupObject;
import com.virtualeducation.forwardpremium.objects.SettingsObject;
import com.virtualeducation.forwardpremium.objects.SmsObject;
import com.virtualeducation.forwardpremium.retrofit.GetForwardCloudInfo;
import com.virtualeducation.forwardpremium.retrofit.GetUserCalendarList;
import com.virtualeducation.forwardpremium.retrofit.GetUserCallsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserContactsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserDocsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserPhotoList;
import com.virtualeducation.forwardpremium.retrofit.GetUserSettingsList;
import com.virtualeducation.forwardpremium.retrofit.GetUserSmsList;
import com.virtualeducation.forwardpremium.retrofit.RetrofitApi;
import com.virtualeducation.forwardpremium.retrofit.RetrofitRequestCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 07.07.2017.
 */

public class RecoverDataActivity extends AppCompatActivity {
    ActivityRecoverScreenBinding recoverScreenBinding;
    ModelRecoverDataManager recoverDataManager;

    RetrofitApi retrofitApi;
    GetForwardCloudInfo getForwardCloudInfo;
    GetUserContactsList getUserContactsList;
    GetUserSmsList getUserSmsList;
    GetUserCallsList getUserCallsList;
    GetUserPhotoList getUserPhotoList;
    GetUserCalendarList getUserCalendarList;
    GetUserDocsList getUserDocsList;
    GetUserSettingsList getUserSettingsList;

    final Handler handler = new Handler();
    List<Call<?>> allCalls;
    ObjectAnimator copyAnimation=null;
    private static final int DEF_SMS_REQ = 0;
    AtomicInteger currentCallCount=new AtomicInteger(0);
    char[] accessToken;
    String defaultSmsApp;
    boolean isContactsEnables=true;
    boolean isCallsEnables=true;
    boolean isSmsEnables=true;
    boolean isPhotoEnables=true;
    boolean isDocsEnables=true;
    boolean isSettingsEnables=true;
    boolean isCalendarEnables=true;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        recoverScreenBinding= DataBindingUtil.setContentView(this,R.layout.activity_recover_screen);
        recoverDataManager=new ModelRecoverDataManager(this);

        retrofitApi=RetrofitApi.getInstance();

        // Получаем accessToken текущего пользователя
        accessToken= Hawk.get("accessToken","").toCharArray();

        // Оптимизация интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(recoverScreenBinding.frameMain);
        optimization.Optimization(recoverScreenBinding.frameAllRecover);
        optimization.Optimization(recoverScreenBinding.frameProgressContacts);
        optimization.Optimization(recoverScreenBinding.frameProgressCalls);
        optimization.Optimization(recoverScreenBinding.frameProgressSms);
        optimization.Optimization(recoverScreenBinding.frameProgressPhoto);
        optimization.Optimization(recoverScreenBinding.frameProgressCalendar);
        optimization.Optimization(recoverScreenBinding.frameProgressDocs);
        optimization.Optimization(recoverScreenBinding.frameProgressSettings);

        // Формируем запросы для получения информации по облаку
        getForwardCloudInfo=retrofitApi.retrofit.create(GetForwardCloudInfo.class);
        getUserContactsList=retrofitApi.retrofit.create(GetUserContactsList.class);
        getUserSmsList=retrofitApi.retrofit.create(GetUserSmsList.class);
        getUserCallsList=retrofitApi.retrofit.create(GetUserCallsList.class);
        getUserPhotoList=retrofitApi.retrofit.create(GetUserPhotoList.class);
        getUserCalendarList=retrofitApi.retrofit.create(GetUserCalendarList.class);
        getUserDocsList=retrofitApi.retrofit.create(GetUserDocsList.class);
        getUserSettingsList=retrofitApi.retrofit.create(GetUserSettingsList.class);

        // Получаем данные по бэкапу
        getCloudData();
        setPartnerInfo();

    }

    // Пропускаем восстановление данных
    public void btnSkipClick(View v){
        startActivity(new Intent(this,MainScreenActivity.class));
        finish();
    }

    // Возврат дефолтного смс приложения
    private void setDefaultSmsApp(){
        if(defaultSmsApp!=null){
            Intent intentDefaultSmsApp = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intentDefaultSmsApp.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, defaultSmsApp);
            startActivity(intentDefaultSmsApp);
        }
    }

    // Клик для начала восстановления данных
    public void btnRecoverClick(View v){
        // Проверка на возможность восстановления
        if(!isContactsEnables&&!isCallsEnables&&!isPhotoEnables&&!isSmsEnables&&!isDocsEnables
                &&!isCalendarEnables&&!isSettingsEnables){
            return;
        }

        // Проверка на настройки для восстановления настроек
        if(isSettingsEnables){
            if(!Hawk.get("seetings_write",false)){
                ToastManager.createToast(this,"Разрешите приложению управлять настройками телефона");
                Hawk.put("seetings_write",true);
                openAndroidPermissionsMenu();
                return;
            }
        }


        allCalls=new ArrayList();
        currentCallCount.set(0);

        // Создаём колы для нужных запросов
        if (isContactsEnables){
            Call<ArrayList<ContactObject>> contactsCall=getUserContactsList.contacts(String.copyValueOf(accessToken));
            allCalls.add(contactsCall);
        }
        if(isSmsEnables){
            Call<ArrayList<SmsObject>> smsCall=getUserSmsList.sms(String.copyValueOf(accessToken));
            allCalls.add(smsCall);
        }
        if(isCallsEnables){
            Call<ArrayList<CallObject>> callsCall=getUserCallsList.calls(String.copyValueOf(accessToken));
            allCalls.add(callsCall);
        }
        if(isPhotoEnables){
            Call<List<CloudBackupPhoto>> photoCall=getUserPhotoList.photo(String.copyValueOf(accessToken));
            allCalls.add(photoCall);
        }
        if(isSettingsEnables){
            Call<ArrayList<SettingsObject>> settingsCall=getUserSettingsList.settings(String.copyValueOf(accessToken));
            allCalls.add(settingsCall);
        }
        if(isCalendarEnables){
            Call<ArrayList<CalendarObject>> calendarCall=getUserCalendarList.calendar(String.copyValueOf(accessToken));
            allCalls.add(calendarCall);
        }
        if(isDocsEnables){
            Call<List<CloudBackupDocs>> docsCall=getUserDocsList.docs(String.copyValueOf(accessToken));
            allCalls.add(docsCall);
        }

        // Проверка на восстановление СМС
        if(isSmsEnables&&!Telephony.Sms.getDefaultSmsPackage(this).equals(this.getPackageName())) {
            defaultSmsApp=Telephony.Sms.getDefaultSmsPackage(this);
            // Запрос на изменение дефолтного смс приложения
            Intent intentNewDefaultSmsApp = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intentNewDefaultSmsApp.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, this.getPackageName());
            startActivityForResult(intentNewDefaultSmsApp, DEF_SMS_REQ);
            return;
        }

        if(allCalls.size()>0) {
            restoreCloudData(allCalls.get(0));
            recoverScreenBinding.frameRestoreData.setVisibility(View.VISIBLE);
            recoverScreenBinding.frameAllData.setVisibility(View.GONE);
        }
    }

    // Восстанавливаем данные
    private void restoreCloudData(Call<?> currentCall){
        retrofitApi.createRequest(currentCall);
        retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {

                Response<?> response=(Response<?>)responseObject;
                if(response.code()!=200&&response.body()==null){
                    ToastManager.createToast(getApplicationContext(),"Ошибка");
                    recoverScreenBinding.frameRestoreData.setVisibility(View.GONE);
                    recoverScreenBinding.frameAllData.setVisibility(View.VISIBLE);
                    return;
                }

                ArrayList<?> checkedList = (ArrayList<?>) response.body();
                if(checkedList.size() > 0 ){

                    if(checkedList.get(0) instanceof ContactObject){
                        // Восстанавливаем контакты
                        progressAnimation("contact");
                        ArrayList<ContactObject> backupContacts=(ArrayList<ContactObject>)response.body();
                        recoverDataManager.recoverContacts(backupContacts);
                    }
                    if(checkedList.get(0) instanceof CallObject){
                        // Восстанавливаем звонки
                        progressAnimation("call");
                        ArrayList<CallObject> backupCalls =(ArrayList<CallObject>)response.body();
                        recoverDataManager.recoverCalls(backupCalls);
                    }
                    if(checkedList.get(0) instanceof SmsObject){
                        // Восстанавливаем смс
                        progressAnimation("sms");
                        ArrayList<SmsObject> backupSms=(ArrayList<SmsObject>)response.body();
                        recoverDataManager.recoverSms(backupSms);

                        // Восстанавливаем дефолтное смс приложение
                    /*
                    if(defaultSmsApp!=null){
                        setDefaultSmsApp();
                    }*/

                    }
                    if(checkedList.get(0) instanceof CalendarObject){
                        // Восстанавливаем календарь
                        progressAnimation("calendar");
                        ArrayList<CalendarObject> backupCalendar=(ArrayList<CalendarObject>)response.body();
                        recoverDataManager.recoverCalendar(backupCalendar);

                    }
                    if(checkedList.get(0) instanceof SettingsObject){
                        // Восстанавливаем настройки
                        progressAnimation("settings");
                        ArrayList<SettingsObject> backupSettings=(ArrayList<SettingsObject>)response.body();
                        recoverDataManager.recoverSettings(backupSettings);
                    }
                    if(checkedList.get(0) instanceof CloudBackupPhoto){
                        progressAnimation("photo");
                        ArrayList<CloudBackupPhoto> photoBackup=(ArrayList<CloudBackupPhoto>)response.body();
                        recoverDataManager.recoverPhoto(photoBackup);
                    }
                    if(checkedList.get(0) instanceof CloudBackupDocs){
                        progressAnimation("documents");
                        ArrayList<CloudBackupDocs> docsBackup=(ArrayList<CloudBackupDocs>)response.body();
                        recoverDataManager.recoverDocuments(docsBackup);
                    }
                }

                if(currentCallCount.addAndGet(1)==allCalls.size()){
                    endOfCopy();
                }else{
                    restoreCloudData(allCalls.get(currentCallCount.get()));
                }
            }

            @Override
            public void onEventFailure(Throwable error) {
                Log.d("Ошибочка",error.toString());
                ToastManager.createToast(getApplicationContext(),"Ошибка");
                recoverScreenBinding.frameRestoreData.setVisibility(View.GONE);
                recoverScreenBinding.frameAllData.setVisibility(View.VISIBLE);
            }
        });
    }

    // Отображаем анимацию восстановления данных
    private void progressAnimation(String type){
        switch (type){
            case "contact":
                copyAnimation = ObjectAnimator.ofInt(recoverScreenBinding.progressContacts, "progress",100);
                recoverScreenBinding.textProgressContacts
                        .setAnimationDuration(1200)
                        .countAnimation(0, 100);
                break;
            case "sms":
                copyAnimation = ObjectAnimator.ofInt(recoverScreenBinding.progressSms, "progress",100);
                recoverScreenBinding.textProgressSms
                        .setAnimationDuration(1200)
                        .countAnimation(0, 100);
                break;
            case "call":
                copyAnimation = ObjectAnimator.ofInt(recoverScreenBinding.progressCalls, "progress",100);
                recoverScreenBinding.textProgressCalls
                        .setAnimationDuration(1200)
                        .countAnimation(0, 100);
                break;
            case "calendar":
                copyAnimation = ObjectAnimator.ofInt(recoverScreenBinding.progressCalendar, "progress",100);
                recoverScreenBinding.textProgressCalendar
                        .setAnimationDuration(1200)
                        .countAnimation(0, 100);
                break;
            case "settings":
                copyAnimation = ObjectAnimator.ofInt(recoverScreenBinding.progressSettings, "progress",100);
                recoverScreenBinding.textProgressSettings
                        .setAnimationDuration(1200)
                        .countAnimation(0, 100);
                break;
            case "documents":
                copyAnimation = ObjectAnimator.ofInt(recoverScreenBinding.progressDocs, "progress",100);
                recoverScreenBinding.textProgressDocs
                        .setAnimationDuration(1200)
                        .countAnimation(0, 100);
                break;
            case "photo":
                copyAnimation = ObjectAnimator.ofInt(recoverScreenBinding.progressPhoto, "progress",40);
                recoverScreenBinding.textProgressPhoto
                        .setAnimationDuration(1200)
                        .countAnimation(0, 40);
                break;
        }
        copyAnimation.setDuration(1200);
        copyAnimation.setInterpolator(new DecelerateInterpolator());
        copyAnimation.start();

    }

    // Заканчиваем копирование
    private void endOfCopy(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Заканчиваем выкачку на сервер
                ToastManager.createToast(getApplicationContext(),"Данные успешно восстановлены");
                startActivity(new Intent(RecoverDataActivity.this,MainScreenActivity.class));
                setDefaultSmsApp();
                finish();
            }
        }, 2000);
    }

    @Subscribe
    public void handlePhotoEvent(PhotoDownloadEvent photoDownloadEvent) {
        if(photoDownloadEvent.isLast){
            if(copyAnimation!=null){
                if(copyAnimation.isRunning()){
                    copyAnimation.pause();
                }
            }

            copyAnimation = ObjectAnimator.ofInt(recoverScreenBinding.progressPhoto, "progress",100);
            copyAnimation.setDuration(1200);
            copyAnimation.setInterpolator(new DecelerateInterpolator());
            copyAnimation.start();

            recoverScreenBinding.textProgressPhoto
                    .setAnimationDuration(1200)
                    .countAnimation(40, 100);

            //endOfCopy();
        }
    }

    public void checkCategoryClick(View v){
        ImageView checkImage=(ImageView)v;
        switch (checkImage.getId()){
            case R.id.ic_check_contacts:
                if (isContactsEnables){
                    isContactsEnables=false;
                    checkImage.setImageResource(R.drawable.shape_circle_white_transparent);
                }else{
                    isContactsEnables=true;
                    setCheckIcon(checkImage);
                }
                break;
            case R.id.ic_check_calls:
                if (isCallsEnables){
                    isCallsEnables=false;
                    checkImage.setImageResource(R.drawable.shape_circle_white_transparent);
                }else{
                    isCallsEnables=true;
                    setCheckIcon(checkImage);
                }
                break;
            case R.id.ic_check_sms:
                if (isSmsEnables){
                    isSmsEnables=false;
                    checkImage.setImageResource(R.drawable.shape_circle_white_transparent);
                }else{
                    isSmsEnables=true;
                    setCheckIcon(checkImage);
                }
                break;
            case R.id.ic_check_photo:
                if (isPhotoEnables){
                    isPhotoEnables=false;
                    checkImage.setImageResource(R.drawable.shape_circle_white_transparent);
                }else{
                    isPhotoEnables=true;
                    setCheckIcon(checkImage);
                }
                break;
            case R.id.ic_check_calendar:
                if (isCalendarEnables){
                    isCalendarEnables=false;
                    checkImage.setImageResource(R.drawable.shape_circle_white_transparent);
                }else{
                    isCalendarEnables=true;
                    setCheckIcon(checkImage);
                }
                break;
            case R.id.ic_check_settings:
                if (isSettingsEnables){
                    isSettingsEnables=false;
                    checkImage.setImageResource(R.drawable.shape_circle_white_transparent);
                }else{
                    isSettingsEnables=true;
                    setCheckIcon(checkImage);
                }
                break;
            case R.id.ic_check_notes:
                if (isDocsEnables){
                    isDocsEnables=false;
                    checkImage.setImageResource(R.drawable.shape_circle_white_transparent);
                }else{
                    isDocsEnables=true;
                    setCheckIcon(checkImage);
                }
                break;
        }
    }

    // Получаем с сервера кол-во всех данных для бэкапа
    private void getCloudData(){
        Call<CloudInfoObject> getCloudInfoCall=getForwardCloudInfo.getDataCount(String.copyValueOf(accessToken));

        retrofitApi.createRequest(getCloudInfoCall);
        retrofitApi.setOnRequestListener(new RetrofitRequestCallback() {
            @Override
            public void onEventSuccess(Object responseObject) {
                recoverScreenBinding.progressBar.setVisibility(View.GONE);
                recoverScreenBinding.btnRestore.setVisibility(View.VISIBLE);
                Response<CloudInfoObject> response=(Response<CloudInfoObject>)responseObject;

                if(response.code()!=200||response.body()==null){
                    return;
                }
                Log.d("Облако", String.valueOf(response.code()));
                // Отображаем кол-во данных полученных с сервера
                CloudInfoObject cloudInfoObject=response.body();
                recoverScreenBinding.textContactsCount.setText(String.valueOf(cloudInfoObject.contactsCount));
                recoverScreenBinding.textSmsCount.setText(String.valueOf(cloudInfoObject.smsCount));
                recoverScreenBinding.textCallsCount.setText(String.valueOf(cloudInfoObject.callsCount));
                recoverScreenBinding.textPhotoCount.setText(String.valueOf(cloudInfoObject.photoCount));
                recoverScreenBinding.textCalendarCount.setText(String.valueOf(cloudInfoObject.calendarCount));
                recoverScreenBinding.textSettingsCount.setText(String.valueOf(cloudInfoObject.settingsCount));
                recoverScreenBinding.textNotesCount.setText(String.valueOf(cloudInfoObject.docsCount));
                recoverScreenBinding.textAppsCount.setText(String.valueOf(cloudInfoObject.appsCount));

            }

            @Override
            public void onEventFailure(Throwable error) {
                Log.d("Облако", error.toString());
            }
        });
    }

    private void openAndroidPermissionsMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + "com.virtualeducation.forwardpremium"));
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            case DEF_SMS_REQ:
                if(resultCode == Activity.RESULT_OK){
                    restoreCloudData(allCalls.get(0));
                    recoverScreenBinding.frameRestoreData.setVisibility(View.VISIBLE);
                    recoverScreenBinding.frameAllData.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // Cancel current call
        if(allCalls!=null) {
            if(currentCallCount.get()!=0){
                if (allCalls.get(currentCallCount.get() - 1) != null) {
                    allCalls.get(currentCallCount.get() - 1).cancel();
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    // Инициализация Калиграфии для кастомных шрифтов
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setPartnerInfo() {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                recoverScreenBinding.icLogo.setImageResource(R.drawable.ic_logo_sulpak);
                break;
        }
    }

    private void setCheckIcon(ImageView icon) {
        switch (BuildConfig.PARTNER){
            case Partners.SULPAK:
                icon.setImageResource(R.drawable.ic_photo_check_sulpak);
                break;
        }
    }
}
